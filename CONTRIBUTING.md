1. Developer creates a new feature branch from `Dev` branch
2. Developer creates a merge request into `Dev` branch
3. Maintainer merges feature into `Dev` branch
4. Maintainer merges `Dev` branch with a `Release` branch for the target version
5. Maintainer merges `Release` branch with `master`

Typical development workflow:

1. Create a branch from the issue
2. `git pull` - this gets the latest updates
3. `git checkout <branch>` - we only want to make changes on the branch
4. make your edits
5. `git add .` - this adds all the changes you've made at once
6. `git commit` - commits the changes
7. `git pull` - we do this again in case others have pushed changes while you are working on yours
8. `git push` - push the changes up to the server
9. create a merge request in gitlab