package edu.ucf.ist.truststudy;

import static org.junit.Assert.assertEquals;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.IntStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import edu.ucf.ist.def.ExperimentServer;

public class TrustStudyServerTest {

  @Test
  public void maxPerformanceRandomTestShouldBeUniform()
      throws ParserConfigurationException, SAXException, IOException, InstantiationException {
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    DocumentBuilder db = dbf.newDocumentBuilder();
    Document dom = db.parse(new File("truststudy_stage1_en.xml"));
    Element rootNode = dom.getDocumentElement();

    IntStream flips = IntStream.rangeClosed(1, 256);
    ArrayList<Integer> results = new ArrayList<>();

    flips.forEach((flip) -> {
      TrustStudyServer server = new TrustStudyServer();
      try {
        ((ExperimentServer) server).initObject(rootNode, 0, null);
      } catch (InstantiationException e) {
        e.printStackTrace();
      }
      results.add(server.getRandomNumber());
    });

    
    float mean = computeMean(results);
    float variance= computeVariance(results,mean);

    assertEquals(mean, .5, variance);
  }

  private float computeVariance(ArrayList<Integer> vals,float mean) {
    float variance = 0;

    for (int i=0;i<vals.size();i++){
      variance+=Math.pow(vals.get(i)-mean,2);
    }
    variance/=vals.size();
    return variance;
  }

  private float computeMean(ArrayList<Integer> vals) {
    float mean = 0;

    for(int i=0;i<vals.size();i++){
      mean+=vals.get(i);
    }

    mean/=vals.size()-1;

    return mean;
  }

}