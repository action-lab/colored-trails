/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucf.ist.truststudy;

import edu.ucf.ist.def.DEFMessage;
import edu.ucf.ist.def.DEFMessageListener;
import edu.ucf.ist.def.L;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author culturelab
 */
public class BribeManager implements DEFMessageListener {

    private static Logger l = L.mcl(BribeManager.class, Level.FINER);
    private String bribeResponseMessage = "response?";
    private String bribeNoResponseLabel = "Ignore";
    private String bribeMonitaryResponseLabel = "Pay";
    private String bribeTextResponseLabel = "Write";
    private String bribeMonitaryResponseMessage = "Pay how much?";
    private String bribeTextResponseMessage = "Write what?";
    private String bribeMessage = "Bribe?";
    private String bribeAcceptLabel = "Sure";
    private String bribeRejectLabel = "Nor for me";
    private String mistakeAcceptLabel = "Sure";
    private String bribeApologyLabel = "Sorry";
    private String bribeApologyMessage = "Sorry";
    private boolean mistake = false;
    private final TrustStudyClient tsc;

    public BribeManager(TrustStudyClient tsc) {
        this.tsc = tsc;
        tsc.addListener(this);
    }

    public void processMessage(DEFMessage dm) {
        if (dm.name.equals("bribemanager.new")) {
            l.finer("got bribemanager.new");
            NodeList strings = dm.mRoot.getElementsByTagName("string");
            for (int i = 0; i < strings.getLength(); i++) {
                Element entry = (Element) strings.item(i);
                if (entry.getAttribute("key").equals("bribeMessage")) {
                    bribeMessage = (entry.getFirstChild().getTextContent());
                }
                if (entry.getAttribute("key").equals("mistakeMessage")) {
                    bribeMessage = (entry.getFirstChild().getTextContent());
                    mistake = true;
                }
                if (entry.getAttribute("key").equals("bribeAcceptLabel")) {
                    bribeAcceptLabel = (entry.getFirstChild().getTextContent());
                }
                if (entry.getAttribute("key").equals("bribeRejectLabel")) {
                    bribeRejectLabel = (entry.getFirstChild().getTextContent());
                }
                if (entry.getAttribute("key").equals("bribeResponseMessage")) {
                    bribeResponseMessage = (entry.getFirstChild().getTextContent());
                }
                if (entry.getAttribute("key").equals("bribeNoResponseLabel")) {
                    bribeNoResponseLabel = (entry.getFirstChild().getTextContent());
                }
                if (entry.getAttribute("key").equals("bribeMonetaryResponseLabel")) {
                    bribeMonitaryResponseLabel = entry.getFirstChild().getTextContent();
                }
                if (entry.getAttribute("key").equals("bribeApologyLabel")) {
                    bribeApologyLabel = (entry.getFirstChild().getTextContent());
                }
                if (entry.getAttribute("key").equals("bribeApologyMessage")) {
                    bribeApologyMessage = (entry.getFirstChild().getTextContent());
                }
                if (entry.getAttribute("key").equals("bribeTextResponseLabel")) {
                    bribeTextResponseLabel = entry.getFirstChild().getTextContent();
                }
                if (entry.getAttribute("key").equals("bribeMonetaryResponseMessage")) {
                    bribeMonitaryResponseMessage = entry.getFirstChild().getTextContent();
                }
                if (entry.getAttribute("key").equals("bribeTextResponseMessage")) {
                    bribeTextResponseMessage = entry.getFirstChild().getTextContent();
                }
            }
            final BribeManager bm = this;
            new Thread(new Runnable() {

                public void run() {
                    bm.showDialog();
                }
            }).start();
        }
}

    public boolean isMarkedForRemoval() {
        return false;
    }

    private void showDialog() {
        l.finer("");
        
        ArrayList<Object> options = new ArrayList<>();
        JOptionPane optionPane1 = null;
        if(!mistake) {
            options.add(bribeAcceptLabel);
            options.add(bribeRejectLabel);

            optionPane1 = new JOptionPane(bribeMessage,
            JOptionPane.QUESTION_MESSAGE,
            JOptionPane.YES_NO_OPTION,
            null,
            options.toArray(),
            options.get(1));

        } else {
            options.add(bribeAcceptLabel);

            optionPane1 = new JOptionPane(bribeMessage,
            JOptionPane.QUESTION_MESSAGE,
            JOptionPane.DEFAULT_OPTION,
            null,
            options.toArray(),
            options.get(0));
            
        }
                
        final JOptionPane optionPane = optionPane1;
            
        final JDialog dialog = new JDialog(tsc.bw,
                null,
                true);
        dialog.setContentPane(optionPane);
        dialog.setDefaultCloseOperation(
                JDialog.DO_NOTHING_ON_CLOSE);

        optionPane.addPropertyChangeListener(
            new PropertyChangeListener() {

                public void propertyChange(PropertyChangeEvent e) {
                    String prop = e.getPropertyName();

                    if (dialog.isVisible()
                            && (e.getSource() == optionPane)
                            && (prop.equals(JOptionPane.VALUE_PROPERTY))) {
                        //If you were going to check something
                        //before closing the window, you'd do
                        //it here.
                        dialog.setVisible(false);
                    }
                }
            }
        );
        dialog.pack();
        dialog.setVisible(true);
        dialog.setLocationRelativeTo(null);

        Object value = optionPane.getValue();
        System.out.println(value);
        if (value == bribeAcceptLabel) {
            showBribeOptionDialog();
        } else if (value == bribeRejectLabel) {
            tsc.sendMessageToServer("bribemanager.closed", "<action response=\"reject\" />");
        }
    }

    private void showBribeOptionDialog() {
        l.finer("");
        Object[] options = {bribeNoResponseLabel,
            bribeMonitaryResponseLabel, bribeTextResponseLabel, bribeApologyLabel};
        final JOptionPane optionPane = new JOptionPane(bribeResponseMessage,
                JOptionPane.QUESTION_MESSAGE,
                JOptionPane.YES_NO_CANCEL_OPTION,
                null,
                options,
                options[0]);


        final JDialog dialog = new JDialog(tsc.bw,
                null,
                true);
        dialog.setContentPane(optionPane);
        dialog.setDefaultCloseOperation(
                JDialog.DO_NOTHING_ON_CLOSE);
        optionPane.addPropertyChangeListener(
                new PropertyChangeListener() {

                    public void propertyChange(PropertyChangeEvent e) {
                        String prop = e.getPropertyName();

                        if (dialog.isVisible()
                                && (e.getSource() == optionPane)
                                && (prop.equals(JOptionPane.VALUE_PROPERTY))) {
                            //If you were going to check something
                            //before closing the window, you'd do
                            //it here.
                            dialog.setVisible(false);
                        }
                    }
                });
        dialog.pack();
        dialog.setVisible(true);
        dialog.setLocationRelativeTo(null);

        Object value = optionPane.getValue();
        System.out.println(value);
        if (!mistake){
                if (value == bribeNoResponseLabel) {
                    tsc.sendMessageToServer("bribemanager.closed", "<action response=\"acceptNoResponse\" />");
                } else if (value == bribeMonitaryResponseLabel) {
                    int amount = getResponseMonetaryAmount();
                    tsc.sendMessageToServer("bribemanager.closed", "<action response=\"acceptMonetaryResponse\"><![CDATA[" + amount + "]]></action>");
                } else if (value == bribeTextResponseLabel) {
                    String response = getResponseText();
                    tsc.sendMessageToServer("bribemanager.closed", "<action response=\"acceptTextResponse\"><![CDATA[" + response + "]]></action>");
                } else if (value == bribeApologyLabel) {
                    String response = getApologyText();
                    tsc.sendMessageToServer("bribemanager.closed", "<action response=\"acceptApologyResponse\"><![CDATA[" + response + "]]></action>");
                }
        } else {
            if (value == bribeNoResponseLabel) {
                tsc.sendMessageToServer("mistakemanager.closed", "<action response=\"acceptNoResponse\" />");
            } else if (value == bribeMonitaryResponseLabel) {
                int amount = getResponseMonetaryAmount();
                tsc.sendMessageToServer("mistakemanager.closed", "<action response=\"acceptMonetaryResponse\">" + amount + "</action>");
            } else if (value == bribeTextResponseLabel) {
                String response = getResponseText();
                tsc.sendMessageToServer("mistakemanager.closed", "<action response=\"acceptTextResponse\"><![CDATA[" + response + "]]></action>");
            } else if (value == bribeApologyLabel) {
                String response = getApologyText();
                tsc.sendMessageToServer("mistakemanager.closed", "<action response=\"acceptApologyResponse\"><![CDATA[" + response + "]]></action>");
            }
        }
    }

    private int getResponseMonetaryAmount() {
        int amount = 5;
        String response = null;
        do {
            response = (String) JOptionPane.showInputDialog(
                    tsc.bw,
                    bribeMonitaryResponseMessage,
                    null,
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    null,
                    "");
            try {
                if (response != null) {
                    amount = Integer.parseInt(response);
                }
            } catch (NumberFormatException ex) {
                response = null;
            }
        } while (response == null);

        return amount;
    }

    private String getResponseText() {
        String response = null;
        do {
            response = (String) JOptionPane.showInputDialog(
                    tsc.bw,
                    bribeTextResponseMessage,
                    null,
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    null,
                    "");
        } while (response == null || response.length() == 0);
        return response;
    }
    
    private String getApologyText() {
        String response = null;
        do {
            response = (String) JOptionPane.showInputDialog(
                    tsc.bw,
                    bribeApologyMessage,
                    null,
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    null,
                    "");
        } while (response == null || response.length() == 0);
        return response;
    }

    public static void main(String[] args) {
        new BribeManager(null).showDialog();
    }
}
