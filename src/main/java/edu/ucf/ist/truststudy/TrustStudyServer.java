/*
 * To change this template, choose Tools | Templates and open the template in the editor.
 */
package edu.ucf.ist.truststudy;

import java.awt.Color;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.swing.JOptionPane;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import edu.ucf.ist.def.AbstractServer;
import edu.ucf.ist.def.DEFMessage;
import edu.ucf.ist.def.EndOfGameListener;
import edu.ucf.ist.def.L;
import edu.ucf.ist.def.Player;
import edu.ucf.ist.session.Session;
import edu.ucf.ist.session.StageResults;
import edu.ucf.ist.truststudy.TrustStudyPlayer.ReparationType;
import edu.ucf.ist.truststudy.dialogs.DialogFactory;
import edu.ucf.ist.truststudy.dialogs.DialogFactory.DialogType;
import edu.ucf.ist.truststudy.dialogs.endofgame.ParticipationPage;
import edu.ucf.ist.truststudy.dialogs.endofstage.MistakeOrBribePage;
import edu.ucf.ist.truststudy.session.TrustStudyEntry;
import edu.ucf.ist.truststudy.session.TrustStudyResults;
import edu.ucf.ist.truststudy.session.TrustStudySession;

/**
 *
 * NOTE: the info elements in the XML file has to have ids in ascending order
 *
 * @author culturelab
 */
public class TrustStudyServer extends AbstractServer {

    public final static int PLAYER_COUNT = 2;
    public int resourceStartLevel = 100;
    public int resourceStartLevel2 = 100;
    Random rand = new Random();
    private static Logger l = L.mcl(TrustStudyServer.class, Level.FINER);
    ArrayList<Information> infos = new ArrayList<>(30);
    Information[] goals = new Information[PLAYER_COUNT];
    Player auto;
    int BOARD_WIDTH = 13;
    int BOARD_HEIGHT = 13;
    int stage;
    int maxBlockInfoID;
    // Goal permutations
    int chosenGoalPermutation = 0;
    int[][] goalPermutations = {{0, 1}, {1, 0}};
    boolean proposalActive = true;
    boolean proposalSuccess = false;
    int proposeOption;
    int unreadyPlayers;
    int goalUnsurePlayers;
    boolean gameOver = false;
    // more tracking information
    long gameStartTime;
    String proposalOwner = "None";
    long combinationTime;
    int proposalCount = 0;
    int combinationCount = 0;
    long missionFinishTime = 0;
    float teamScore;
    int shared = 0;
    int unshared = 0;
    int experimenterNumber = 1;
    String participationResponse = "";
    boolean bribeAccepted = false;

    int resourceLevel = 0;
    int resourceLevel2 = 0;
    int tallyCurrency = 0;
    int tallyCurrency2 = 0;
    int bribeShareAmount = 0;
    long proposalStartTime = 0;
    private String mistakeReparation = "";
    private String mistakeReparationString = "";
    int mistakeshare = 0;
    int bribeshare = 0;
    private boolean madeMistake = false;
    private boolean goalsSet = false;
    EndOfGameListener listener = null;
    private TrustStudyResults results = null;
    Costs costs = null;
    String currencyLabel = "Coin";
    String resourceLabel = "Food";

    enum ResourceLevelGet {
        NONE, ASK, PREVIOUS
    };

    ResourceLevelGet resourceLevelSource = ResourceLevelGet.NONE;

    enum StageType {
        NORMAL, VIOLATION, REPAIR, REVENGE
    }

    StageType stageType = StageType.NORMAL;

    public StageType getStageType() {
        return this.stageType;
    }

    boolean hasBehavioralResponse = false;
    boolean isLastStage = false;
    private String behavioralResponse = "";

    public enum Fault {
        MISTAKE, BRIBE
    }

    Fault fault = getRandomNumber() > 0 ? Fault.MISTAKE : Fault.BRIBE;

    private String confrontResponse = "none";

    @Override
    public void initObject(Element serverRoot, int playerNum, EndOfGameListener l)
            throws InstantiationException {

        int goalCount = 0;

        if(serverRoot.hasAttribute("fault")){
            String faultCondition = serverRoot.getAttribute("fault").toUpperCase();
            if(faultCondition.equals("MISTAKE")){
                this.fault = Fault.MISTAKE;
            } else if (faultCondition.equals("BRIBE")){
                this.fault = Fault.BRIBE;
            } else {
                //TODO (Doug): throw error or warning
            }
        }

        experimenterNumber = playerNum;
        listener = l;

        super.initObject(serverRoot);
        if (serverRoot.hasAttribute("seed")) {
            rand = new Random(Long.parseLong(serverRoot.getAttribute("seed")));
        } else {
            rand = new Random();
        }
        stage = Integer.parseInt(serverRoot.getAttribute("stage"));

        resourceStartLevel = Integer.parseInt(serverRoot.getAttribute("startingResource"));
        resourceStartLevel2 = resourceStartLevel;
        if (serverRoot.hasAttribute("resourceLevel")) {
            if (serverRoot.getAttribute("resourceLevel").equalsIgnoreCase("ask")) {
                resourceLevelSource = ResourceLevelGet.ASK;
            } else if (serverRoot.getAttribute("resourceLevel").equalsIgnoreCase("previous")) {
                resourceLevelSource = ResourceLevelGet.PREVIOUS;
            } else {
                resourceLevelSource = ResourceLevelGet.NONE;
            }
        }

        currencyLabel = serverRoot.getAttribute("currencyLabel");
        resourceLabel = serverRoot.getAttribute("resourceLabel");

        stageType = StageType.valueOf(serverRoot.getAttribute("stageType").toUpperCase());

        hasBehavioralResponse =
                Boolean.valueOf(serverRoot.getAttribute("hasBehavioralResponse").toLowerCase());

        NodeList playerNodes = serverRoot.getElementsByTagName("player");
        players = new Player[playerNodes.getLength()];
        for (int i = 0; i < playerNodes.getLength(); i++) {
            Element p = (Element) playerNodes.item(i);
            players[i] = new TrustStudyPlayer(p.getAttribute("id"), this, p.getAttribute("name"),
                    p.getAttribute("class"),
                    new Color(Integer.parseInt(p.getAttribute("red")),
                            Integer.parseInt(p.getAttribute("blue")),
                            Integer.parseInt(p.getAttribute("green"))),
                    Integer.parseInt(p.getAttribute("offsetX")),
                    Integer.parseInt(p.getAttribute("offsetY")));
            ((TrustStudyPlayer) players[i]).resourceRemaining = resourceLevel;
        }
        auto = players[0];
        unreadyPlayers = players.length;
        goalUnsurePlayers = players.length;

        // init the sharing information.
        NodeList infoNodes = serverRoot.getElementsByTagName("info");
        for (int i = 0; i < infoNodes.getLength(); i++) {
            Element p = (Element) infoNodes.item(i);
            Information info = new Information(
                    // Ignoring the ids from the XML file. New ones are assigned in the order infos
                    // are ordered in the XML file.
                    i, Integer.parseInt(p.getAttribute("row")),
                    Integer.parseInt(p.getAttribute("col")), p.getAttribute("name"),
                    p.getAttribute("icon"), p.getAttribute("owner"), p.getAttribute("coinAdd"),
                    p.getAttribute("scoreAdd"), p.getAttribute("type"));
            if (p.hasAttribute("descriptionKey")) {
                info.setDescription(r.getString(p.getAttribute("descriptionKey")));
            }
            infos.add(info);
            if (info.type.equals("goal")) {
                for (Player pl : players) {
                    if (!pl.getID().equals(info.owner)) {
                        info.sharedList.add(pl.getID());
                    }
                }
                goals[goalCount++] = info;
            }
        }

        for (Player player : players) {
            TrustStudyPlayer tsp = (TrustStudyPlayer) player;
            // Extra IDs are assigned to paths.
            Information pathInformation = new Information(infos.size(),
                    r.getString("s.pathOf.start") + player.getName() + r.getString("s.pathOf.end"),
                    player.getID());
            pathInformation.getPath().add(new Point(6, 12));
            infos.add(pathInformation);
            tsp.setPathID(pathInformation.id);
        }

        XmlMapper mapper = new XmlMapper();
        try {
            NodeList list = serverRoot.getElementsByTagName("Cost");
            Node node = list.item(0);
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter s = new StringWriter();
            StreamResult _msg = new StreamResult(s);
            t.transform(new DOMSource(node), _msg);
            String msg = _msg.getWriter().toString().replaceAll("\n", "").replaceAll("\t", "");
            costs = mapper.readValue(msg, Costs.class);
        } catch (IOException ex) {
            Logger.getLogger(TrustStudyServer.class.getName()).log(Level.SEVERE, null, ex);
            costs = new Costs();
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(TrustStudyServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(TrustStudyServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Session getSession(int id, File logFile) {
        return new TrustStudySession(id, logFile);
    }

    @Override
    public void startExperiment() {
        switch (resourceLevelSource) {
            case ASK: {
                boolean haveInput = false;
                do {
                    try {
                        resourceLevel2 = Integer.parseInt((String) JOptionPane.showInputDialog(null,
                                "Enter Red Player's current " + resourceLabel.toLowerCase()
                                        + " level:",
                                "Input " + capitalize(resourceLabel) + " Level - Computer",
                                JOptionPane.PLAIN_MESSAGE, null, null,
                                Integer.toString(resourceStartLevel2)));
                        haveInput = true;
                    } catch (NumberFormatException ex) {
                        l.severe(ex.toString());
                    }
                } while (!haveInput);
                haveInput = false;
                do {
                    try {
                        resourceLevel = Integer.parseInt((String) JOptionPane.showInputDialog(null,
                                "Enter Blue Player's current " + resourceLabel.toLowerCase()
                                        + " level:",
                                "Input " + capitalize(resourceLabel) + " Level - Player",
                                JOptionPane.PLAIN_MESSAGE, null, null,
                                Integer.toString(resourceStartLevel)));
                        haveInput = true;
                    } catch (NumberFormatException ex) {
                        l.severe(ex.toString());
                    }
                } while (!haveInput);
                haveInput = false;
                do {
                    try {
                        tallyCurrency2 = Integer.parseInt((String) JOptionPane.showInputDialog(null,
                                "Enter Red player's " + currencyLabel.toLowerCase() + " total:",
                                "Input " + capitalize(currencyLabel) + " Total - Computer",
                                JOptionPane.PLAIN_MESSAGE, null, null,
                                Integer.toString(tallyCurrency2)));
                        haveInput = true;
                    } catch (NumberFormatException ex) {
                        l.severe(ex.toString());
                    }
                } while (!haveInput);
                haveInput = false;
                do {
                    try {
                        tallyCurrency = Integer.parseInt((String) JOptionPane.showInputDialog(null,
                                "Enter Blue Player's " + currencyLabel.toLowerCase() + " total:",
                                "Input " + capitalize(currencyLabel) + " Total - Player",
                                JOptionPane.PLAIN_MESSAGE, null, null,
                                Integer.toString(tallyCurrency)));
                        haveInput = true;
                    } catch (NumberFormatException ex) {
                        l.severe(ex.toString());
                    }
                } while (!haveInput);
                break;
            }
            case PREVIOUS: {
                if (previousStage != null) {
                    TrustStudyResults r = ((TrustStudyServer) previousStage).getResults();
                    if (r != null) {
                        StageResults.Entries e = r.getEntries();
                        if (e.size() == 2) {
                            // player 1
                            TrustStudyEntry computer = (TrustStudyEntry) e.get(0);
                            resourceLevel2 = Integer.parseInt(computer.getResourceRemaining());
                            tallyCurrency2 = Integer.parseInt(computer.getTotalCurrency());

                            // player 2
                            TrustStudyEntry player = (TrustStudyEntry) e.get(1);
                            resourceLevel = Integer.parseInt(player.getResourceRemaining());
                            tallyCurrency = Integer.parseInt(player.getTotalCurrency());

                            break;
                        }
                    }

                }
                // fallthrough - first stage with foodLevel entry added
            }
            default: {
                resourceLevel = resourceStartLevel;
                resourceLevel2 = resourceStartLevel2;
            }
        }
        initilizeClients();
        sendInitialState();
        // work the message queue
        while (!gameOver) {
            DEFMessage m = null;
            try {
                m = getMessage();
            } catch (InterruptedException ex) {
                l.log(Level.INFO, "Interrupt recieved by {0} server. Exiting message loop.",
                        getExperimentName());
            }
            if (gameOver) {
                break;
            }
            if (m != null) {
                processClientMessage(m);
            }
        }
        try {
            disconnectClients(null);
        } catch (TransformerException | JMSException ex) {
            Logger.getLogger(TrustStudyServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void endGameRequest() {
        gameOver = true;

    }

    protected Player getPlayerById(String id) {
        for (Player p : players) {
            if (p.getID().equals(id)) {
                return p;
            }
        }
        return null;
    }

    protected TrustStudyResults getResults() {
        return results;
    }

    private void initilizeClients() {
        for (Player p : players) {
            StringBuilder sb = new StringBuilder(50);
            TrustStudyPlayer tsp = ((TrustStudyPlayer) p);
            Color pc = tsp.getColor();
            sb.append("<message name=\"truststudy.init\">\n");

            sb.append("<init name=\"").append(p.getName()).append("\" width=\"").append(BOARD_WIDTH)
                    .append("\" height=\"").append(BOARD_HEIGHT).append("\" red=\"")
                    .append(pc.getRed()).append("\" green=\"").append(pc.getGreen())
                    .append("\" blue=\"").append(pc.getBlue()).append("\" offsetX=\"")
                    .append(tsp.getOffsetX()).append("\" offsetY=\"").append(tsp.getOffsetY())
                    .append("\" >\n");
            for (Player partner : players) {
                if (partner != p) {
                    TrustStudyPlayer tsp_partner = ((TrustStudyPlayer) partner);
                    Color partnerc = tsp_partner.getColor();
                    sb.append("<partner id=\"").append(partner.getID()).append("\" name=\"")
                            .append(partner.getName()).append("\" red=\"").append(partnerc.getRed())
                            .append("\" green=\"").append(partnerc.getGreen()).append("\" blue=\"")
                            .append(partnerc.getBlue()).append("\" offsetX=\"")
                            .append(tsp_partner.getOffsetX()).append("\" offsetY=\"")
                            .append(tsp_partner.getOffsetY()).append("\" />\n");
                }
            }
            sb.append("</init>\n");
            sb.append("</message>\n");
            sendMessage(new DEFMessage(getServerCommId(), p.getID(), sb.toString()));
        }
    }

    private void sendInitialState() {

        gameStartTime = System.currentTimeMillis();
        combinationTime = gameStartTime;

        StringBuilder sb = new StringBuilder(1000);
        // sending info nodes
        sb.append("<message name=\"truststudy.setState\">\n");
        sb.append(
                "<button name=\"changeGoalsButton\" enabled=\"true\" /><button name=\"finalizeGoalsButton\" enabled=\"false\" /><button name=\"executeButton\" enabled=\"false\" />\n");
        for (Information info : infos) {
            if (info.type.equals("path")) {
                sb.append("<info id=\"").append(info.id).append("\"  text=\"").append(info.name)
                        .append("\" owner=\"").append(info.owner)
                        .append("\" type=\"path\" subType=\"path\" offsetXPixel=\"")
                        .append(((TrustStudyPlayer) (getPlayerById(info.owner))).getOffsetX())
                        .append("\"  offsetYPixel=\"")
                        .append(((TrustStudyPlayer) (getPlayerById(info.owner))).getOffsetY())
                        .append("\"  >\n");
                for (Point p : info.getPath()) {
                    sb.append("<location x=\"").append(p.x).append("\" y=\"").append(p.y)
                            .append("\" />\n");
                }
                sb.append("</info>\n");
            } else {
                sb.append("<info id=\"").append(info.id).append("\" text=\"").append(info.name)
                        .append("\" row=\"").append(info.row).append("\" col=\"").append(info.col)
                        .append("\" icon=\"").append(info.icon).append("\" owner=\"")
                        .append(info.owner).append("\" type=\"block\" subType =\"")
                        .append(info.type).append("\" >\n");
                if (info.getDescription() != null) {
                    sb.append("<description><![CDATA[").append(info.getDescription())
                            .append("]]></description>");
                }
                for (String s : info.sharedList) {
                    sb.append("<shared>").append(s).append("</shared>\n");
                }
                sb.append("</info>\n");
            }
        }
        for (Player player : players) {
        }
        sb.append("<string key=\"title\"><![CDATA[").append(r.getString("board.title"))
                .append("]]></string>\n");
        sb.append("<string key=\"messageArea\" ><![CDATA[")
                .append(r.getString("board.messageAreaTitle")).append("]]></string>\n");
        sb.append("<string key=\"ownedList\" ><![CDATA[")
                .append(r.getString("board.ownedListTitle")).append("]]></string>\n");
        sb.append("<string key=\"sharePrefix\" ><![CDATA[")
                .append(r.getString("board.menu.share.prefix")).append("]]></string>\n");
        sb.append("<string key=\"sharePostfix\" ><![CDATA[")
                .append(r.getString("board.menu.share.postfix")).append("]]></string>\n");
        sb.append("<string key=\"unsharePrefix\" ><![CDATA[")
                .append(r.getString("board.menu.unshare.prefix")).append("]]></string>\n");
        sb.append("<string key=\"unsharePostfix\" ><![CDATA[")
                .append(r.getString("board.menu.unshare.postfix")).append("]]></string>\n");
        sb.append("<string key=\"noShare\" ><![CDATA[").append(r.getString("board.menu.noshare"))
                .append("]]></string>\n");
        sb.append("<string key=\"status\" ><![CDATA[").append(r.getString("board.status.welcome"))
                .append("]]></string>\n");
        sb.append("<string key=\"foodStartLevel\">").append(resourceStartLevel)
                .append("</string>\n"); // Must precede
                                        // resourceLevel
                                        // for correct
                                        // operation of
                                        // progressbar
        sb.append("<string key=\"foodLevel\">").append(resourceLevel).append("</string>\n");
        sb.append("<string key=\"progressText\">").append(resourceLevel).append(" / ")
                .append(resourceStartLevel).append("</string>\n");
        sb.append("</message>");

        sendMessage(new DEFMessage(getServerCommId(), getExperimentName() + ".all", sb.toString()));
        for (Player p : players) {
            sb = new StringBuilder(300);
            sb.append("<message name=\"truststudy.setState\">\n");
            int j = 0;
            for (Player pt : players) {
                if (pt == p) {
                    continue;
                }
                sb.append("<string key=\"sharedList").append(j++).append("\" ><![CDATA[")
                        .append(r.getString("board.sharedListTitle.start")).append(pt.getName())
                        .append(r.getString("board.sharedListTitle.end")).append("]]></string>\n");
            }
            sb.append("</message>");
            sendMessage(new DEFMessage(getServerCommId(), p.getID(), sb.toString()));

        }

        if (fault == Fault.MISTAKE) {
            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all",
                    "<message name=\"highseverity\"><name>highseverity</name></message>"));
        }

    }

    private TrustStudyPlayer getHumanPlayer() {
        String clientId = experimentName + ".b";
        TrustStudyPlayer person = null;
        for (Player p : players) {
            if (p.getID().equals(clientId)) {
                person = (TrustStudyPlayer) p;
                break;
            }
        }
        return person;
    }

    private boolean playerHasGoal(Player player) {
        TrustStudyPlayer tsp = (TrustStudyPlayer) player;
        Information info = getPathInformationByPlayer(tsp);
        boolean hasGoal = false;
        Information sendersGoal = null;
        for (Information goal : goals) {
            if (goal.owner.equals(tsp.getID())) {
                sendersGoal = goal;
            }
        }
        for (Point p : info.path) {
            if (p.x == sendersGoal.col && p.y == sendersGoal.row) {
                hasGoal = true;
            }
        }
        return hasGoal;
    }

    private void processClientMessage(DEFMessage m) {

        /**
         * *******************************
         */
        // <editor-fold defaultstate="collapsed" desc="truststudy.proposalListRequest">
        if (m.name.equals("truststudy.proposalListRequest")) {
            StringBuilder sb = new StringBuilder(200);
            sb.append("<message name=\"proposalList.new\">\n");
            sb.append("<status cancelEnabled=\"true\">");
            for (int[] goalPermutation : goalPermutations) {
                sb.append("<data>");
                for (int j = 0; j < players.length; j++) {
                    if (j > 0) {
                        sb.append(", ");
                    }
                    sb.append(players[j].getName()).append(" to ")
                            .append(goals[goalPermutation[j]].name);
                }
                sb.append("</data>");
            }
            sb.append("</status>");
            sb.append("</message>\n");
            sendMessage(new DEFMessage(getServerCommId(), m.sender, sb.toString()));
            return;
        }
        // </editor-fold>

        /**
         * *******************************
         */
        // <editor-fold defaultstate="collapsed" desc="proposal.action">
        if (m.name.equals("proposal.action")) {
            String choice = ((Element) m.mRoot.getFirstChild()).getAttribute("choice");
            TrustStudyPlayer chooser = ((TrustStudyPlayer) getPlayerById(m.sender));
            if (choice.equals("propose") && !proposalSuccess) {

                if (unreadyPlayers < PLAYER_COUNT) {
                    sendMessage(new DEFMessage(getServerCommId(), m.sender,
                            "<message name=\"dialog.new\"><name>noMoreGoalChange</name></message>"));
                    sendMessage(new DEFMessage(getServerCommId(), m.sender,
                            "<message name=\"dialog.setState\">" + "<status"
                                    + " buttonEnabled=\"false\"" + " text=\""
                                    + r.getString("noMoreGoalChange") + "\"" + " width=\"200\""
                                    + " height=\"100\"" + " timer=\"1500\"" + " />"
                                    + "</message>"));

                }

                proposeOption =
                        Integer.parseInt(((Element) m.mRoot.getFirstChild()).getAttribute("value"));

                for (int j = 0; j < players.length; j++) {
                    int playerindex = 0;
                    if (j == 0) {
                        playerindex = 1;
                    }

                    sendMessage(new DEFMessage(getServerCommId(), players[j].getID(),
                            "<message name=\"truststudy.addLog\">"
                                    + (players[j].getID().equals(m.sender) ? "You"
                                            : players[playerindex].getName())
                                    // + " proposed goals"
                                    + " " + r.getString("proposedGoals")
                                    // + players[j].getName() + " to " +
                                    // goals[goalPermutations[proposeOption][j]].name
                                    + "</message>"));
                }
                if (proposeOption < 0) {
                    return;
                }
                proposalActive = true;
                chooser.proposalState = TrustStudyPlayer.ProposalStates.Proposed;
                ((TrustStudyPlayer) getPlayerById(m.sender)).proposalCount++;
                proposalStartTime = System.currentTimeMillis();
            }
            if (choice.equals("decline")) {
                chooser.proposalState = TrustStudyPlayer.ProposalStates.Declined;
                proposalActive = false;
                for (int j = 0; j < players.length; j++) {
                    int playerindex = 0;
                    if (j == 0) {
                        playerindex = 1;
                    }
                    sendMessage(new DEFMessage(getServerCommId(), players[j].getID(),
                            "<message name=\"truststudy.addLog\">"
                                    + (players[j].getID().equals(m.sender) ? "You"
                                            : players[playerindex].getName())
                                    // + " declined that proposal "
                                    + " " + r.getString("proposalDenclined") + "</message>"));
                }
                for (Player p : players) {
                    TrustStudyPlayer tsp = (TrustStudyPlayer) p;
                    if (tsp.proposalState == TrustStudyPlayer.ProposalStates.Proposed) {
                        tsp.roundSummary.append(",1").append(",").append(proposeOption).append(",")
                                .append(proposalStartTime - gameStartTime).append(",0,0,0");
                    }
                    if (tsp.proposalState == TrustStudyPlayer.ProposalStates.Declined) {
                        tsp.roundSummary.append(",0").append(",").append(proposeOption).append(",")
                                .append(proposalStartTime - gameStartTime).append(",0,1,0");
                    }
                }
            }
            if (choice.equals("accept")) {
                chooser.proposalState = TrustStudyPlayer.ProposalStates.Accepted;
            }
            if (choice.equals("confirm")) {
                chooser.proposalState = TrustStudyPlayer.ProposalStates.Confirmed;
                for (int j = 0; j < players.length; j++) {
                    int playerindex = 0;
                    if (j == 0) {
                        playerindex = 1;
                    }
                    sendMessage(new DEFMessage(getServerCommId(), players[j].getID(),
                            "<message name=\"truststudy.addLog\">"
                                    + (players[j].getID().equals(m.sender) ? "You"
                                            : players[playerindex].getName())
                                    // + " accepted that proposal"
                                    + " " + r.getString("proposalAccepted") + "</message>"));
                }
                proposalActive = false;
                proposalSuccess = true;
                for (Player p : players) {
                    TrustStudyPlayer tsp = (TrustStudyPlayer) p;
                    if (tsp.proposalState == TrustStudyPlayer.ProposalStates.Undecided) {
                        proposalActive = true;
                        proposalSuccess = false;
                    }
                }
                for (Player p : players) {
                    TrustStudyPlayer tsp = (TrustStudyPlayer) p;
                    if (tsp.proposalState == TrustStudyPlayer.ProposalStates.Proposed) {
                        tsp.roundSummary.append(",1").append(",").append(proposeOption).append(",")
                                .append(proposalStartTime - gameStartTime).append(", 0,0,0");
                    }
                    if (tsp.proposalState == TrustStudyPlayer.ProposalStates.Confirmed) {
                        tsp.roundSummary.append(",0").append(",").append(proposeOption).append(",")
                                .append(proposalStartTime - gameStartTime).append(",1,0,0");
                    }
                }
            }
            if (choice.equals("retract")) {
                chooser.proposalState = TrustStudyPlayer.ProposalStates.Retracted;
                for (int j = 0; j < players.length; j++) {
                    int playerindex = 0;
                    if (j == 0) {
                        playerindex = 1;
                    }
                    sendMessage(new DEFMessage(getServerCommId(), players[j].getID(),
                            "<message name=\"truststudy.addLog\">"
                                    + (players[j].getID().equals(m.sender) ? "You"
                                            : players[playerindex].getName())
                                    + " retracted that proposal" + "</message>"));
                }
                proposalActive = false;
                for (Player p : players) {
                    TrustStudyPlayer tsp = (TrustStudyPlayer) p;
                    if (tsp.proposalState == TrustStudyPlayer.ProposalStates.Retracted) {
                        tsp.roundSummary.append(",1").append(",").append(proposeOption).append(",")
                                .append(proposalStartTime - gameStartTime).append(",0,0,1");
                    }

                }
            }
            for (Player p : players) {
                TrustStudyPlayer tsp = (TrustStudyPlayer) p;
                StringBuilder sb = new StringBuilder(400);
                sb.append("<message name=\"proposal.setState\">\n");
                switch (tsp.proposalState) {
                    case Proposed:
                        if (proposalActive) {
                            sb.append("<button accept=\"false\" retract=\"true\"/>");
                        } else {
                            sb.append("<button accept=\"false\" retract=\"false\"/>");
                        }
                        break;
                    case Accepted:
                    case Undecided:
                        if (proposalActive) {
                            sb.append("<button accept=\"true\" decline=\"true\"/>");
                        } else {
                            sb.append("<button accept=\"false\" decline=\"false\"/>");
                        }
                        break;
                    case Confirmed:
                        if (proposalActive) {
                            sb.append("<button accept=\"false\" decline=\"false\"/>");
                        } else {
                            sb.append("<button accept=\"false\" decline=\"false\"/>");
                        }
                        break;
                    case Declined:
                        sb.append("<button accept=\"false\" decline=\"false\"/>");
                        break;
                    case Retracted:
                        sb.append("<button accept=\"false\" retract=\"false\"/>");
                        break;
                }
                sb.append("<text><![CDATA[<html>");
                for (int j = 0; j < players.length; j++) {
                    if (j > 0) {
                        sb.append(", ");
                    }
                    sb.append(players[j].getName()).append(" to ")
                            .append(goals[goalPermutations[proposeOption][j]].name);
                }
                if (tsp.proposalState == TrustStudyPlayer.ProposalStates.Accepted) {
                    sb.append(r.getString("goalsConfirm"));
                }
                sb.append("</html>]]></text>");
                if (tsp.proposalState == TrustStudyPlayer.ProposalStates.Accepted) {
                    sb.append("<acceptLabel><![CDATA[").append(r.getString("goalsAccept"))
                            .append("]]></acceptLabel>");
                }
                sb.append("<status><![CDATA[");
                for (Player p2 : players) {
                    TrustStudyPlayer tsp2 = (TrustStudyPlayer) p2;
                    sb.append(tsp2.getName());
                    sb.append(":");
                    switch (tsp2.proposalState) {
                        case Proposed:
                            sb.append(r.s("proposal.proposed"));
                            break;
                        case Accepted:
                        case Undecided:
                            sb.append(r.s("proposal.undecided"));
                            break;
                        case Confirmed:
                            sb.append(r.s("proposal.accepted"));
                            break;
                        case Declined:
                            sb.append(r.s("proposal.declined"));
                            break;
                        case Retracted:
                            sb.append(r.s("proposal.retracted"));
                            break;
                    }
                    sb.append("    ");
                }
                sb.append("]]></status>");
                sb.append("</message>\n");
                sendMessage(new DEFMessage(getServerCommId(), p.getID(), sb.toString()));
            }

            if (proposalSuccess) {

                // update state
                for (Player p : players) {
                    TrustStudyPlayer tsp = (TrustStudyPlayer) p;
                    if (tsp.proposalState == TrustStudyPlayer.ProposalStates.Proposed) {
                        // l.finer("Remembering proposal owner");
                        proposalOwner = tsp.getName();

                    }
                }
                combinationCount++;
                combinationTime = System.currentTimeMillis();

            }

            if (!proposalActive) {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(TrustStudyServer.class.getName()).log(Level.SEVERE, null, ex);
                }
                StringBuilder sb = new StringBuilder(200);
                sb.append("<message name=\"proposal.close\">\n");
                sb.append("</message>");
                sendMessage(
                        new DEFMessage(getServerCommId(), experimentName + ".all", sb.toString()));
                for (Player p : players) {
                    ((TrustStudyPlayer) p).proposalState =
                            TrustStudyPlayer.ProposalStates.Undecided;
                }
            }

            if (proposalSuccess) {

                // rearrange the proposals and update clients
                chosenGoalPermutation = proposeOption;
                for (int i = 0; i < goalPermutations[chosenGoalPermutation].length; i++) {
                    goals[goalPermutations[chosenGoalPermutation][i]].owner = players[i].getID();
                }
                sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all",
                        "<message name=\"nonmodalpopup.new\"><message timeout=\"10000\"><message title=\""
                                + r.getString("popupTitle") + "\"><![CDATA[" + r.s("postGoalHint")
                                + "]]></message></message></message>"));

                StringBuilder sb = new StringBuilder(200);
                // sending info nodes
                sb.append("<message name=\"truststudy.setState\">\n");
                for (Information info : goals) {
                    info.sharedList.clear();
                    for (Player pl : players) {
                        if (!pl.getID().equals(info.owner)) {
                            info.sharedList.add(pl.getID());
                        }
                    }
                    sb.append("<info id=\"").append(info.id)
                            .append("\" type=\"block\" subType=\"goal\" owner=\"")
                            .append(info.owner).append("\" >\n");
                    for (String s : info.sharedList) {
                        sb.append("<shared>").append(s).append("</shared>\n");
                    }
                    sb.append("</info>\n");
                }
                sb.append("</message>");
                sendMessage(
                        new DEFMessage(getServerCommId(), experimentName + ".all", sb.toString()));
                goalUnsurePlayers = players.length;
                goalsSet = true;
                sb = new StringBuilder(200);
                sb.append("<message name=\"truststudy.setState\">\n");
                sb.append(
                        "<button name=\"changeGoalsButton\" enabled=\"false\" /><button name=\"finalizeGoalsButton\" enabled=\"false\" />\n");
                sb.append("</message>");
                sendMessage(
                        new DEFMessage(getServerCommId(), experimentName + ".all", sb.toString()));

                for (Player player : players) {
                    TrustStudyPlayer tsp = (TrustStudyPlayer) player;
                    Information info = getPathInformationByPlayer(tsp);
                    Point last = info.path.get(info.path.size() - 1);
                    boolean executeEnabled = (last.x == 6 && last.y == 0);
                    sendMessage(new DEFMessage(getServerCommId(), tsp.getID(),
                            "<message name=\"truststudy.setState\"><button name=\"executeButton\" "
                                    + "enabled=\"" + Boolean.toString(executeEnabled)
                                    + "\" /></message>"));
                }

            }
        }
        // </editor-fold>

        /**
         * *******************************
         */
        // <editor-fold defaultstate="collapsed" desc="truststudy.proposalResponse">
        if (m.name.equals("truststudy.proposalResponse")) {

            return;
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="truststudy.finalizeGoals">
        if (m.name.equals("truststudy.finalizeGoals")) {
            // Change buttons
            goalUnsurePlayers--;
            if (goalUnsurePlayers == 0) {
                StringBuilder sb = new StringBuilder(200);
                sb.append("<message name=\"truststudy.setState\">\n");
                sb.append(
                        "<button name=\"changeGoalsButton\" enabled=\"false\" /><button name=\"finalizeGoalsButton\" enabled=\"false\" />\n");
                sb.append("</message>");
                sendMessage(
                        new DEFMessage(getServerCommId(), experimentName + ".all", sb.toString()));

                for (Player player : players) {
                    TrustStudyPlayer tsp = (TrustStudyPlayer) player;
                    Information info = getPathInformationByPlayer(tsp);
                    Point last = info.path.get(info.path.size() - 1);

                    boolean executeEnabled = (last.x == 6 && last.y == 0);
                    sendMessage(new DEFMessage(getServerCommId(), tsp.getID(),
                            "<message name=\"truststudy.setState\"><button name=\"executeButton\" "
                                    + "enabled=\"" + Boolean.toString(executeEnabled)
                                    + "\" /></message>"));
                }
            } else {
                StringBuilder sb = new StringBuilder(200);
                sb.append("<message name=\"truststudy.setState\">\n");
                sb.append(
                        "<button name=\"changeGoalsButton\" enabled=\"true\" /><button name=\"finalizeGoalsButton\" enabled=\"false\" />\n");
                sb.append("</message>");
                sendMessage(new DEFMessage(getServerCommId(), m.sender, sb.toString()));

            }

        }
        // </editor-fold>

        // sent from boardwindow.maybeshowpopupmenu() // or APClient during run()
        // <editor-fold defaultstate="collapsed" desc="truststudy.setShareStatus">
        if (m.name.equals("truststudy.setShareStatus")) {
            TrustStudyPlayer tsp = (TrustStudyPlayer) getPlayerById(m.sender);
            Element infoElement = (Element) m.mRoot.getFirstChild();
            int infoId = Integer.parseInt(infoElement.getAttribute("id"));
            Information info = infos.get(infoId);
            String targetId = infoElement.getAttribute("target");
            boolean isShared = Boolean.parseBoolean(infoElement.getAttribute("share"));
            sendMessage(new DEFMessage(getServerCommId(), m.sender,
                    "<message name=\"truststudy.addLog\">" + "You "
                            + (isShared ? "shared " : "unshared ") + info.name + " with "
                            + getPlayerById(targetId).getName() + "</message>"));
            sendMessage(new DEFMessage(getServerCommId(), targetId,
                    "<message name=\"truststudy.addLog\">" + getPlayerById(m.sender).getName() + " "
                            + (isShared ? "" : r.getString("sharedModifier"))
                            + r.getString("sharedVerb") + " " + info.name + " "
                            + r.getString("withYou")// + "with you"
                            + "</message>"));
            boolean sendMessage = false;
            boolean sendPathInfo = false;
            if (isShared) {
                tsp.shared++;
                if (infos.get(infoId).type.equals("damage")) {
                    tsp.damageShare++;
                }
                if (infos.get(infoId).type.equals("bonus")) {
                    tsp.bonusShare++;
                }
                shared++;
                if (!(infos.get(infoId).sharedList.contains(targetId))) {
                    infos.get(infoId).sharedList.add(targetId);
                    sendMessage = true;
                    if (info.type.equals("path")) {
                        sendPathInfo = true;
                    }
                }
            } else {
                if (infos.get(infoId).sharedList.contains(targetId)) {
                    tsp.unShared++;
                    unshared++;
                }
                infos.get(infoId).sharedList.remove(targetId);
                sendMessage = true;
            }
            // send the update share information to others
            if (sendMessage) {
                StringBuilder sb = new StringBuilder(100);
                // sending info nodes
                sb.append("<message name=\"truststudy.setState\">\n");
                sb.append("<info id=\"").append(info.id).append("\">\n");
                info.sharedList.forEach((s) -> {
                    sb.append("<shared>").append(s).append("</shared>\n");
                });
                if (sendPathInfo) {
                    info.getPath().forEach((p) -> {
                        sb.append("<location x=\"").append(p.x).append("\" y=\"").append(p.y)
                                .append("\" />\n");
                    });
                }
                sb.append("</info>\n");
                sb.append("</message>");
                sendMessage(
                        new DEFMessage(getServerCommId(), experimentName + ".all", sb.toString()));
            }
            return;
        }
        // </editor-fold>

        // sent from boardwindow.maybeshowpopupmenu()
        // <editor-fold defaultstate="collapsed" desc="truststudy.updatePath">
        if (m.name.equals("truststudy.updatePath")) {
            Element infoElement = (Element) m.mRoot.getFirstChild();
            Information info = infos.get(Integer.parseInt(infoElement.getAttribute("id")));
            info.path.clear();
            NodeList locationNodes = infoElement.getElementsByTagName("location");
            for (int i = 0; i < locationNodes.getLength(); i++) {
                Element loc = (Element) locationNodes.item(i);
                info.path.add(new Point(Integer.parseInt(loc.getAttribute("x")),
                        Integer.parseInt(loc.getAttribute("y"))));
            }
            // check whether the execute button should be enabled
            boolean executeEnabled = false;
            Point last = info.path.get(info.path.size() - 1);
            int newFoodLevel = Math.max(0, resourceLevel - info.path.size() + 1);

            executeEnabled = (last.x == 6 && last.y == 0) && goalsSet;

            sendMessage(new DEFMessage(getServerCommId(), m.sender,
                    "<message name=\"truststudy.setState\"><button name=\"executeButton\" "
                            + "enabled=\"" + Boolean.toString(executeEnabled) + "\" />"
                            + "<string key=\"foodLevel\">" + newFoodLevel + "</string>"
                            + "<string key=\"progressText\">" + newFoodLevel + " / "
                            + resourceStartLevel + "</string>" + "</message>"));
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="truststudy.executeRequest">
        if (m.name.equals("truststudy.executeRequest")) {
            unreadyPlayers--;
            TrustStudyPlayer tsp = (TrustStudyPlayer) getPlayerById(m.sender);
            tsp.finalPathTime = System.currentTimeMillis();

            for (Player player : players) {
                sendMessage(new DEFMessage(getServerCommId(), player.getID(),
                        "<message name=\"truststudy.addLog\">"
                                + (player.getID().equals(m.sender) ? "You" : player.getName())
                                + " completed "
                                + (player.getID().equals(m.sender) ? "your" : "their")
                                + " path and hit the road." + "</message>"));
            }

            /* Send the new path to everyone */
            Information pathInfo = getPathInformationByPlayer(tsp);
            for (Player p : players) {
                if (!pathInfo.sharedList.contains(p.getID())) {
                    pathInfo.sharedList.add(p.getID());
                }
            }
            StringBuilder sb = new StringBuilder(100);
            // sending info nodes
            sb.append("<message name=\"truststudy.setState\">\n");
            sb.append("<info id=\"").append(pathInfo.id).append("\">\n");
            pathInfo.sharedList.forEach((s) -> {
                sb.append("<shared>").append(s).append("</shared>\n");
            });
            pathInfo.getPath().forEach((p) -> {
                sb.append("<location x=\"").append(p.x).append("\" y=\"").append(p.y)
                        .append("\" />\n");
            });
            sb.append("</info>\n");
            sb.append("</message>");
            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", sb.toString()));
            tsp.executeTime = System.currentTimeMillis();
            if (unreadyPlayers > 0) {
                sendMessage(new DEFMessage(getServerCommId(), m.sender,
                        "<message name=\"dialog.new\"><name>executeWait</name></message>"));
                sendMessage(new DEFMessage(getServerCommId(), m.sender,
                        "<message name=\"dialog.setState\">" + "<status"
                                + " buttonEnabled=\"false\""
                                + " text=\"Waiting for others to finish...\"" + " width=\"200\""
                                + " height=\"100\"" + " />" + "</message>"));

            } else {
                missionFinishTime = System.currentTimeMillis();
                sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all",
                        "<message name=\"dialog.close\"></message>"));
                sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all",
                        "<message name=\"truststudy.execute\"></message>"));
                unreadyPlayers = players.length;
            }
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="truststudy.executeOver">
        if (m.name.equals("truststudy.executeOver")) {
            unreadyPlayers--;
            if (unreadyPlayers == 0) {
                unreadyPlayers = players.length;
                // unreadyPlayers--;
                boolean bhasGoal = false;

                for (Player player : players) {
                    // get the path.
                    TrustStudyPlayer tsp = (TrustStudyPlayer) player;
                    l.log(Level.FINE, "Processing goalcheck for {0}", tsp.getName());

                    if (tsp.getID().endsWith(".b")) {
                        bhasGoal = playerHasGoal(player);
                    }
                }
                if (!bhasGoal) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("<message name=\"bribemanager.new\">");

                    sb.append(makeString("mistakeMessage", "mistake.message"));
                    sb.append(makeString("bribeAcceptLabel", "bribe.acceptLabel"));
                    sb.append(makeString("bribeRejectLabel", "bribe.rejectLabel"));
                    sb.append(makeString("bribeResponseMessage", "mistake.responseMessage"));
                    sb.append(makeString("bribeNoResponseLabel", "bribe.noResponseLabel"));
                    sb.append(makeString("bribeMonetaryResponseLabel",
                            "bribe.monetaryResponseLabel"));
                    sb.append(makeString("bribeApologyLabel", "bribe.apologyLabel"));
                    sb.append(makeString("bribeApologyMessage", "bribe.apologyMessage"));
                    sb.append(makeString("bribeTextResponseLabel", "bribe.textResponseLabel"));
                    sb.append(makeString("bribeMonetaryResponseMessage",
                            "bribe.monetaryResponseMessage"));
                    sb.append(makeString("bribeTextResponseMessage", "bribe.textResponseMessage"));

                    sb.append("</message>");
                    sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all",
                            sb.toString()));
                } else {
                    unreadyPlayers = 0;
                    showEndOfStageDialogs();
                }
            }
        }
        // </editor-fold>

        if (m.name.equals(DialogFactory.getName(DialogType.ENDSTAGE) + ".submit")) {
            NodeList formData = m.mRoot.getElementsByTagName("data");

            for (int i = 0; i < formData.getLength(); i++) {
                Element item = (Element) formData.item(i);
                String key = item.getAttribute("key");
                if (key.equals(MistakeOrBribePage.getChoiceText())) {
                    behavioralResponse = item.getTextContent();
                } else if (key.equals(MistakeOrBribePage.getConfrontResponseText())) {
                    confrontResponse = item.getTextContent();
                } else {
                    // future data
                }
            }

            stageEnd();
        }

        if (m.name.equals(DialogFactory.getName(DialogType.ENDGAME) + ".submit")) {
            NodeList formData = m.mRoot.getElementsByTagName("data");

            for (int i = 0; i < formData.getLength(); i++) {
                Element item = (Element) formData.item(i);
                String key = item.getAttribute("key");
                if (key.equals(ParticipationPage.getParticipationText())) {
                    participationResponse = item.getTextContent();
                } else {
                    // future data
                }
            }

            endGame();
        }

        if (m.name.equals(DialogFactory.getName(DialogType.REPAIR) + ".submit")) {
            NodeList formData = m.mRoot.getElementsByTagName("data");

            for (int i = 0; i < formData.getLength(); i++) {
                Element item = (Element) formData.item(i);
                String key = item.getAttribute("key");
                TrustStudyPlayer person = getHumanPlayer();
                if (key.equals("two")) {
                    person.reparationString1 = item.getTextContent();
                } else if (key.equals("three")) {
                    person.reparationString2 = item.getTextContent();
                } else {

                }
            }

            showEndOfGameDialogs();
        }

        // <editor-fold defaultstate="collapsed" desc="bribemanager.closed">
        if (m.name.equals("bribemanager.closed")) {
            unreadyPlayers--;
            TrustStudyPlayer tsp = (TrustStudyPlayer) getPlayerById(m.sender);
            Element actionElement = (Element) m.mRoot.getFirstChild();
            String response = actionElement.getAttribute("response");
            String responseString = null;
            if (actionElement.getFirstChild() != null) {
                responseString = actionElement.getFirstChild().getTextContent();
            }
            if (response.equals("reject")) {
                tsp.tookBribe = false;
            } else {
                tsp.tookBribe = true;
                tsp.reparation = response;
                if (tsp.reparation.equals("acceptMonetaryResponse")) {
                    bribeshare = Integer.parseInt(responseString);
                } else if (!tsp.reparation.equals("acceptNoResponse")) {
                    tsp.reparationString1 = responseString;
                }
            }

            if (unreadyPlayers == 0) {
                sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all",
                        "<message name=\"dialog.close\"></message>"));
                showEndOfGameDialogs();
            } else {
                sendMessage(new DEFMessage(getServerCommId(), m.sender,
                        "<message name=\"dialog.new\"><name>bribeResponseWait</name></message>"));
                sendMessage(new DEFMessage(getServerCommId(), m.sender,
                        "<message name=\"dialog.setState\">" + "<status"
                                + " buttonEnabled=\"false\"" + " text=\"Waiting...\""
                                + " width=\"200\"" + " height=\"100\"" + " />" + "</message>"));
            }
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="mistakemanager.closed">
        if (m.name.equals("mistakemanager.closed")) {
            unreadyPlayers--;
            // TrustStudyPlayer tsp = (TrustStudyPlayer) getPlayerById(m.sender);
            Element actionElement = (Element) m.mRoot.getFirstChild();
            String response = actionElement.getAttribute("response");
            String responseString = null;
            if (actionElement.getFirstChild() != null) {
                responseString = actionElement.getFirstChild().getTextContent();
            }
            if (response.equals("reject")) {
                // tsp.tookBribe = false;
            } else {
                // tsp.tookBribe = true;
                madeMistake = true;
                mistakeReparation = response;
                if (mistakeReparation.equals("acceptMonetaryResponse")) {
                    mistakeshare = Integer.parseInt(responseString);
                } else if (!mistakeReparation.equals("acceptNoResponse")) {
                    mistakeReparationString = responseString;
                }
            }

            if (unreadyPlayers == 0) {
                sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all",
                        "<message name=\"dialog.close\"></message>"));
                showEndOfStageDialogs();
            } else {
                sendMessage(new DEFMessage(getServerCommId(), m.sender,
                        "<message name=\"dialog.new\"><name>bribeResponseWait</name></message>"));
                sendMessage(new DEFMessage(getServerCommId(), m.sender,
                        "<message name=\"dialog.setState\">" + "<status"
                                + " buttonEnabled=\"false\"" + " text=\"Waiting...\""
                                + " width=\"200\"" + " height=\"100\"" + " />" + "</message>"));
            }
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="apologyMoneyAcceptOrReject">
        if (m.name.equals("apologyMoneyAccept") || m.name.equals("apologyMoneyReject")) {
            unreadyPlayers--;
            ((TrustStudyPlayer) getPlayerById(m.sender)).apologyAccept = m.name;
            if (m.name.equals("apologyMoneyReject")) {
                bribeShareAmount = 0;
            } else {
                bribeAccepted = true;
            }
            if (unreadyPlayers == 0) {
                showEndOfGameDialogs();
            } else {
                sendMessage(new DEFMessage(getServerCommId(), m.sender,
                        "<message name=\"dialog.new\"><name>deceptionWait</name></message>"));
                sendMessage(new DEFMessage(getServerCommId(), m.sender,
                        "<message name=\"dialog.setState\">" + "<status"
                                + " buttonEnabled=\"false\""
                                + " text=\"Waiting for others to finish...\"" + " width=\"200\""
                                + " height=\"100\"" + " />" + "</message>"));
            }
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="dialog.closed">
        if (m.name.equals("dialog.closed")) {
            NodeList names = m.mRoot.getElementsByTagName("name");
            if (names.getLength() >= 0) {
                String dialogName = names.item(0).getFirstChild().getNodeValue();
                l.log(Level.FINER, "Got dialog.closed for {0}", dialogName);
                if (dialogName.equals("deception")) {
                    unreadyPlayers--;
                    if (unreadyPlayers == 0) {
                        showEndOfGameDialogs();
                    } else {
                        sendMessage(new DEFMessage(getServerCommId(), m.sender,
                                "<message name=\"dialog.new\"><name>deceptionWait</name></message>"));
                        sendMessage(new DEFMessage(getServerCommId(), m.sender,
                                "<message name=\"dialog.setState\">" + "<status"
                                        + " buttonEnabled=\"false\""
                                        + " text=\"Waiting for others to finish...\""
                                        + " width=\"200\"" + " height=\"100\"" + " />"
                                        + "</message>"));
                    }
                }

                if (dialogName.equals("finalscore")) {
                    sendMessage(new DEFMessage(getServerCommId(), m.sender,
                            "<message name=\"launchUrl\"><url>" + r.getString("surveyurl")
                                    + "</url></message>"));
                    sendMessage(new DEFMessage(getServerCommId(), m.sender,
                            "<message name=\"dialog.new\"><name>aftersurvey</name></message>"));
                    sendMessage(new DEFMessage(getServerCommId(), m.sender,
                            "<message name=\"dialog.setState\">" + "<status"
                                    + " name=\"aftersurvey\"" + " buttonText=\"Complete\""
                                    + " buttonEnabled=\"true\"" + " width=\"200\""
                                    + " height=\"200\"" + " ><![CDATA[" + r.getString("endmessage")
                                    + "]]></status>" + "</message>"));
                }
                if (dialogName.equals("aftersurvey")) {
                    sendMessage(new DEFMessage(getServerCommId(), m.sender,
                            "<message name=\"disconnect\">" + r.getString("gameOver")
                                    + "</message>"));
                }
            }
        }
        // </editor-fold>

    }

    private void endGame() {
        computeFinalScore();
        sendPlayerSummary();
        displayExperimenterSummary();
    }

    private void computeFinalScore() {
        boolean rhasGoal = false;
        boolean bhasGoal = false;

        // update the score
        results = new TrustStudyResults();

        // for each player
        for (Player player : players) {
            // get the path.
            TrustStudyPlayer tsp = (TrustStudyPlayer) player;
            l.log(Level.FINE, "Processing score for {0}", tsp.getName());
            Information pathInfo = getPathInformationByPlayer(player);
            // iterate over the blocks and update the score
            tsp.currencyPoints = 0;
            tsp.distanceTravelled = 0;

            if (tsp.getID().endsWith(".a")) {
                tsp.resourceRemaining = resourceLevel2;
            } else {
                tsp.resourceRemaining = resourceLevel;
            }

            int hazardsCrossed = 0;
            int bonusCrossed = 0;

            for (Point point : pathInfo.path) {
                for (Information info : infos) {

                    if (info.col == point.x && info.row == point.y) {
                        if (tsp.getID().equals(info.owner)) {
                            tsp.currencyPoints += info.coinAdd;
                        }
                        tsp.resourceRemaining += info.scoreAdd;
                        if (info.type.equals("payoff")) {
                            tsp.currencyCollected++;
                        }
                        l.log(Level.FINE, "Score after {0} = {1}",
                                new Object[] {info.name, tsp.resourceRemaining});
                    }
                    if (info.col == point.x && info.row == point.y && info.type.equals("damage")) {
                        hazardsCrossed++;
                    }
                    if (info.col == point.x && info.row == point.y && info.type.equals("bonus")) {
                        bonusCrossed++;
                    }
                    if (tsp.getID().endsWith(".a")) {
                        rhasGoal = playerHasGoal(tsp);
                    }
                    if (tsp.getID().endsWith(".b")) {
                        bhasGoal = playerHasGoal(tsp);
                    }

                }
                tsp.distanceTravelled++;
            }
            tsp.distanceTravelled--;

            tsp.resourceRemaining -= tsp.distanceTravelled * costs.getDistance();
            tsp.resourceRemaining -= tsp.bonusShare * costs.getBonusShare();
            tsp.resourceRemaining -= tsp.damageShare * costs.getDamageShare();

            if (fault == Fault.BRIBE && tsp.getID().endsWith("a")
                    && stageType == StageType.VIOLATION) {
                tsp.currencyPoints += costs.getBribeCurrencyPay();
                tsp.resourceRemaining =
                        Math.max(0, tsp.resourceRemaining - costs.getBribeResourceCost());
            }

            if (bribeAccepted && tsp.getID().endsWith("a") && stageType == StageType.REPAIR) {
                tsp.currencyPoints -= bribeShareAmount;
            }

            if (bribeAccepted && tsp.getID().endsWith("b") && stageType == StageType.REPAIR) {
                tsp.currencyPoints += bribeShareAmount;
            }

            if (tsp.getID().endsWith(".b")) {
                if (!bhasGoal) {
                    tsp.resourceRemaining -= costs.getNoGoal();
                }
                if (madeMistake) {
                    tsp.currencyPoints -= mistakeshare;
                }

                if (hasBehavioralResponse) {
                    if (behavioralResponse.equals(MistakeOrBribePage.getPickUpSlackText())) {
                        tsp.currencyPoints -= MistakeOrBribePage.getSlackValue();
                    }
                }
            }
            if (tsp.getID().endsWith(".a")) {
                if (madeMistake) {
                    tsp.currencyPoints += mistakeshare;
                }
                if (!rhasGoal) {
                    tsp.resourceRemaining -= costs.getNoGoal();
                }
                if (hasBehavioralResponse) {
                    if (behavioralResponse.equals(MistakeOrBribePage.getRetaliateText())) {
                        tsp.resourceRemaining -= MistakeOrBribePage.getRetaliateValue();
                    }

                    if (behavioralResponse.equals(MistakeOrBribePage.getPickUpSlackText())) {
                        tsp.resourceRemaining += 100;
                    }
                }
            }
            final int bonusCrossedResult = bonusCrossed;
            final int hazardsCrossedResult = hazardsCrossed;
            TrustStudyEntry e = new TrustStudyEntry(stage) {
                {
                    setApologyAccepted((tsp.apologyAccept != null
                            && tsp.apologyAccept.equals("apologyMoneyAccept") ? "true" : "false"));

                    setBlueMadeMistake(madeMistake ? "true" : "false");
                    setBlueMistakeRepType(mistakeReparation);
                    setBlueMistakeResponse(mistakeReparationString);
                    setBlueTookBribe((tsp.tookBribe ? "true" : "false"));
                    setBonusShared(Integer.toString(tsp.bonusShare));
                    setCurrencyEarned(Integer.toString(tsp.currencyPoints));
                    setDistanceTravelled(Integer.toString(tsp.distanceTravelled));
                    setResourceBonusCrossed(Integer.toString(bonusCrossedResult));
                    setResourceLevel(Integer
                            .toString(tsp.getID().endsWith(".b") ? resourceLevel : resourceLevel2));
                    setResourceRemaining(Integer.toString(tsp.resourceRemaining));
                    setHazardsCrossed(Integer.toString(hazardsCrossedResult));
                    setHazardsShared(Integer.toString(tsp.damageShare));
                    setMistakeOrBribe(stageType == StageType.VIOLATION
                            ? (fault == Fault.MISTAKE ? "Mistake" : "Bribe")
                            : "");
                    setBehavioralResponse(behavioralResponse);
                    setParticipationResponse(participationResponse);
                    setRedRepType(
                            (stageType == StageType.REPAIR ? tsp.getReparationType().toString()
                                    : ""));
                    setBlueBribeRepType((tsp.reparation.isEmpty() ? "" : tsp.reparation));
                    setRepString1(tsp.reparationString1);
                    setRepString2(tsp.reparationString2);
                    setStageId((tsp.getID().endsWith(".b") ? "Blue" : "Red"));
                    setTimeForFinalPath(Long.toString(tsp.finalPathTime - gameStartTime));
                    setTotalCurrency(Integer.toString(tsp.currencyPoints
                            + (tsp.getID().endsWith(".b") ? tallyCurrency : tallyCurrency2)));
                    setConfrontResponse(tsp.getID().endsWith(".b") ? confrontResponse : "N/A");
                }
            };

            results.getEntries().add(e);

        }

        listener.Report(results);
    }

    private void sendPlayerSummary() {
        for (Player player : players) {
            int tempCurrencyTotal = 0;
            int tempResourceTotal = 0;
            TrustStudyPlayer tsp = (TrustStudyPlayer) player;
            StringBuilder othersummary = new StringBuilder();
            for (Player otherplayer : players) {
                if (otherplayer != player) {
                    tempCurrencyTotal =
                            ((TrustStudyPlayer) otherplayer).currencyPoints + tallyCurrency2;
                    tempResourceTotal = ((TrustStudyPlayer) otherplayer).resourceRemaining
                            + tsp.resourceRemaining;
                    othersummary.append(MessageFormat.format(r.getString("othersummary"),
                            otherplayer.getName(),
                            ((TrustStudyPlayer) otherplayer).resourceRemaining, resourceStartLevel,
                            ((TrustStudyPlayer) otherplayer).currencyPoints, tempCurrencyTotal));
                }
            }
            StringBuilder totalsummary = new StringBuilder();
            totalsummary.append(MessageFormat.format(r.getString("totalsummary"), tempResourceTotal,
                    resourceStartLevel * 2));

            sendMessage(new DEFMessage(getServerCommId(), player.getID(),
                    "<message name=\"dialog.new\"><name>finalscore</name></message>"));
            sendMessage(new DEFMessage(getServerCommId(), player.getID(),
                    "<message name=\"dialog.setState\">" + "<status" + " name=\"finalscore\""
                            + " buttonEnabled=\"true\"" + " buttonText=\""
                            + r.getString("gotosurveylabel") + "\"" + " width=\"300\""
                            + " height=\"300\">" + "<![CDATA["
                            + MessageFormat.format(r.getString("summary"), player.getName(),
                                    tsp.resourceRemaining, resourceStartLevel, tsp.currencyPoints,
                                    tsp.currencyPoints + tallyCurrency, othersummary.toString(),
                                    totalsummary.toString())
                            + "]]>" + "</status>" + "</message>"));
        }
    }

    private void displayExperimenterSummary() {
        final StringBuilder summary = new StringBuilder(5000);
        summary.append("<html><body>");
        summary.append("Game duration: ")
                .append(getTimeString(((System.currentTimeMillis() - gameStartTime) / 1000)))
                .append("<br>");
        summary.append("Time for team goal acceptance: ")
                .append(getTimeString(((combinationTime - gameStartTime) / 1000))).append("<br>");
        summary.append("Team goal selector: ").append(proposalOwner).append("<br>");
        summary.append("Team goal iterations: ").append(combinationCount).append("<br>");
        summary.append("Shared: ").append(shared).append(" pieces<br>");
        summary.append("Un-shared: ").append(unshared).append(" pieces<br>");
        if (stageType == StageType.VIOLATION) {
            summary.append("Severity: ").append(fault == Fault.MISTAKE ? "Mistake" : "Bribe")
                    .append("<br>");
        }
        summary.append("<hr>");
        summary.append("<hr>");
        summary.append("<h3>Per player summary</h3>");
        summary.append("<hr>");
        for (Player p : players) {
            TrustStudyPlayer tsp = (TrustStudyPlayer) p;
            // HACK
            summary.append("Name: ").append(tsp.getName()).append("<br>");
            summary.append("Time for final path: ")
                    .append(getTimeString((tsp.finalPathTime - gameStartTime) / 1000))
                    .append("<br>");
            summary.append("Distance travelled: ").append(tsp.distanceTravelled).append("<br>");
            summary.append(capitalize(resourceLabel)).append(" Remaining: ")
                    .append(tsp.resourceRemaining).append("<br>");
            summary.append("Shared: ").append(tsp.shared).append("<br>");
            summary.append("Unshared: ").append(tsp.unShared).append("<br>");
            if (stageType == StageType.REPAIR) {
                summary.append("Reparation Type: ").append(tsp.getReparationType().name())
                        .append("<br>");
                summary.append("Reparation Response 1: ").append(tsp.reparationString1)
                        .append("<br>");
                summary.append("Reparation Response 1: ").append(tsp.reparationString2)
                        .append("<br>");
            }
            if (stageType == StageType.REVENGE) {
                summary.append("Bribe? ").append(tsp.tookBribe).append("<br>");
                summary.append("Blue Reparation: ")
                        .append(tsp.reparation == null ? "NA" : tsp.reparation).append("<br>");
            }
            summary.append(capitalize(currencyLabel)).append(" collected: ")
                    .append(tsp.currencyCollected).append("<br>");
            int tempCoinTotal = 0;
            if (tsp.getID().endsWith(".a")) {
                tempCoinTotal = tallyCurrency2 + tsp.currencyPoints;
            } else {
                tempCoinTotal = tallyCurrency + tsp.currencyPoints;
            }
            summary.append(capitalize(currencyLabel)).append(" points earned: ")
                    .append(tsp.currencyPoints).append("<br>");
            summary.append("Total ").append(currencyLabel.toLowerCase()).append(" points earned: ")
                    .append(tempCoinTotal).append("<br>");
            summary.append("<hr>");
        }
        summary.append("<hr>");
        summary.append("</body></html>");

        final Thread experimentThread = Thread.currentThread();

        new Thread(() -> {
            JOptionPane.showMessageDialog(manager, summary.toString(),
                    "Player:" + experimenterNumber, JOptionPane.INFORMATION_MESSAGE);
            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all",
                    "<message name=\"dialog.close\"></message>"));
            gameOver = true;
            experimentThread.interrupt();
        }).start();
    }

    private String makeString(String clientKey, String resourceKey) {
        return "<string key=\"" + clientKey + "\"><![CDATA[" + r.getString(resourceKey)
                + "]]></string>\n";
    }

    private String capitalize(String input) {
        input = input.toLowerCase();
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }

    private String getTimeString(long i) {
        int hours = (int) (i / 3600);
        int minutes = (int) (i % 3600) / 60;
        int seconds = (int) i % 60;

        return "" + hours + ":" + minutes / 10 + minutes % 10 + ":" + seconds / 10 + seconds % 10;
    }

    private Information getPathInformationByPlayer(Player player) {

        for (Information info : infos) {
            if (info.type.equals("path") && info.owner.equals(player.getID())) {
                return info;
            }
        }
        return null;

    }

    private void showEndOfStageDialogs() {
        if (unreadyPlayers == 0) {
            TrustStudyPlayer tsp = getHumanPlayer();

            boolean notRepairAndGoal = stageType != StageType.REPAIR && playerHasGoal(tsp);

            if (notRepairAndGoal || hasBehavioralResponse) {
                StringBuilder msg = new StringBuilder("<message name=\""
                        + DialogFactory.getName(DialogType.ENDSTAGE) + ".new\">");
                msg.append("<repair>" + Boolean.toString(notRepairAndGoal) + "</repair>");
                msg.append("<behavior>" + Boolean.toString(hasBehavioralResponse) + "</behavior>");
                msg.append("<mistake>" + Boolean.toString(fault == Fault.MISTAKE) + "</mistake>");
                msg.append("</message>");
                sendMessage(new DEFMessage(getServerCommId(), tsp.getID(), msg.toString()));
            } else {
                stageEnd();
            }
        }
    }

    private void showEndOfGameDialogs() {
        //sleep added for local testing. 
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
            TrustStudyPlayer tsp = getHumanPlayer();
            boolean lastStage = (nextStage == null);
            if (lastStage) {
                StringBuilder msg = new StringBuilder("<message name=\""+DialogFactory.getName(DialogType.ENDGAME)+".new\">");
                msg.append("</message>");
                sendMessage(new DEFMessage(getServerCommId(), tsp.getID(), msg.toString()));
            } else {
                endGame();
            }
    }

    private void stageEnd() {
        if (unreadyPlayers == 0) {
            TrustStudyPlayer tsp = getHumanPlayer();
            unreadyPlayers = players.length;

            switch (stageType) {
                case NORMAL:
                    showEndOfGameDialogs();
                    break;
                case VIOLATION:
                    unreadyPlayers--; // -1 because computer is not going to respond

                    sendMessage(new DEFMessage(getServerCommId(), tsp.getID(),
                            "<message name=\"dialog.new\"><name>deception</name></message>"));
                    sendMessage(new DEFMessage(getServerCommId(), tsp.getID(),
                            "<message name=\"dialog.setState\">" + "<status"
                                    + " buttonEnabled=\"true\"" + " buttonText=\"OK\""
                                    + " width=\"550\"" + " height=\"200\""
                                    + " ><![CDATA[<html><body>"
                                    + (fault == Fault.MISTAKE
                                            ? MessageFormat.format(r.getString("mistake"),
                                                    auto.getName())
                                            : MessageFormat.format(r.getString("bribe"),
                                                    auto.getName()))
                                    + "</body></html>]]></status>" + "</message>"));
                    break;
                case REPAIR:
                    unreadyPlayers--;
                    Integer selection = 0;
                    // always use same repair strategy in sequential repair stages
                    TrustStudyServer previousStage = (TrustStudyServer) this.previousStage;
                    if (previousStage != null && previousStage.getStageType() == StageType.REPAIR) {
                        selection = previousStage.getHumanPlayer()
                                .getReparationType() == ReparationType.MaxPerformance
                                        ? 0
                                        : 1;
                    } else {
                        selection = getRandomNumber();
                    }
                    switch (selection) {
                        case 0: // max performance
                            tsp.setReparationType(ReparationType.MaxPerformance);
                            resourceLevel += 50;
                            resourceLevel2 += 50;
                            break;
                        case 1: // show concern
                            tsp.setReparationType(ReparationType.ShowConcern);
                            break;
                    }

                    boolean showconcern = selection == 1 ? true : false;
                    StringBuilder msg = new StringBuilder("<message name=\""+DialogFactory.getName(DialogType.REPAIR)+".new\">");
                    msg.append("<showconcern>" + Boolean.toString(showconcern) + "</showconcern>");
                    msg.append("</message>");
                    sendMessage(new DEFMessage(getServerCommId(), tsp.getID(), msg.toString()));
                    break;
                case REVENGE:
                    StringBuilder sb = new StringBuilder();
                    sb.append("<message name=\"bribemanager.new\">");
                    sb.append(makeString("bribeMessage", "bribe.message"));
                    sb.append(makeString("bribeAcceptLabel", "bribe.acceptLabel"));
                    sb.append(makeString("bribeRejectLabel", "bribe.rejectLabel"));
                    sb.append(makeString("bribeResponseMessage", "bribe.responseMessage"));
                    sb.append(makeString("bribeNoResponseLabel", "bribe.noResponseLabel"));
                    sb.append(makeString("bribeMonetaryResponseLabel",
                            "bribe.monetaryResponseLabel"));
                    sb.append(makeString("bribeApologyLabel", "bribe.apologyLabel"));
                    sb.append(makeString("bribeApologyMessage", "bribe.apologyMessage"));
                    sb.append(makeString("bribeTextResponseLabel", "bribe.textResponseLabel"));
                    sb.append(makeString("bribeMonetaryResponseMessage",
                            "bribe.monetaryResponseMessage"));
                    sb.append(makeString("bribeTextResponseMessage", "bribe.textResponseMessage"));
                    sb.append("</message>");
                    sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all",
                            sb.toString()));
                    break;
            }
        }
    }

    public int getRandomNumber() {
        return rand.nextInt(2);
    }
}


class Information {

    int id;
    int row;
    int col;
    String name;
    String icon;
    String owner;
    String type;
    ArrayList<String> sharedList;
    public ArrayList<Point> path;
    float coinAdd;
    float scoreAdd;
    String description = null;

    public Information(int id, int row, int col, String name, String icon, String owner,
            String coinAdd, String scoreAdd, String type) {
        this.id = id;
        this.row = row;
        this.col = col;
        this.name = name;
        this.icon = icon;
        this.owner = owner;
        this.type = type;
        this.coinAdd = (coinAdd == null || coinAdd.length() == 0 ? 0f : Float.parseFloat(coinAdd));
        this.scoreAdd =
                (scoreAdd == null || scoreAdd.length() == 0 ? 0f : Float.parseFloat(scoreAdd));
        this.sharedList = new ArrayList<>(TrustStudyServer.PLAYER_COUNT);

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Information(int id, String name, String owner) {
        this.id = id;
        this.name = name;
        this.owner = owner;
        this.type = "path";
        this.sharedList = new ArrayList<>(TrustStudyServer.PLAYER_COUNT);
        this.path = new ArrayList<>(25);
    }

    public ArrayList<Point> getPath() {
        return path;
    }
}


class Costs {
    @JacksonXmlProperty
    private int Distance = 1;
    @JacksonXmlProperty
    private int BonusShare = 5;
    @JacksonXmlProperty
    private int DamageShare = 5;
    @JacksonXmlProperty
    private int BribeResourceCost = 100;
    @JacksonXmlProperty
    private int NoGoal = 100;
    @JacksonXmlProperty
    private int BribeShareAmount = 10;
    @JacksonXmlProperty
    private int BribeCurrencyPay = 100;

    /**
     * @return the Distance
     */
    public int getDistance() {
        return Distance;
    }

    /**
     * @param Distance the Distance to set
     */
    public void setDistance(int Distance) {
        this.Distance = Distance;
    }

    /**
     * @return the BonusShare
     */
    public int getBonusShare() {
        return BonusShare;
    }

    /**
     * @param BonusShare the BonusShare to set
     */
    public void setBonusShare(int BonusShare) {
        this.BonusShare = BonusShare;
    }

    /**
     * @return the DamageShare
     */
    public int getDamageShare() {
        return DamageShare;
    }

    /**
     * @param DamageShare the DamageShare to set
     */
    public void setDamageShare(int DamageShare) {
        this.DamageShare = DamageShare;
    }

    /**
     * @return the BribeResourceCost
     */
    public int getBribeResourceCost() {
        return BribeResourceCost;
    }

    /**
     * @param BribeResourceCost the BribeResourceCost to set
     */
    public void setBribeResourceCost(int BribeResourceCost) {
        this.BribeResourceCost = BribeResourceCost;
    }

    /**
     * @return the NoGoal
     */
    public int getNoGoal() {
        return NoGoal;
    }

    /**
     * @param NoGoal the NoGoal to set
     */
    public void setNoGoal(int NoGoal) {
        this.NoGoal = NoGoal;
    }

    /**
     * @return the BribeShareAmount
     */
    public int getBribeShareAmount() {
        return BribeShareAmount;
    }

    /**
     * @param BribeShareAmount the BribeShareAmount to set
     */
    public void setBribeShareAmount(int BribeShareAmount) {
        this.BribeShareAmount = BribeShareAmount;
    }

    /**
     * @return the BribeCurrencyPay
     */
    public int getBribeCurrencyPay() {
        return BribeCurrencyPay;
    }

    /**
     * @param BribeCurrencyPay the BribeCurrencyPay to set
     */
    public void setBribeCurrencyPay(int BribeCurrencyPay) {
        this.BribeCurrencyPay = BribeCurrencyPay;
    }
}
