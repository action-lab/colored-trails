/*
 * To change this template, choose Tools | Templates and open the template in the editor.
 */
package edu.ucf.ist.truststudy;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.SwingUtilities;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import edu.ucf.ist.def.AbstractClient;
import edu.ucf.ist.def.DEFMessage;
import edu.ucf.ist.def.L;
import edu.ucf.ist.def.NonModalPopup;
import edu.ucf.ist.def.SimpleClientDialog;
import edu.ucf.ist.dialogs.DialogWizard;
import edu.ucf.ist.dialogs.PageFactory;
import edu.ucf.ist.truststudy.dialogs.DialogFactory;
import edu.ucf.ist.truststudy.dialogs.DialogFactory.DialogType;
import edu.ucf.ist.truststudy.dialogs.endofgame.EndGameDialogFactory;
import edu.ucf.ist.truststudy.dialogs.endofstage.EndStageDialogFactory;
import edu.ucf.ist.truststudy.dialogs.repair.RepairDialogFactory;

/**
 *
 * @author culturelab
 */
public class TrustStudyClient extends AbstractClient {

    private static Logger l = L.mcl(TrustStudyClient.class, Level.FINER);
    protected BoardWindow bw;
    protected List<Point> path;
    protected ProposalListDialog pld;
    protected ProposalDialog pd;
    protected ArrayList<InformationContainer> infos;
    protected String[] partnerIDs;
    protected String[] partnerNames;
    protected JList[] partnerSharedInfoLists;
    protected String selfName;
    protected Color[] partnerColors;
    // {x,y}
    // protected int[][] partnerOffsets;
    protected int offsetX;
    protected int offsetY;
    private Color myColor;

    @Override
    protected void postInit() {

        path = Collections.synchronizedList(new ArrayList<>(100));
        infos = new ArrayList<>(40);
        pd = new ProposalDialog(bw, this);
    }
    DialogWizard dialog;
    void showDialog(PageFactory factory){
        try {
            dialog =new DialogWizard(bw, factory);
            dialog.setOnSubmit(() -> {
                StringBuilder msg =new StringBuilder("<message name=\""+factory.getName()+".submit\">");
                DialogWizard.Data data = dialog.getFormData();
                data.forEach((key, value) -> {
                    msg.append("<data key=\"").append(key).append("\">").append(value).append("</data>");
                });
                msg.append("</message>");
                this.sendMessage(new DEFMessage(this.getClientId(), this.getServerId(), msg.toString()));
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        dialog.setVisible(false);
                        dialog.dispose();   
                    }
                });
            });
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    dialog.setVisible(true);  
                }
            });
            
        } catch (Exception e) {
            l.info(e.toString());
            StringBuilder msg = new StringBuilder("<message name=\""+factory.getName()+".submit\">");
            msg.append("</message>");
            this.sendMessage(new DEFMessage(this.getClientId(), this.getServerId(), msg.toString()));
        }
    }

    @Override
    public void processMessage(DEFMessage defMessage) {

        // <editor-fold defaultstate="collapsed" desc="truststudy.init">
        if (defMessage.name.equals("truststudy.init")) {
            // Process the partnerIDs first
            l.finer("Initializing the client.");
            NodeList initNodeList = defMessage.mRoot.getElementsByTagName("init");
            if (initNodeList.getLength() > 0) {
                Element init = (Element) initNodeList.item(0);
                bw = new BoardWindow(jh, this);
                partnerSharedInfoLists = new JList[] { bw.jListShared0 /* , bw.jListShared1 */ }; // TrustStudyServer.PLAYER_COUNT
                bw.initBoard(Integer.parseInt(init.getAttribute("width")),
                        Integer.parseInt(init.getAttribute("height")));
                bw.setLocation(0, 0);
                // bw.setVisible(true);
                selfName = init.getAttribute("name");
                // offsetX = Integer.parseInt(init.getAttribute("offsetX"));
                // offsetY = Integer.parseInt(init.getAttribute("offsetY"));

                myColor = new Color(Integer.parseInt(init.getAttribute("red")),
                        Integer.parseInt(init.getAttribute("blue")), Integer.parseInt(init.getAttribute("green")));
                NodeList partnerNodes = init.getElementsByTagName("partner");
                if (partnerNodes.getLength() > 0) {
                    // PARTNERSIZE dependent
                    partnerIDs = new String[] { ((Element) partnerNodes.item(0))
                            .getAttribute("id") /* , ((Element) partnerNodes.item(1)).getAttribute("id") */ };
                    partnerNames = new String[] { ((Element) partnerNodes.item(0))
                            .getAttribute("name") /* , ((Element) partnerNodes.item(1)).getAttribute("name") */ };
                    partnerColors = new Color[] {
                            new Color(Integer.parseInt(((Element) partnerNodes.item(0)).getAttribute("red")),
                                    Integer.parseInt(((Element) partnerNodes.item(0)).getAttribute("blue")),
                                    Integer.parseInt(((Element) partnerNodes.item(0)).getAttribute("green")))
                            /*
                             * , new Color(Integer.parseInt(((Element)
                             * partnerNodes.item(1)).getAttribute("red")), Integer.parseInt(((Element)
                             * partnerNodes.item(1)).getAttribute("blue")), Integer.parseInt(((Element)
                             * partnerNodes.item(1)).getAttribute("green")))
                             */
                    };
                    // partnerOffsets = new int[][] {
                    // { Integer.parseInt(((Element) partnerNodes.item(0)).getAttribute("offsetX")),
                    // Integer.parseInt(((Element) partnerNodes.item(0)).getAttribute("offsetY")) }
                    // ,{ Integer.parseInt(((Element)
                    // partnerNodes.item(1)).getAttribute("offsetX")), Integer.parseInt(((Element)
                    // partnerNodes.item(1)).getAttribute("offsetY")) }
                    // };
                }
            }
            return;
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="proposal.setState">
        if (defMessage.name.equals("proposal.setState")) {
            // l.finer("Got proposal.setState");
            // will be handled by ProposalDialog after this message is sent upstream
            // Notice the lack of return;
            // int proposalID =
            // Integer.parseInt(defMessage.mRoot.getFirstChild().getFirstChild().getNodeValue());
            // if(pd == null) {
            // }
            // if(!pd.isVisible()) {
            // pd.processMessage(defMessage);
            // }
            // return;
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="proposal.new">
        if (defMessage.name.equals("proposalList.new")) {
            // int proposalID =
            // Integer.parseInt(defMessage.mRoot.getFirstChild().getFirstChild().getNodeValue());
            if (pld == null || !pld.isVisible()) {
                pld = new ProposalListDialog(bw, this);
            }
            if (!pld.isVisible()) {
                pld.processMessage(defMessage);
            }
            // l.finer("pld closed");
            return;
        }
        // </editor-fold>

        /*
         * <pre> <message name="dialog.new"> <dialogID>0</dialogID> </message></pre>
         */
        // <editor-fold defaultstate="collapsed" desc="dialog.new">
        if (defMessage.name.equals("dialog.new")) {
            l.finer("Got dialog.new");
            new SimpleClientDialog(bw, this);
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="nonmodalpopup.new">
        if (defMessage.name.equals("nonmodalpopup.new")) {
            l.finer("Got nonmodalpopup.new");
            NodeList messageNodes = defMessage.mRoot.getElementsByTagName("message");
            Element messageElement = (Element) messageNodes.item(0);
            long timeout = Long.parseLong(messageElement.getAttribute("timeout"));
            String title = messageElement.getAttribute("title");
            String message = messageElement.getFirstChild().getTextContent();
            NonModalPopup nmp = new NonModalPopup(message, timeout, title);
            nmp.setVisible(true);
            return;
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="launchUrl">
        if (defMessage.name.equals("launchUrl")) {
            l.finer("Got nonmodalpopup.new");
            NodeList urlNodes = defMessage.mRoot.getElementsByTagName("url");
            Element urlElement = (Element) urlNodes.item(0);
            edu.ucf.ist.def.BareBonesBrowserLaunch.openURL(urlElement.getFirstChild().getTextContent());
            return;
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="truststudy.setState">
        if (defMessage.name.equals("truststudy.setState")) {

            // Process the block and path informations if any
            NodeList infoNodes = defMessage.mRoot.getElementsByTagName("info");
            for (int i = 0; i < infoNodes.getLength(); i++) {
                Element e = (Element) infoNodes.item(i);
                InformationContainer ic = null;
                int id = Integer.parseInt(e.getAttribute("id"));
                while (infos.size() <= id) {
                    infos.add(null);
                }
                if ((ic = infos.get(id)) == null) {
                    if ("block".equals(e.getAttribute("type"))) {
                        ic = new InformationContainer();
                    } else if ("path".equals(e.getAttribute("type"))) {
                        ic = new PathInformationContainer();
                    }
                    infos.set(id, ic);
                }
                if (e.hasAttribute("row")) {
                    ic.setRow(Integer.parseInt(e.getAttribute("row")));
                }
                if (e.hasAttribute("col")) {
                    ic.setCol(Integer.parseInt(e.getAttribute("col")));
                }
                if (e.hasAttribute("text")) {
                    ic.setText(e.getAttribute("text"));
                }
                if (e.hasAttribute("owner")) {
                    ic.setOwner(e.getAttribute("owner"));
                    ic.setColor(getColorForID(ic.getOwner()));
                }
                if (e.hasAttribute("icon")) {
                    ic.setImg(BoardWindow.createImageIcon(e.getAttribute("icon")));
                }
                if (e.hasAttribute("subType")) {
                    ic.setSubType(e.getAttribute("subType"));
                }
                NodeList nl = null;
                if ((nl = e.getElementsByTagName("description")).getLength() > 0) {
                    ic.setDescription(nl.item(0).getFirstChild().getTextContent());
                }
                if (ic instanceof PathInformationContainer) {

                    NodeList locations = e.getElementsByTagName("location");
                    PathInformationContainer pic = (PathInformationContainer) ic;
                    if (bw.myPIC == null && pic.owner.equals(getClientId())) {
                        l.finer("Got own path");
                        bw.myPIC = pic;
                        bw.myPIC.setColor(myColor);
                    }
                    if (locations.getLength() > 0) {
                        List<Point> positions = pic.getPositions();
                        positions.clear();
                        for (int j = 0; j < locations.getLength(); j++) {
                            Element locElement = (Element) locations.item(j);
                            positions.add(new Point(Integer.parseInt(locElement.getAttribute("x")),
                                    Integer.parseInt(locElement.getAttribute("y"))));
                        }
                    }
                    if (e.hasAttribute("offsetXPixel")) {
                        pic.setOffsetXPixel(Integer.parseInt(e.getAttribute("offsetXPixel")));
                    }
                    if (e.hasAttribute("shareEnabled")) {
                        ic.setShareEnabled(Boolean.parseBoolean(e.getAttribute("shareEnabled")));
                    }
                    if (e.hasAttribute("offsetYPixel")) {
                        pic.setOffsetYPixel(Integer.parseInt(e.getAttribute("offsetYPixel")));
                    }
                }

                // based on the updated information, move all the info into the right lists.
                // remove it from old lists
                ((DefaultListModel) bw.jListOwned.getModel()).removeElement(ic);
                ((DefaultListModel) bw.jListCommon.getModel()).removeElement(ic);
                ((DefaultListModel) bw.jListIncoming.getModel()).removeElement(ic);
                for (JList jl : partnerSharedInfoLists) {
                    ((DefaultListModel) jl.getModel()).removeElement(ic);
                }

                if (ic.isPublic()) {
                    ((DefaultListModel) bw.jListCommon.getModel()).addElement(ic);
                }

                // add to own list
                if (ic.owner.equals(getClientId())) {
                    ((DefaultListModel) bw.jListOwned.getModel()).addElement(ic);
                }

                // add it to shared lists
                NodeList sharedMembers = e.getElementsByTagName("shared");
                for (int j = 0; j < sharedMembers.getLength(); j++) {
                    String sharedId = sharedMembers.item(j).getFirstChild().getNodeValue();
                    int sharedIndex = getPartnerIndex(sharedId);

                    if (sharedIndex != -1) {
                        // this is our info and we are sharing it.
                        if (ic.owner.equals(getClientId())) {
                            ((DefaultListModel) partnerSharedInfoLists[sharedIndex].getModel()).addElement(ic);
                        }

                    } else {
                        // If someone is sharing this with us
                        if (sharedId.equals(getClientId())) {
                            ((DefaultListModel) bw.jListIncoming.getModel()).addElement(ic);
                        }
                    }
                }
            }

            // Process the strings
            NodeList tempList = defMessage.mRoot.getElementsByTagName("string");
            bw.setStrings(tempList);

            NodeList buttonList = defMessage.mRoot.getElementsByTagName("button");
            bw.processButtons(buttonList);

            updateBoardWindow();

            return;
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="truststudy.execute">
        if (defMessage.name.equals("truststudy.execute")) {
            bw.setEnabled(false);
            bw.doPathAnimation();
            return;
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="truststudy.addLog">
        if (defMessage.name.equals("truststudy.addLog")) {
            bw.addLog(defMessage.mRoot.getFirstChild().getTextContent());
            return;
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="bribemanager.new">
        if (defMessage.name.equals("bribemanager.new")) {
            l.finer("Got bribemanager.new");
            new BribeManager(this);
        }
        // </editor-fold>
        if (defMessage.name.equals(DialogFactory.getName(DialogType.ENDSTAGE) + ".new")){
            showDialog(new EndStageDialogFactory(
                Boolean.parseBoolean(defMessage.mRoot.getElementsByTagName("repair").item(0).getTextContent()), 
                Boolean.parseBoolean(defMessage.mRoot.getElementsByTagName("behavior").item(0).getTextContent()), 
                Boolean.parseBoolean(defMessage.mRoot.getElementsByTagName("mistake").item(0).getTextContent())
                )
            );
        }

        if (defMessage.name.equals(DialogFactory.getName(DialogType.REPAIR) + ".new")){
            showDialog(new RepairDialogFactory(
                Boolean.parseBoolean(defMessage.mRoot.getElementsByTagName("showconcern").item(0).getTextContent())
                )
            );
            }
            
        if (defMessage.name.equals(DialogFactory.getName(DialogType.ENDGAME) + ".new")){
            showDialog(new EndGameDialogFactory());
        }

        l.finer("sending message '" + defMessage.name + "' upstream.");
        super.processMessage(defMessage);
    }

    private void updateBoardWindow() {
        if (bw.isVisible()) {
            bw.jPanelBoard.repaint();
        } else {
            bw.setVisible(true);
        }
    }

    public int getPartnerIndex(String partnerID) {
        int i = 0;
        while (i < partnerIDs.length && !partnerID.equals(partnerIDs[i])) {
            i++;
        }
        if (i == partnerIDs.length) {
            return -1;
        } else {
            return i;
        }
    }

    public Color getColorForID(String id) {
        if (id.equals(getClientId())) {
            return myColor;
        } else {
            int partnerIndex = getPartnerIndex(id);
            if (partnerIndex == -1) {
                return null;
            } else {
                return partnerColors[partnerIndex];
            }
        }
    }

    String getNameForID(String id) {
        if (id.equals(getClientId())) {
            return selfName;
        } else {
            int partnerIndex = getPartnerIndex(id);
            if (partnerIndex == -1) {
                return null;
            } else {
                return partnerNames[partnerIndex];
            }
        }

    }
}
