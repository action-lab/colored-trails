/*
 * APClient stands for Agent Provocateur Client To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucf.ist.truststudy;

import edu.ucf.ist.def.AbstractClient;
import edu.ucf.ist.def.DEFMessage;
import edu.ucf.ist.def.L;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.xml.transform.TransformerException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author ganil
 */
public class TrustStudyAPClient extends AbstractClient {

    private static Logger l = L.mcl(TrustStudyAPClient.class, Level.FINER);
    private String dialogName = null;
    private boolean proposalActive;
    private long gameStart = System.currentTimeMillis();
    private int executeDelay;
    Random rand = new Random(gameStart);
    private boolean isGameStarted = false;
    private boolean isGoalFinalized = false;
    private boolean proposed = false;
    private boolean holdTheAgentProposal = false;
    private boolean canfinalizeGoals = false;
    private boolean canExecute = false;
    int ownedPathID = -1;
    int targetGoalBlock;
    int stage;
    private String[] partnerIDs;
    private String[] partnerNames;
    private Color[] partnerColors;
    private ArrayList<String> shareCommands = new ArrayList(100);
    private boolean fast = false;
    private long goalProposeWaitTimer = 10000;
    private boolean s1_g1_b1 = false;
    private boolean s1_g1_b2 = false;
    private boolean s1_g2_b1 = false;
    private boolean s1_g2_b2 = false;
    private boolean s2_g2_b1 = false;
    private boolean s2_g2_b2 = false;
    private boolean s3_g1_b1 = false;
    private boolean s3_g2_b1 = false;
    private boolean s3_g2_b1a = false;
    private boolean s3_g2_b1b = false;
    boolean highSeverity = false;

    @Override
    protected void postInit() {

        if (getClientId().contains("stage1")) {
            stage = 1;
            executeDelay = 90000;
            goalProposeWaitTimer = 25000;
        }
        if (getClientId().contains("stage2")) {
            stage = 2;
            executeDelay = 60000;
            goalProposeWaitTimer = 20000;
        }
        if (getClientId().contains("stage3")) {
            stage = 3;
            executeDelay = 45000;
            goalProposeWaitTimer = 20000;
        }
        if (getClientId().contains("stage4")) {
            stage = 4;
            executeDelay = 45000;
            goalProposeWaitTimer = 15000;
        }
        if (getClientId().contains("stage5")) {
            stage = 5;
            executeDelay = 45000;
            goalProposeWaitTimer = 15000;
        }
        if (getClientId().contains("stage6")) {
            stage = 6;
            executeDelay = 45000;
            goalProposeWaitTimer = 15000;
        }
        if (getClientId().contains("stage7")) {
            stage = 7;
            executeDelay = 45000;
            goalProposeWaitTimer = 15000;
        }
        if (fast) {
            executeDelay = 1000;
            goalProposeWaitTimer = 6000;
        }

        // executeDelay = 15000 + rand.nextInt(5000);
        final TrustStudyAPClient tsapc_this = this;
        Thread t = new Thread(new Runnable() {

            TrustStudyAPClient tsapc = tsapc_this;
            long shareDelay = 15000;

            public void run() {
                try {
                    l.finer("Waiting AGAIN");
                    while (!tsapc.isGameStarted) {
                        Thread.sleep(1000 + rand.nextInt(1000));
                    }
                    // Thread.sleep(5000 + rand.nextInt(2000));

                    // Thread.sleep((fast?100:2000) + rand.nextInt(1000));
                    // l.info("FINISHED SHARING");
                    while (!tsapc.isGoalFinalized) {
                        // if (tsapc.canfinalizeGoals) {
                        // Thread.sleep(1000 + rand.nextInt(1000));
                        // if (tsapc.canfinalizeGoals) {
                        // tsapc.sendMessageToServer("truststudy.finalizeGoals", "");
                        // l.info("FINALIZE GOAL REQUEST SENT");
                        // }
                        // }

                        Thread.sleep(1000);
                        tsapc.goalProposeWaitTimer -= 1000;
                        if (!tsapc.isGoalFinalized && !tsapc.proposed && !tsapc.holdTheAgentProposal
                                && goalProposeWaitTimer <= 0/* && goalProposeWaitTimer > -1000 */) {
                            // TODO: read option from configuration file
                            tsapc.proposed = true;
                            tsapc.sendMessage(new DEFMessage(tsapc.getClientId(),
                                    tsapc.getServerId(),
                                    "<message name=\"proposal.action\"><proposalaction choice=\"propose\" value=\""
                                            + 0 + "\" /></message>"));

                        }
                    }
                    l.info("FINISHED FINALIZING GOALS");
                    Thread.sleep(shareDelay);
                    while (shareCommands.size() > 0) {
                        tsapc.sendMessageToServer("truststudy.setShareStatus",
                                shareCommands.remove(0));
                        Thread.sleep((fast ? 200 : 3000) + rand.nextInt(4000));
                    }
                    Thread.sleep(executeDelay);
                    l.info("SENDING EXECUTE REQUEST");

                    l.finer("Sending execute for stage " + stage + " and targetGoal "
                            + targetGoalBlock);
                    String solutionPath =
                            "<location x=\"6\"  y=\"12\" /><location x=\"6\"  y=\"12\" /><location x=\"7\"  y=\"12\" /><location x=\"8\"  y=\"12\" /><location x=\"8\"  y=\"11\" /><location x=\"8\"  y=\"10\" /><location x=\"8\"  y=\"9\" /><location x=\"8\"  y=\"8\" /><location x=\"8\"  y=\"7\" /><location x=\"8\"  y=\"6\" /><location x=\"8\"  y=\"5\" /><location x=\"8\"  y=\"4\" /><location x=\"8\"  y=\"3\" /><location x=\"8\"  y=\"2\" /><location x=\"8\"  y=\"1\" /><location x=\"8\"  y=\"0\" /><location x=\"7\"  y=\"0\" /><location x=\"6\"  y=\"0\" />";

                    switch (stage) {
                        case 1: 
                            switch (targetGoalBlock) {
                                case 18: // Medicine
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + "<location x=\"6\"  y=\"11\" />"
                                            + "<location x=\"6\"  y=\"10\" />"
                                            + "<location x=\"6\"  y=\"9\" />"
                                            + "<location x=\"5\"  y=\"9\" />"
                                            + "<location x=\"4\"  y=\"9\" />"
                                            + "<location x=\"3\"  y=\"9\" />"
                                            + "<location x=\"2\"  y=\"9\" />"
                                            + "<location x=\"1\"  y=\"9\" />"
                                            + "<location x=\"1\"  y=\"8\" />"
                                            + "<location x=\"1\"  y=\"7\" />"
                                            + "<location x=\"1\"  y=\"6\" />"
                                            + "<location x=\"1\"  y=\"5\" />"
                                            + "<location x=\"1\"  y=\"4\" />"
                                            + "<location x=\"1\"  y=\"3\" />"
                                            + "<location x=\"1\"  y=\"2\" />"
                                            + "<location x=\"1\"  y=\"1\" />"
                                            + "<location x=\"1\"  y=\"0\" />"
                                            + "<location x=\"2\"  y=\"0\" />"
                                            + "<location x=\"3\"  y=\"0\" />"
                                            + "<location x=\"4\"  y=\"0\" />"
                                            + "<location x=\"5\"  y=\"0\" />"
                                            + "<location x=\"6\"  y=\"0\" />";
                                    break;
                                case 19: // Water
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + "<location x=\"7\"  y=\"12\" />"
                                            + "<location x=\"8\"  y=\"12\" />"
                                            + "<location x=\"9\"  y=\"12\" />"
                                            + "<location x=\"9\"  y=\"11\" />"
                                            + "<location x=\"9\"  y=\"10\" />"
                                            + "<location x=\"9\"  y=\"9\" />"
                                            + (tsapc.s2_g2_b1
                                                    ? "<location x=\"10\"  y=\"9\" />"
                                                            + "<location x=\"10\"  y=\"8\" />"
                                                            + "<location x=\"10\"  y=\"7\" />"
                                                    : "<location x=\"9\"  y=\"8\" />")
                                            + "<location x=\"9\"  y=\"7\" />"
                                            + "<location x=\"9\"  y=\"6\" />"
                                            + (tsapc.s2_g2_b2 ? "<location x=\"8\"  y=\"6\" />"
                                                    : "<location x=\"9\"  y=\"5\" />")
                                            + "<location x=\"8\"  y=\"5\" />"
                                            + "<location x=\"7\"  y=\"5\" />"
                                            + "<location x=\"6\"  y=\"5\" />"
                                            + "<location x=\"6\"  y=\"4\" />"
                                            + "<location x=\"6\"  y=\"3\" />"
                                            + "<location x=\"6\"  y=\"2\" />"
                                            + "<location x=\"6\"  y=\"1\" />"
                                            + "<location x=\"6\"  y=\"0\" />";
                                    break;
                                case 20: // Food
                                    solutionPath =
                                            "<location x=\"6\"  y=\"12\" /><location x=\"6\"  y=\"12\" /><location x=\"7\"  y=\"12\" /><location x=\"8\"  y=\"12\" /><location x=\"8\"  y=\"11\" /><location x=\"8\"  y=\"10\" /><location x=\"8\"  y=\"9\" /><location x=\"8\"  y=\"8\" /><location x=\"8\"  y=\"7\" /><location x=\"8\"  y=\"6\" /><location x=\"8\"  y=\"5\" /><location x=\"8\"  y=\"4\" /><location x=\"8\"  y=\"3\" /><location x=\"8\"  y=\"2\" /><location x=\"8\"  y=\"1\" /><location x=\"8\"  y=\"0\" /><location x=\"7\"  y=\"0\" /><location x=\"6\"  y=\"0\" />";
                                    break;
                            }
                            break;
                        case 2: 
                            switch (targetGoalBlock) {
                                case 18: // Medicine
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + "<location x=\"6\"  y=\"11\" />"
                                            + "<location x=\"6\"  y=\"10\" />"
                                            + (tsapc.s3_g1_b1
                                                    ? "<location x=\"6\"  y=\"9\" />"
                                                            + "<location x=\"6\"  y=\"8\" />"
                                                            + "<location x=\"6\"  y=\"7\" />"
                                                            + "<location x=\"5\"  y=\"7\" />"
                                                            + "<location x=\"4\"  y=\"7\" />"
                                                            + "<location x=\"3\"  y=\"7\" />"
                                                            + "<location x=\"3\"  y=\"6\" />"
                                                    : "<location x=\"5\"  y=\"10\" />"
                                                            + "<location x=\"4\"  y=\"10\" />"
                                                            + "<location x=\"3\"  y=\"10\" />"
                                                            + "<location x=\"2\"  y=\"10\" />"
                                                            + "<location x=\"2\"  y=\"9\" />"
                                                            + "<location x=\"2\"  y=\"8\" />"
                                                            + "<location x=\"2\"  y=\"7\" />")
                                            + "<location x=\"2\"  y=\"6\" />"
                                            + "<location x=\"1\"  y=\"6\" />"
                                            + "<location x=\"1\"  y=\"5\" />"
                                            + "<location x=\"1\"  y=\"4\" />"
                                            + "<location x=\"1\"  y=\"3\" />"
                                            + "<location x=\"1\"  y=\"2\" />"
                                            + "<location x=\"2\"  y=\"2\" />"
                                            + "<location x=\"3\"  y=\"2\" />"
                                            + "<location x=\"4\"  y=\"2\" />"
                                            + "<location x=\"5\"  y=\"2\" />"
                                            + "<location x=\"6\"  y=\"2\" />"
                                            + "<location x=\"6\"  y=\"1\" />"
                                            + "<location x=\"6\"  y=\"0\" />";
                                    break;
                                case 19: // Water
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + (tsapc.s3_g2_b1a || tsapc.s3_g2_b1b
                                                    ? "<location x=\"7\"  y=\"12\" />"
                                                            + "<location x=\"8\"  y=\"12\" />"
                                                            + "<location x=\"9\"  y=\"12\" />"
                                                            + "<location x=\"9\"  y=\"11\" />"
                                                    : "<location x=\"6\"  y=\"11\" />"
                                                            + "<location x=\"6\"  y=\"10\" />"
                                                            + "<location x=\"7\"  y=\"10\" />"
                                                            + "<location x=\"8\"  y=\"10\" />")
                                            + "<location x=\"9\"  y=\"10\" />"
                                            + "<location x=\"10\"  y=\"10\" />"
                                            + "<location x=\"11\"  y=\"10\" />"
                                            + "<location x=\"11\"  y=\"9\" />"
                                            + "<location x=\"11\"  y=\"8\" />"
                                            + "<location x=\"11\"  y=\"7\" />"
                                            + "<location x=\"11\"  y=\"6\" />"
                                            + "<location x=\"10\"  y=\"6\" />"
                                            + "<location x=\"9\"  y=\"6\" />"
                                            + "<location x=\"8\"  y=\"6\" />"
                                            + "<location x=\"8\"  y=\"5\" />"
                                            + "<location x=\"8\"  y=\"4\" />"
                                            + "<location x=\"8\"  y=\"3\" />"
                                            + "<location x=\"7\"  y=\"3\" />"
                                            + "<location x=\"6\"  y=\"3\" />"
                                            + "<location x=\"6\"  y=\"2\" />"
                                            + "<location x=\"6\"  y=\"1\" />"
                                            + "<location x=\"6\"  y=\"0\" />";
                                    break;
                                case 20: // Food
                                    solutionPath =
                                            "<location x=\"6\"  y=\"12\" /><location x=\"6\"  y=\"12\" /><location x=\"7\"  y=\"12\" /><location x=\"8\"  y=\"12\" /><location x=\"8\"  y=\"11\" /><location x=\"8\"  y=\"10\" /><location x=\"8\"  y=\"9\" /><location x=\"8\"  y=\"8\" /><location x=\"8\"  y=\"7\" /><location x=\"8\"  y=\"6\" /><location x=\"8\"  y=\"5\" /><location x=\"8\"  y=\"4\" /><location x=\"8\"  y=\"3\" /><location x=\"8\"  y=\"2\" /><location x=\"8\"  y=\"1\" /><location x=\"8\"  y=\"0\" /><location x=\"7\"  y=\"0\" /><location x=\"6\"  y=\"0\" />";
                                    break;
                            }
                            break;
                        case 4: 
                            l.log(Level.FINE, "{0}, {1}, {2}",
                                    new Object[] {tsapc.s1_g1_b1, tsapc.s1_g2_b1, tsapc.s1_g2_b2});
                            switch (targetGoalBlock) {
                                case 18: // Medicine
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + "<location x=\"5\"  y=\"12\" />"
                                            + "<location x=\"4\"  y=\"12\" />"
                                            + "<location x=\"3\"  y=\"12\" />"
                                            + "<location x=\"2\"  y=\"12\" />"
                                            + "<location x=\"1\"  y=\"12\" />"
                                            + "<location x=\"1\"  y=\"11\" />"
                                            + "<location x=\"1\"  y=\"10\" />"
                                            + "<location x=\"1\"  y=\"9\" />"
                                            + (tsapc.s1_g1_b1 ? (tsapc.s1_g1_b2
                                                    ? "<location x=\"0\"  y=\"9\" />"
                                                            + "<location x=\"0\"  y=\"8\" />"
                                                            + "<location x=\"0\"  y=\"7\" />"
                                                            + "<location x=\"0\"  y=\"6\" />"
                                                            + "<location x=\"0\"  y=\"5\" />"
                                                            + "<location x=\"0\"  y=\"4\" />"
                                                            + "<location x=\"0\"  y=\"3\" />"
                                                    : "<location x=\"0\"  y=\"9\" />"
                                                            + "<location x=\"0\"  y=\"8\" />"
                                                            + "<location x=\"0\"  y=\"7\" />"
                                                            + "<location x=\"1\"  y=\"7\" />"
                                                            + "<location x=\"1\"  y=\"6\" />"
                                                            + "<location x=\"1\"  y=\"5\" />"
                                                            + "<location x=\"1\"  y=\"4\" />")
                                                    : (tsapc.s1_g1_b2
                                                            ? "<location x=\"1\"  y=\"8\" />"
                                                                    + "<location x=\"1\"  y=\"7\" />"
                                                                    + "<location x=\"0\"  y=\"7\" />"
                                                                    + "<location x=\"0\"  y=\"6\" />"
                                                                    + "<location x=\"0\"  y=\"5\" />"
                                                                    + "<location x=\"0\"  y=\"4\" />"
                                                                    + "<location x=\"0\"  y=\"3\" />"
                                                            : "<location x=\"1\"  y=\"8\" />"
                                                                    + "<location x=\"1\"  y=\"7\" />"
                                                                    + "<location x=\"1\"  y=\"6\" />"
                                                                    + "<location x=\"1\"  y=\"5\" />"
                                                                    + "<location x=\"1\"  y=\"4\" />"))
                                            + "<location x=\"1\"  y=\"3\" />"
                                            + "<location x=\"1\"  y=\"2\" />"
                                            + "<location x=\"1\"  y=\"1\" />"
                                            + "<location x=\"1\"  y=\"0\" />"
                                            + "<location x=\"2\"  y=\"0\" />"
                                            + "<location x=\"3\"  y=\"0\" />"
                                            + "<location x=\"4\"  y=\"0\" />"
                                            + "<location x=\"5\"  y=\"0\" />"
                                            + "<location x=\"6\"  y=\"0\" />";
                                    if (highSeverity) {
                                        solutionPath = "<location x=\"6\"  y=\"12\" />"
                                                + "<location x=\"5\"  y=\"12\" />"
                                                + "<location x=\"4\"  y=\"12\" />"
                                                + "<location x=\"3\"  y=\"12\" />"
                                                + "<location x=\"2\"  y=\"12\" />"
                                                + "<location x=\"1\"  y=\"12\" />"
                                                + "<location x=\"1\"  y=\"11\" />"
                                                + "<location x=\"1\"  y=\"10\" />"
                                                + "<location x=\"1\"  y=\"9\" />"
                                                + (tsapc.s1_g1_b1 ? (tsapc.s1_g1_b2
                                                        ? "<location x=\"0\"  y=\"9\" />"
                                                                + "<location x=\"0\"  y=\"8\" />"
                                                                + "<location x=\"0\"  y=\"7\" />"
                                                                + "<location x=\"0\"  y=\"6\" />"
                                                                + "<location x=\"0\"  y=\"5\" />"
                                                                + "<location x=\"1\"  y=\"5\" />"
                                                        : "<location x=\"0\"  y=\"9\" />"
                                                                + "<location x=\"0\"  y=\"8\" />"
                                                                + "<location x=\"0\"  y=\"7\" />"
                                                                + "<location x=\"1\"  y=\"7\" />"
                                                                + "<location x=\"1\"  y=\"6\" />"
                                                                + "<location x=\"1\"  y=\"5\" />")
                                                        : (tsapc.s1_g1_b2
                                                                ? "<location x=\"1\"  y=\"8\" />"
                                                                        + "<location x=\"1\"  y=\"7\" />"
                                                                        + "<location x=\"0\"  y=\"7\" />"
                                                                        + "<location x=\"0\"  y=\"6\" />"
                                                                        + "<location x=\"0\"  y=\"5\" />"
                                                                        + "<location x=\"1\"  y=\"5\" />"
                                                                : "<location x=\"1\"  y=\"8\" />"
                                                                        + "<location x=\"1\"  y=\"7\" />"
                                                                        + "<location x=\"1\"  y=\"6\" />"
                                                                        + "<location x=\"1\"  y=\"5\" />"))
                                                + "<location x=\"2\"  y=\"5\" />"
                                                + "<location x=\"3\"  y=\"5\" />"
                                                + "<location x=\"4\"  y=\"5\" />"
                                                + "<location x=\"5\"  y=\"5\" />"
                                                + "<location x=\"6\"  y=\"5\" />"
                                                + "<location x=\"6\"  y=\"4\" />"
                                                + "<location x=\"6\"  y=\"3\" />"
                                                + "<location x=\"6\"  y=\"2\" />"
                                                + "<location x=\"6\"  y=\"1\" />"
                                                + "<location x=\"6\"  y=\"0\" />";
                                    }
                                    break;
                                case 19: // Water
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + "<location x=\"7\"  y=\"12\" />"
                                            + "<location x=\"8\"  y=\"12\" />"
                                            + (tsapc.s1_g2_b1
                                                    ? "<location x=\"8\"  y=\"11\" />"
                                                            + "<location x=\"8\"  y=\"10\" />"
                                                    : "<location x=\"9\"  y=\"12\" />"
                                                            + "<location x=\"9\"  y=\"11\" />")
                                            + "<location x=\"9\"  y=\"10\" />"
                                            + "<location x=\"9\"  y=\"9\" />"
                                            + "<location x=\"9\"  y=\"8\" />"
                                            + "<location x=\"9\"  y=\"7\" />"
                                            + "<location x=\"8\"  y=\"7\" />"
                                            + "<location x=\"8\"  y=\"6\" />"
                                            + "<location x=\"8\"  y=\"5\" />"
                                            + "<location x=\"8\"  y=\"4\" />"
                                            + "<location x=\"8\"  y=\"3\" />"
                                            + (tsapc.s1_g2_b2
                                                    ? "<location x=\"7\"  y=\"3\" />"
                                                            + "<location x=\"6\"  y=\"3\" />"
                                                            + "<location x=\"6\"  y=\"2\" />"
                                                            + "<location x=\"6\"  y=\"1\" />"
                                                    : "<location x=\"8\"  y=\"2\" />"
                                                            + "<location x=\"8\"  y=\"1\" />"
                                                            + "<location x=\"8\"  y=\"0\" />"
                                                            + "<location x=\"7\"  y=\"0\" />")
                                            + "<location x=\"6\"  y=\"0\" />";
                                    if (highSeverity) {
                                        solutionPath = "<location x=\"6\"  y=\"12\" />"
                                                + "<location x=\"7\"  y=\"12\" />"
                                                + "<location x=\"7\"  y=\"11\" />"
                                                + "<location x=\"7\"  y=\"10\" />"
                                                + "<location x=\"7\"  y=\"9\" />"
                                                + "<location x=\"7\"  y=\"8\" />"
                                                + "<location x=\"7\"  y=\"7\" />"
                                                + "<location x=\"7\"  y=\"6\" />"
                                                + "<location x=\"7\"  y=\"5\" />"
                                                + "<location x=\"7\"  y=\"4\" />"
                                                + "<location x=\"7\"  y=\"3\" />"
                                                + "<location x=\"7\"  y=\"2\" />"
                                                + "<location x=\"7\"  y=\"1\" />"
                                                + "<location x=\"7\"  y=\"0\" />"
                                                + "<location x=\"6\"  y=\"0\" />";
                                    }
                                    break;
                                // case 20: // Food
                                // solutionPath = "<location x=\"6\" y=\"12\" /><location x=\"6\"
                                // y=\"12\" /><location x=\"7\" y=\"12\" /><location x=\"8\"
                                // y=\"12\" /><location x=\"8\" y=\"11\" /><location x=\"8\"
                                // y=\"10\" /><location x=\"8\" y=\"9\" /><location x=\"8\" y=\"8\"
                                // /><location x=\"8\" y=\"7\" /><location x=\"8\" y=\"6\"
                                // /><location x=\"8\" y=\"5\" /><location x=\"8\" y=\"4\"
                                // /><location x=\"8\" y=\"3\" /><location x=\"8\" y=\"2\"
                                // /><location x=\"8\" y=\"1\" /><location x=\"8\" y=\"0\"
                                // /><location x=\"7\" y=\"0\" /><location x=\"6\" y=\"0\" />";
                                // break;
                            }
                            break;
                        case 6: 
                            switch (targetGoalBlock) {
                                case 18: // Medicine
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + "<location x=\"6\"  y=\"11\" />"
                                            + "<location x=\"6\"  y=\"10\" />"
                                            + "<location x=\"6\"  y=\"9\" />"
                                            + "<location x=\"5\"  y=\"9\" />"
                                            + "<location x=\"4\"  y=\"9\" />"
                                            + "<location x=\"3\"  y=\"9\" />"
                                            + "<location x=\"2\"  y=\"9\" />"
                                            + "<location x=\"1\"  y=\"9\" />"
                                            + "<location x=\"1\"  y=\"8\" />"
                                            + "<location x=\"1\"  y=\"7\" />"
                                            + "<location x=\"1\"  y=\"6\" />"
                                            + "<location x=\"1\"  y=\"5\" />"
                                            + "<location x=\"1\"  y=\"4\" />"
                                            + "<location x=\"1\"  y=\"3\" />"
                                            + "<location x=\"1\"  y=\"2\" />"
                                            + "<location x=\"1\"  y=\"1\" />"
                                            + "<location x=\"1\"  y=\"0\" />"
                                            + "<location x=\"2\"  y=\"0\" />"
                                            + "<location x=\"3\"  y=\"0\" />"
                                            + "<location x=\"4\"  y=\"0\" />"
                                            + "<location x=\"5\"  y=\"0\" />"
                                            + "<location x=\"6\"  y=\"0\" />";
                                    break;
                                case 19: // Water
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + "<location x=\"7\"  y=\"12\" />"
                                            + "<location x=\"8\"  y=\"12\" />"
                                            + "<location x=\"9\"  y=\"12\" />"
                                            + "<location x=\"9\"  y=\"11\" />"
                                            + "<location x=\"9\"  y=\"10\" />"
                                            + "<location x=\"9\"  y=\"9\" />"
                                            + (tsapc.s2_g2_b1
                                                    ? "<location x=\"10\"  y=\"9\" />"
                                                            + "<location x=\"10\"  y=\"8\" />"
                                                            + "<location x=\"10\"  y=\"7\" />"
                                                    : "<location x=\"9\"  y=\"8\" />")
                                            + "<location x=\"9\"  y=\"7\" />"
                                            + "<location x=\"9\"  y=\"6\" />"
                                            + (tsapc.s2_g2_b2 ? "<location x=\"8\"  y=\"6\" />"
                                                    : "<location x=\"9\"  y=\"5\" />")
                                            + "<location x=\"8\"  y=\"5\" />"
                                            + "<location x=\"7\"  y=\"5\" />"
                                            + "<location x=\"6\"  y=\"5\" />"
                                            + "<location x=\"6\"  y=\"4\" />"
                                            + "<location x=\"6\"  y=\"3\" />"
                                            + "<location x=\"6\"  y=\"2\" />"
                                            + "<location x=\"6\"  y=\"1\" />"
                                            + "<location x=\"6\"  y=\"0\" />";
                                    break;
                                case 20: // Food
                                    solutionPath =
                                            "<location x=\"6\"  y=\"12\" /><location x=\"6\"  y=\"12\" /><location x=\"7\"  y=\"12\" /><location x=\"8\"  y=\"12\" /><location x=\"8\"  y=\"11\" /><location x=\"8\"  y=\"10\" /><location x=\"8\"  y=\"9\" /><location x=\"8\"  y=\"8\" /><location x=\"8\"  y=\"7\" /><location x=\"8\"  y=\"6\" /><location x=\"8\"  y=\"5\" /><location x=\"8\"  y=\"4\" /><location x=\"8\"  y=\"3\" /><location x=\"8\"  y=\"2\" /><location x=\"8\"  y=\"1\" /><location x=\"8\"  y=\"0\" /><location x=\"7\"  y=\"0\" /><location x=\"6\"  y=\"0\" />";
                                    break;
                            }
                            break;
                        case 5: 
                            l.log(Level.FINE, "{0}, {1}, {2}",
                                    new Object[] {tsapc.s1_g1_b1, tsapc.s1_g2_b1, tsapc.s1_g2_b2});
                            switch (targetGoalBlock) {
                                case 18: // Medicine
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + "<location x=\"5\"  y=\"12\" />"
                                            + "<location x=\"4\"  y=\"12\" />"
                                            + "<location x=\"3\"  y=\"12\" />"
                                            + "<location x=\"2\"  y=\"12\" />"
                                            + "<location x=\"1\"  y=\"12\" />"
                                            + "<location x=\"1\"  y=\"11\" />"
                                            + "<location x=\"1\"  y=\"10\" />"
                                            + "<location x=\"1\"  y=\"9\" />"
                                            + (tsapc.s1_g1_b1 ? (tsapc.s1_g1_b2
                                                    ? "<location x=\"0\"  y=\"9\" />"
                                                            + "<location x=\"0\"  y=\"8\" />"
                                                            + "<location x=\"0\"  y=\"7\" />"
                                                            + "<location x=\"0\"  y=\"6\" />"
                                                            + "<location x=\"0\"  y=\"5\" />"
                                                            + "<location x=\"0\"  y=\"4\" />"
                                                            + "<location x=\"0\"  y=\"3\" />"
                                                    : "<location x=\"0\"  y=\"9\" />"
                                                            + "<location x=\"0\"  y=\"8\" />"
                                                            + "<location x=\"0\"  y=\"7\" />"
                                                            + "<location x=\"1\"  y=\"7\" />"
                                                            + "<location x=\"1\"  y=\"6\" />"
                                                            + "<location x=\"1\"  y=\"5\" />"
                                                            + "<location x=\"1\"  y=\"4\" />")
                                                    : (tsapc.s1_g1_b2
                                                            ? "<location x=\"1\"  y=\"8\" />"
                                                                    + "<location x=\"1\"  y=\"7\" />"
                                                                    + "<location x=\"0\"  y=\"7\" />"
                                                                    + "<location x=\"0\"  y=\"6\" />"
                                                                    + "<location x=\"0\"  y=\"5\" />"
                                                                    + "<location x=\"0\"  y=\"4\" />"
                                                                    + "<location x=\"0\"  y=\"3\" />"
                                                            : "<location x=\"1\"  y=\"8\" />"
                                                                    + "<location x=\"1\"  y=\"7\" />"
                                                                    + "<location x=\"1\"  y=\"6\" />"
                                                                    + "<location x=\"1\"  y=\"5\" />"
                                                                    + "<location x=\"1\"  y=\"4\" />"))
                                            + "<location x=\"1\"  y=\"3\" />"
                                            + "<location x=\"1\"  y=\"2\" />"
                                            + "<location x=\"1\"  y=\"1\" />"
                                            + "<location x=\"1\"  y=\"0\" />"
                                            + "<location x=\"2\"  y=\"0\" />"
                                            + "<location x=\"3\"  y=\"0\" />"
                                            + "<location x=\"4\"  y=\"0\" />"
                                            + "<location x=\"5\"  y=\"0\" />"
                                            + "<location x=\"6\"  y=\"0\" />";
                                    break;
                                case 19: // Water
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + "<location x=\"7\"  y=\"12\" />"
                                            + "<location x=\"8\"  y=\"12\" />"
                                            + (tsapc.s1_g2_b1
                                                    ? "<location x=\"8\"  y=\"11\" />"
                                                            + "<location x=\"8\"  y=\"10\" />"
                                                    : "<location x=\"9\"  y=\"12\" />"
                                                            + "<location x=\"9\"  y=\"11\" />")
                                            + "<location x=\"9\"  y=\"10\" />"
                                            + "<location x=\"9\"  y=\"9\" />"
                                            + "<location x=\"9\"  y=\"8\" />"
                                            + "<location x=\"9\"  y=\"7\" />"
                                            + "<location x=\"8\"  y=\"7\" />"
                                            + "<location x=\"8\"  y=\"6\" />"
                                            + "<location x=\"8\"  y=\"5\" />"
                                            + "<location x=\"8\"  y=\"4\" />"
                                            + "<location x=\"8\"  y=\"3\" />"
                                            + (tsapc.s1_g2_b2
                                                    ? "<location x=\"7\"  y=\"3\" />"
                                                            + "<location x=\"6\"  y=\"3\" />"
                                                            + "<location x=\"6\"  y=\"2\" />"
                                                            + "<location x=\"6\"  y=\"1\" />"
                                                    : "<location x=\"8\"  y=\"2\" />"
                                                            + "<location x=\"8\"  y=\"1\" />"
                                                            + "<location x=\"8\"  y=\"0\" />"
                                                            + "<location x=\"7\"  y=\"0\" />")
                                            + "<location x=\"6\"  y=\"0\" />";
                                    break;
                                // case 20: // Food
                                // solutionPath = "<location x=\"6\" y=\"12\" /><location x=\"6\"
                                // y=\"12\" /><location x=\"7\" y=\"12\" /><location x=\"8\"
                                // y=\"12\" /><location x=\"8\" y=\"11\" /><location x=\"8\"
                                // y=\"10\" /><location x=\"8\" y=\"9\" /><location x=\"8\" y=\"8\"
                                // /><location x=\"8\" y=\"7\" /><location x=\"8\" y=\"6\"
                                // /><location x=\"8\" y=\"5\" /><location x=\"8\" y=\"4\"
                                // /><location x=\"8\" y=\"3\" /><location x=\"8\" y=\"2\"
                                // /><location x=\"8\" y=\"1\" /><location x=\"8\" y=\"0\"
                                // /><location x=\"7\" y=\"0\" /><location x=\"6\" y=\"0\" />";
                                // break;
                            }
                            break;
                        case 3: 
                            switch (targetGoalBlock) {
                                case 18: // Medicine
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + "<location x=\"6\"  y=\"11\" />"
                                            + "<location x=\"6\"  y=\"10\" />"
                                            + "<location x=\"6\"  y=\"9\" />"
                                            + "<location x=\"5\"  y=\"9\" />"
                                            + "<location x=\"4\"  y=\"9\" />"
                                            + "<location x=\"3\"  y=\"9\" />"
                                            + "<location x=\"2\"  y=\"9\" />"
                                            + "<location x=\"1\"  y=\"9\" />"
                                            + "<location x=\"1\"  y=\"8\" />"
                                            + "<location x=\"1\"  y=\"7\" />"
                                            + "<location x=\"1\"  y=\"6\" />"
                                            + "<location x=\"1\"  y=\"5\" />"
                                            + "<location x=\"1\"  y=\"4\" />"
                                            + "<location x=\"1\"  y=\"3\" />"
                                            + "<location x=\"1\"  y=\"2\" />"
                                            + "<location x=\"1\"  y=\"1\" />"
                                            + "<location x=\"1\"  y=\"0\" />"
                                            + "<location x=\"2\"  y=\"0\" />"
                                            + "<location x=\"3\"  y=\"0\" />"
                                            + "<location x=\"4\"  y=\"0\" />"
                                            + "<location x=\"5\"  y=\"0\" />"
                                            + "<location x=\"6\"  y=\"0\" />";
                                    break;
                                case 19: // Water
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + "<location x=\"7\"  y=\"12\" />"
                                            + "<location x=\"8\"  y=\"12\" />"
                                            + "<location x=\"9\"  y=\"12\" />"
                                            + "<location x=\"9\"  y=\"11\" />"
                                            + "<location x=\"9\"  y=\"10\" />"
                                            + "<location x=\"9\"  y=\"9\" />"
                                            + (tsapc.s2_g2_b1
                                                    ? "<location x=\"10\"  y=\"9\" />"
                                                            + "<location x=\"10\"  y=\"8\" />"
                                                            + "<location x=\"10\"  y=\"7\" />"
                                                    : "<location x=\"9\"  y=\"8\" />")
                                            + "<location x=\"9\"  y=\"7\" />"
                                            + "<location x=\"9\"  y=\"6\" />"
                                            + (tsapc.s2_g2_b2 ? "<location x=\"8\"  y=\"6\" />"
                                                    : "<location x=\"9\"  y=\"5\" />")
                                            + "<location x=\"8\"  y=\"5\" />"
                                            + "<location x=\"7\"  y=\"5\" />"
                                            + "<location x=\"6\"  y=\"5\" />"
                                            + "<location x=\"6\"  y=\"4\" />"
                                            + "<location x=\"6\"  y=\"3\" />"
                                            + "<location x=\"6\"  y=\"2\" />"
                                            + "<location x=\"6\"  y=\"1\" />"
                                            + "<location x=\"6\"  y=\"0\" />";
                                    break;
                                case 20: // Food
                                    solutionPath =
                                            "<location x=\"6\"  y=\"12\" /><location x=\"6\"  y=\"12\" /><location x=\"7\"  y=\"12\" /><location x=\"8\"  y=\"12\" /><location x=\"8\"  y=\"11\" /><location x=\"8\"  y=\"10\" /><location x=\"8\"  y=\"9\" /><location x=\"8\"  y=\"8\" /><location x=\"8\"  y=\"7\" /><location x=\"8\"  y=\"6\" /><location x=\"8\"  y=\"5\" /><location x=\"8\"  y=\"4\" /><location x=\"8\"  y=\"3\" /><location x=\"8\"  y=\"2\" /><location x=\"8\"  y=\"1\" /><location x=\"8\"  y=\"0\" /><location x=\"7\"  y=\"0\" /><location x=\"6\"  y=\"0\" />";
                                    break;
                            }
                            break;
                        case 7: 
                            switch (targetGoalBlock) {
                                case 18: // Medicine
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + "<location x=\"6\"  y=\"11\" />"
                                            + "<location x=\"6\"  y=\"10\" />"
                                            + (tsapc.s3_g1_b1
                                                    ? "<location x=\"6\"  y=\"9\" />"
                                                            + "<location x=\"6\"  y=\"8\" />"
                                                            + "<location x=\"6\"  y=\"7\" />"
                                                            + "<location x=\"5\"  y=\"7\" />"
                                                            + "<location x=\"4\"  y=\"7\" />"
                                                            + "<location x=\"3\"  y=\"7\" />"
                                                            + "<location x=\"3\"  y=\"6\" />"
                                                    : "<location x=\"5\"  y=\"10\" />"
                                                            + "<location x=\"4\"  y=\"10\" />"
                                                            + "<location x=\"3\"  y=\"10\" />"
                                                            + "<location x=\"2\"  y=\"10\" />"
                                                            + "<location x=\"2\"  y=\"9\" />"
                                                            + "<location x=\"2\"  y=\"8\" />"
                                                            + "<location x=\"2\"  y=\"7\" />")
                                            + "<location x=\"2\"  y=\"6\" />"
                                            + "<location x=\"1\"  y=\"6\" />"
                                            + "<location x=\"1\"  y=\"5\" />"
                                            + "<location x=\"1\"  y=\"4\" />"
                                            + "<location x=\"1\"  y=\"3\" />"
                                            + "<location x=\"1\"  y=\"2\" />"
                                            + "<location x=\"2\"  y=\"2\" />"
                                            + "<location x=\"3\"  y=\"2\" />"
                                            + "<location x=\"4\"  y=\"2\" />"
                                            + "<location x=\"5\"  y=\"2\" />"
                                            + "<location x=\"6\"  y=\"2\" />"
                                            + "<location x=\"6\"  y=\"1\" />"
                                            + "<location x=\"6\"  y=\"0\" />";
                                    break;
                                case 19: // Water
                                    solutionPath = "<location x=\"6\"  y=\"12\" />"
                                            + (tsapc.s3_g2_b1a || tsapc.s3_g2_b1b
                                                    ? "<location x=\"7\"  y=\"12\" />"
                                                            + "<location x=\"8\"  y=\"12\" />"
                                                            + "<location x=\"9\"  y=\"12\" />"
                                                            + "<location x=\"9\"  y=\"11\" />"
                                                    : "<location x=\"6\"  y=\"11\" />"
                                                            + "<location x=\"6\"  y=\"10\" />"
                                                            + "<location x=\"7\"  y=\"10\" />"
                                                            + "<location x=\"8\"  y=\"10\" />")
                                            + "<location x=\"9\"  y=\"10\" />"
                                            + "<location x=\"10\"  y=\"10\" />"
                                            + "<location x=\"11\"  y=\"10\" />"
                                            + "<location x=\"11\"  y=\"9\" />"
                                            + "<location x=\"11\"  y=\"8\" />"
                                            + "<location x=\"11\"  y=\"7\" />"
                                            + "<location x=\"11\"  y=\"6\" />"
                                            + "<location x=\"10\"  y=\"6\" />"
                                            + "<location x=\"9\"  y=\"6\" />"
                                            + "<location x=\"8\"  y=\"6\" />"
                                            + "<location x=\"8\"  y=\"5\" />"
                                            + "<location x=\"8\"  y=\"4\" />"
                                            + "<location x=\"8\"  y=\"3\" />"
                                            + "<location x=\"7\"  y=\"3\" />"
                                            + "<location x=\"6\"  y=\"3\" />"
                                            + "<location x=\"6\"  y=\"2\" />"
                                            + "<location x=\"6\"  y=\"1\" />"
                                            + "<location x=\"6\"  y=\"0\" />";
                                    break;
                                case 20: // Food
                                    solutionPath =
                                            "<location x=\"6\"  y=\"12\" /><location x=\"6\"  y=\"12\" /><location x=\"7\"  y=\"12\" /><location x=\"8\"  y=\"12\" /><location x=\"8\"  y=\"11\" /><location x=\"8\"  y=\"10\" /><location x=\"8\"  y=\"9\" /><location x=\"8\"  y=\"8\" /><location x=\"8\"  y=\"7\" /><location x=\"8\"  y=\"6\" /><location x=\"8\"  y=\"5\" /><location x=\"8\"  y=\"4\" /><location x=\"8\"  y=\"3\" /><location x=\"8\"  y=\"2\" /><location x=\"8\"  y=\"1\" /><location x=\"8\"  y=\"0\" /><location x=\"7\"  y=\"0\" /><location x=\"6\"  y=\"0\" />";
                                    break;
                            }
                            break;
                    }
                    tsapc.sendMessageToServer("truststudy.updatePath",
                            "<info id=\"" + ownedPathID + "\">" + solutionPath + "</info>");
                    tsapc.sendMessageToServer("truststudy.executeRequest", "");
                } catch (InterruptedException ex) {
                    Logger.getLogger(TrustStudyAPClient.class.getName()).log(Level.SEVERE, null,
                            ex);
                }
            }
        });
        t.start();

    }

    boolean proposalResponded = false;

    @Override
    public void processMessage(final DEFMessage defMessage) {
        if (defMessage.name.equals("echo")) {
            Thread t = new Thread(new Runnable() {

                public void run() {
                    try {
                        Thread.sleep(Long.parseLong(((Element) (defMessage.mRoot.getFirstChild()))
                                .getAttribute("delay")));
                        jh.send(new DEFMessage(getClientId(), getServerId(),
                                defMessage.mRoot.getFirstChild().getFirstChild().getTextContent()));
                    } catch (InterruptedException ex) {
                        Logger.getLogger(TrustStudyAPClient.class.getName()).log(Level.SEVERE, null,
                                ex);
                    } catch (JMSException ex) {
                        Logger.getLogger(TrustStudyAPClient.class.getName()).log(Level.SEVERE, null,
                                ex);
                    } catch (TransformerException ex) {
                        Logger.getLogger(TrustStudyAPClient.class.getName()).log(Level.SEVERE, null,
                                ex);
                    }
                }
            });
            t.start();

        }

        if (defMessage.name.equals("proposal.setState")) {
            if (!proposalActive) {
                isGameStarted = false;
                proposalActive = true;
                NodeList nl = defMessage.mRoot.getElementsByTagName("button");
                String acceptValue = ((Element) nl.item(0)).getAttribute("accept");
                boolean canAccept = false;
                try {
                    canAccept = Boolean.parseBoolean(acceptValue);
                } catch (NumberFormatException ex) {
                }

                if (canAccept) {
                    proposalResponded = false;
                    final TrustStudyAPClient tsapc_this = this;
                    Thread t = new Thread(new Runnable() {

                        TrustStudyAPClient tsapc = tsapc_this;

                        public void run() {
                            try {
                                tsapc.holdTheAgentProposal = true;
                                Thread.sleep(1900);
                                // if (proposalActive ) {


                                sendMessageToServer("proposal.action",
                                        "<proposalaction choice=\"confirm\" />");
                                proposalResponded = true;

                                // }
                                // jh.send(new DEFMessage(getClientId(), getServerId(),
                                // defMessage.mRoot.getFirstChild().getFirstChild().getTextContent()));
                            } catch (InterruptedException ex) {
                                Logger.getLogger(TrustStudyAPClient.class.getName())
                                        .log(Level.SEVERE, null, ex);
                            }
                        }
                    });
                    t.start();
                } else {
                    proposalResponded = true;
                }
            }
        }

        if (defMessage.name.equals("proposal.close")) {
            if (proposalActive) {
                isGameStarted = true;
                proposalActive = false;
            }
        }

        if (defMessage.name.equals("dialog.setState")) {
            isGameStarted = false;
            Element statusElement = ((Element) (defMessage.mRoot.getFirstChild()));
            if (statusElement.hasAttribute("buttonEnabled")
                    && Boolean.parseBoolean(statusElement.getAttribute("buttonEnabled"))) {
                sendMessage(new DEFMessage(getClientId(), getServerId(),
                        "<message name=\"dialog.closed\">"
                                + (dialogName == null ? "" : "<name>" + dialogName + "</name>")
                                + "</message>"));
                isGameStarted = true;
            }
        }

        if (defMessage.name.equals("dialog.new")) {
            // int pid =
            // Integer.parseInt(defMessage.mRoot.getFirstChild().getFirstChild().getNodeValue());
            NodeList names = defMessage.mRoot.getElementsByTagName("name");
            if (names.getLength() > 0) {
                dialogName = defMessage.mRoot.getElementsByTagName("name").item(0).getFirstChild()
                        .getNodeValue();
            }
            return;
        }

        if (defMessage.name.equals("truststudy.init")) {
            l.finer("Initializing the client.");
            NodeList initNodeList = defMessage.mRoot.getElementsByTagName("init");
            if (initNodeList.getLength() > 0) {
                Element init = (Element) initNodeList.item(0);
                NodeList partnerNodes = init.getElementsByTagName("partner");
                if (partnerNodes.getLength() > 0) {
                    // PARTNERSIZE dependent
                    partnerIDs = new String[] {((Element) partnerNodes.item(0)).getAttribute(
                            "id") /* , ((Element) partnerNodes.item(1)).getAttribute("id") */};
                    partnerNames = new String[] {((Element) partnerNodes.item(0)).getAttribute(
                            "name")/* , ((Element) partnerNodes.item(1)).getAttribute("name") */};
                    partnerColors = new Color[] {new Color(
                            Integer.parseInt(((Element) partnerNodes.item(0)).getAttribute("red")),
                            Integer.parseInt(((Element) partnerNodes.item(0)).getAttribute("blue")),
                            Integer.parseInt(
                                    ((Element) partnerNodes.item(0)).getAttribute("green")))
                            /*
                             * , new Color(Integer.parseInt(((Element)
                             * partnerNodes.item(1)).getAttribute("red")),
                             * Integer.parseInt(((Element)
                             * partnerNodes.item(1)).getAttribute("blue")),
                             * Integer.parseInt(((Element)
                             * partnerNodes.item(1)).getAttribute("green")))
                             */
                    };
                }
            }
            return;
        }

        if (defMessage.name.equals("truststudy.setState")) {
            // Process the block and path informations if any
            NodeList infoNodes = defMessage.mRoot.getElementsByTagName("info");
            for (int i = 0; i < infoNodes.getLength(); i++) {
                Element e = (Element) infoNodes.item(i);
                if ("path".equals(e.getAttribute("type"))
                        && getClientId().equals(e.getAttribute("owner"))) {
                    ownedPathID = Integer.parseInt(e.getAttribute("id"));
                }
                if ("block".equals(e.getAttribute("type"))
                        && "goal".equals(e.getAttribute("subType"))
                        && getClientId().equals(e.getAttribute("owner"))) {
                    targetGoalBlock = Integer.parseInt(e.getAttribute("id"));
                    switch (targetGoalBlock) {
                        case 18: // Medicine
                            e.getAttribute("name").equals("Medicine");
                            break;
                        case 19: // Water
                            e.getAttribute("name").equals("Water");
                            break;
                        case 20: // Foods
                            e.getAttribute("name").equals("Food");
                            break;
                    }
                }
                {
                    int id = Integer.parseInt(e.getAttribute("id"));
                    switch (stage) {
                        case 3:
                            switch (id) {
                                case 9:
                                    s1_g1_b1 = hasShared(e);
                                    break;
                                case 14:
                                    s1_g1_b2 = hasShared(e);
                                    break;
                                case 11:
                                    s1_g2_b1 = hasShared(e);
                                    break;
                                case 12:
                                    s1_g2_b2 = hasShared(e);
                                    break;
                            }
                            break;
                        case 5: 
                            switch (id) {
                                case 9:
                                    s1_g1_b1 = hasShared(e);
                                    break;
                                case 14:
                                    s1_g1_b2 = hasShared(e);
                                    break;
                                case 11:
                                    s1_g2_b1 = hasShared(e);
                                    break;
                                case 12:
                                    s1_g2_b2 = hasShared(e);
                                    break;
                            }
                            break;
                        case 1: 
                            switch (id) {
                                case 9:
                                    s2_g2_b1 = hasShared(e);
                                    break;
                                case 14:
                                    s2_g2_b2 = hasShared(e);
                                    break;
                            }
                            break;
                        case 4:
                            switch (id) {
                                case 9:
                                    s2_g2_b1 = hasShared(e);
                                    break;
                                case 14:
                                    s2_g2_b2 = hasShared(e);
                                    break;
                            }
                            break;
                        case 6:
                            switch (id) {
                                case 9:
                                    s2_g2_b1 = hasShared(e);
                                    break;
                                case 14:
                                    s2_g2_b2 = hasShared(e);
                                    break;
                            }
                            break;
                        case 2:

                            switch (id) {
                                case 15:
                                    s3_g1_b1 = hasShared(e);
                                    break;
                                case 10:
                                    s3_g2_b1a = hasShared(e);
                                    break;
                                case 17:
                                    s3_g2_b1b = hasShared(e);
                                    break;
                            }
                        case 7:

                            switch (id) {
                                case 15:
                                    s3_g1_b1 = hasShared(e);
                                    break;
                                case 10:
                                    s3_g2_b1a = hasShared(e);
                                    break;
                                case 17:
                                    s3_g2_b1b = hasShared(e);
                                    break;
                            }
                    }
                }
                if ("block".equals(e.getAttribute("type"))
                        && (/* "payoff".equals(e.getAttribute("subType")) || */"damage"
                                .equals(e.getAttribute("subType"))
                                || "bonus".equals(e.getAttribute("subType")))
                        && getClientId().equals(e.getAttribute("owner"))) {
                    for (String partnerID : partnerIDs) {
                        shareCommands.add("<info id=\"" + e.getAttribute("id") + "\" target=\""
                                + partnerID + "\" share=\"true\"/>");
                    }
                }

            }
            NodeList buttonNodes = defMessage.mRoot.getElementsByTagName("button");
            for (int i = 0; i < buttonNodes.getLength(); i++) {
                Element e = (Element) buttonNodes.item(i);
                if ("finalizeGoalsButton".equals(e.getAttribute("name"))) {
                    canfinalizeGoals = Boolean.parseBoolean(e.getAttribute("enabled"));
                    l.finer("Can finalize goals = " + canfinalizeGoals);
                }
                if ("changeGoalsButton".equals(e.getAttribute("name"))) {
                    isGoalFinalized = !Boolean.parseBoolean(e.getAttribute("enabled"));
                    holdTheAgentProposal = isGoalFinalized;
                    l.finer("Is goal finalized = " + isGoalFinalized);
                }
                if ("executeButton".equals(e.getAttribute("name"))) {
                    canExecute = Boolean.parseBoolean(e.getAttribute("enabled"));
                    l.finer("ExecuteButton = " + canExecute);
                }
            }
            isGameStarted = true;
            return;
        }

        if (defMessage.name.equals("truststudy.execute")) {
            sendMessageToServer("truststudy.executeOver", "");
            return;
        }

        if (defMessage.name.equals("bribemanager.new")) {
            sendMessageToServer("bribemanager.closed", "<action response=\"reject\" />");
            return;
        }

        if (defMessage.name.equals("highseverity")) {
            highSeverity = true;
        }

        l.log(Level.FINER, "sending message ''{0}'' upstream.", defMessage.name);
        super.processMessage(defMessage);

    }

    private boolean hasShared(Element e) {

        NodeList nl = e.getElementsByTagName("shared");
        for (int i = 0; i < nl.getLength(); i++) {
            l.finer("Examining node with shared member "
                    + nl.item(i).getFirstChild().getNodeValue());
            if (nl.item(i).getFirstChild().getNodeValue().equals(getClientId())) {
                l.fine("Shared node isolated.");
                return true;
            }
        }
        return false;
    }
}
