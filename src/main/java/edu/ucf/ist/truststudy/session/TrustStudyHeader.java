package edu.ucf.ist.truststudy.session;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import edu.ucf.ist.session.Header;

/**
 *
 * @author dougcooper
 */
public class TrustStudyHeader extends Header {
    public static final String StageId = "Stage ID";
    public static final String ResourceLevel = "Food Level";
    public static final String ResourceRemaining = "Food Remaining";
    public static final String CurrencyEarned = "Curreny Earned";
    public static final String TotalCurrency = "Total Currency";
    public static final String HazardsShared = "Hazards Shared";
    public static final String BonusShared = "Bonus Shared";
    public static final String HazardsCrossed = "Hazards Crossed";
    public static final String ResourceBonusCrossed = "Food Bonus Crossed";
    public static final String DistanceTravelled = "Distance Travelled";
    public static final String TimeForFinalPath = "Time for Final Path";
    public static final String MistakeOrBribe = "Mistake/Bribe";
    public static final String RedRepType = "Red Reparation Type";
    public static final String BlueTookBribe = "Blue Took Bribe?";
    public static final String BlueBribeRepType = "Blue Bribe Reparation Type";
    public static final String RepString1 = "Repair Response 1";
    public static final String RepString2 = "Repair Response 2";
    public static final String ApologyAccepted = "Apology Accepted?";
    public static final String BlueMadeMistake = "Blue Made Mistake?";
    public static final String BlueMistakeRepType = "Blue Mistake Reparation Type";
    public static final String BlueMistakeResponse = "Blue Mistake Response";
    public static final String ParticipationResponse = "Participation Response";
    public static final String BehavioralResponse = "Behavioral Reponse";
    public static final String ConfrontResponse = "Confront Reponse";

    TrustStudyHeader() {
        _names.add(StageId);
        _names.add(ResourceLevel);
        _names.add(ResourceRemaining);
        _names.add(CurrencyEarned);
        _names.add(TotalCurrency);
        _names.add(HazardsShared);
        _names.add(BonusShared);
        _names.add(HazardsCrossed);
        _names.add(ResourceBonusCrossed);
        _names.add(DistanceTravelled);
        _names.add(TimeForFinalPath);
        _names.add(MistakeOrBribe);
        _names.add(RedRepType);
        _names.add(BlueTookBribe);
        _names.add(BlueBribeRepType);
        _names.add(RepString1);
        _names.add(RepString2);
        _names.add(ApologyAccepted);
        _names.add(BlueMadeMistake);
        _names.add(BlueMistakeRepType);
        _names.add(BlueMistakeResponse);
        _names.add(ParticipationResponse);
        _names.add(BehavioralResponse);
        _names.add(ConfrontResponse);
    }

}
