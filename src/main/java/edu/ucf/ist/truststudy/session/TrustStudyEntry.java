/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucf.ist.truststudy.session;

import java.lang.reflect.Field;
import java.util.List;
import org.apache.commons.lang3.reflect.FieldUtils;
import edu.ucf.ist.session.Entry;
import edu.ucf.ist.session.EntryField;

/**
 *
 * @author dougcooper
 */
public class TrustStudyEntry extends Entry {

    @EntryField(name = TrustStudyHeader.StageId)
    private String stageId = defaultValue;
    @EntryField(name = TrustStudyHeader.ResourceLevel)
    private String resourceLevel = defaultValue;
    @EntryField(name = TrustStudyHeader.ResourceRemaining)
    private String resourceRemaining = defaultValue;
    @EntryField(name = TrustStudyHeader.CurrencyEarned)
    private String currencyEarned = defaultValue;
    @EntryField(name = TrustStudyHeader.TotalCurrency)
    private String totalCurrency = defaultValue;
    @EntryField(name = TrustStudyHeader.HazardsShared)
    private String hazardsShared = defaultValue;
    @EntryField(name = TrustStudyHeader.BonusShared)
    private String bonusShared = defaultValue;
    @EntryField(name = TrustStudyHeader.HazardsCrossed)
    private String hazardsCrossed = defaultValue;
    @EntryField(name = TrustStudyHeader.ResourceBonusCrossed)
    private String resourceBonusCrossed = defaultValue;
    @EntryField(name = TrustStudyHeader.DistanceTravelled)
    private String distanceTravelled = defaultValue;
    @EntryField(name = TrustStudyHeader.TimeForFinalPath)
    private String timeForFinalPath = defaultValue;
    @EntryField(name = TrustStudyHeader.MistakeOrBribe)
    private String mistakeOrBribe = defaultValue;
    @EntryField(name = TrustStudyHeader.RedRepType)
    private String redPrepType = defaultValue;
    @EntryField(name = TrustStudyHeader.BlueTookBribe)
    private String blueTookBribe = defaultValue;
    @EntryField(name = TrustStudyHeader.BlueBribeRepType)
    private String blueBribeRepType = defaultValue;
    @EntryField(name = TrustStudyHeader.RepString1)
    private String repString1 = defaultValue;
    @EntryField(name = TrustStudyHeader.RepString2)
    private String repString2 = defaultValue;
    @EntryField(name = TrustStudyHeader.ApologyAccepted)
    private String apologyAccepted = defaultValue;
    @EntryField(name = TrustStudyHeader.ParticipationResponse)
    private String participationResponse = defaultValue;
    @EntryField(name = TrustStudyHeader.BlueMadeMistake)
    private String blueMadeMistake = defaultValue;
    @EntryField(name = TrustStudyHeader.BlueMistakeRepType)
    private String blueMistakeRepType = defaultValue;
    @EntryField(name = TrustStudyHeader.BlueMistakeResponse)
    private String blueMistakeResponse = defaultValue;
    @EntryField(name = TrustStudyHeader.BehavioralResponse)
    private String behavioralResponse = defaultValue;
    @EntryField(name = TrustStudyHeader.ConfrontResponse)
    private String confrontResponse = defaultValue;
    

    public String getBehavioralResponse() {
        return this.behavioralResponse;
    }

    public void setBehavioralResponse(String behavioralResponse) {
        this.behavioralResponse = behavioralResponse;
    }

    public TrustStudyEntry(int stageNumber) {
        super(stageNumber);
    }

    public String getStageId() {
        return stageId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public String getResourceLevel() {
        return resourceLevel;
    }

    public void setResourceLevel(String resourceLevel) {
        this.resourceLevel = resourceLevel;
    }

    public String getResourceRemaining() {
        return resourceRemaining;
    }

    public void setResourceRemaining(String resourceRemaining) {
        this.resourceRemaining = resourceRemaining;
    }

    public String getCurrencyEarned() {
        return currencyEarned;
    }

    public void setCurrencyEarned(String currencyEarned) {
        this.currencyEarned = currencyEarned;
    }

    public String getTotalCurrency() {
        return totalCurrency;
    }

    public void setTotalCurrency(String totalCurrency) {
        this.totalCurrency = totalCurrency;
    }

    public String getHazardsShared() {
        return hazardsShared;
    }

    public void setHazardsShared(String hazardsShared) {
        this.hazardsShared = hazardsShared;
    }

    public String getBonusShared() {
        return bonusShared;
    }

    public void setBonusShared(String bonusShared) {
        this.bonusShared = bonusShared;
    }

    public String getHazardsCrossed() {
        return hazardsCrossed;
    }

    public void setHazardsCrossed(String hazardsCrossed) {
        this.hazardsCrossed = hazardsCrossed;
    }

    public String getResourceBonusCrossed() {
        return resourceBonusCrossed;
    }

    public void setResourceBonusCrossed(String resourceBonusCrossed) {
        this.resourceBonusCrossed = resourceBonusCrossed;
    }

    public String getDistanceTravelled() {
        return distanceTravelled;
    }

    public void setDistanceTravelled(String distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    public String getTimeForFinalPath() {
        return timeForFinalPath;
    }

    public void setTimeForFinalPath(String timeForFinalPath) {
        this.timeForFinalPath = timeForFinalPath;
    }

    public String getMistakeOrBribe() {
        return mistakeOrBribe;
    }

    public void setMistakeOrBribe(String mistakeOrBribe) {
        this.mistakeOrBribe = mistakeOrBribe;
    }

    public String getRedPrepType() {
        return redPrepType;
    }

    public void setRedRepType(String redPrepType) {
        this.redPrepType = redPrepType;
    }

    public String getBlueTookBribe() {
        return blueTookBribe;
    }

    public void setBlueTookBribe(String blueTookBribe) {
        this.blueTookBribe = blueTookBribe;
    }

    public String getBlueBribeRepType() {
        return blueBribeRepType;
    }

    public void setBlueBribeRepType(String blueBribeRepType) {
        this.blueBribeRepType = blueBribeRepType;
    }

    public void setRepString1(String repString) {
        this.repString1 = repString;
    }

    public String getRepString1() {
        return this.repString1;
    }

    public void setRepString2(String repString) {
        this.repString2 = repString;
    }

    public String getRepString2() {
        return this.repString2;
    }

    public String getApologyAccepted() {
        return apologyAccepted;
    }

    public void setApologyAccepted(String apologyAccepted) {
        this.apologyAccepted = apologyAccepted;
    }

    public String getParticipationResponse() {
        return participationResponse;
    }

    public void setParticipationResponse(String participationResponse) {
        this.participationResponse = participationResponse;
    }

    public String getBlueMadeMistake() {
        return blueMadeMistake;
    }

    public void setBlueMadeMistake(String blueMadeMistake) {
        this.blueMadeMistake = blueMadeMistake;
    }

    public String getBlueMistakeRepType() {
        return blueMistakeRepType;
    }

    public void setBlueMistakeRepType(String blueMistakeRepType) {
        this.blueMistakeRepType = blueMistakeRepType;
    }

    public String getBlueMistakeResponse() {
        return blueMistakeResponse;
    }

    public void setBlueMistakeResponse(String blueMistakeResponse) {
        this.blueMistakeResponse = blueMistakeResponse;
    }

    public String getConfrontResponse() {
        return confrontResponse;
    }

    public void setConfrontResponse(String confrontResponse) {
        this.confrontResponse = confrontResponse;
    }

    @Override
    public List<Field> getEntryFields() {
        List<Field> fields = FieldUtils.getFieldsListWithAnnotation(TrustStudyEntry.class, EntryField.class);
        fields.addAll(super.getEntryFields());
        return fields;
      }
    

}
