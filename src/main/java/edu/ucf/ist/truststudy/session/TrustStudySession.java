/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */
package edu.ucf.ist.truststudy.session;

import java.io.File;
import edu.ucf.ist.session.Session;

/**
 *
 * @author dougcooper
 */
public class TrustStudySession extends Session {

    public TrustStudySession(int id, File logFile) {
        super(id, logFile, new TrustStudyHeader());
    }

    public static void main(String[] args) {

        int experimentNumber = 0;
        String path = "logs/";
        String baseName = "testlog";

        File f = Session.LogUtils.BuildLogFile(experimentNumber, path, baseName);

        TrustStudySession session = new TrustStudySession(experimentNumber, f);

        TrustStudyEntry e = new TrustStudyEntry(experimentNumber) {
            {
                setApologyAccepted("true");
                setBlueBribeRepType("true");
                setBlueMadeMistake("true");
                setBlueMistakeRepType("mistakeReparation");
                setBlueMistakeResponse("mistakeReparationString");
                setBlueTookBribe("true");
                setBonusShared(Integer.toString(100));
                setCurrencyEarned(Integer.toString(100));
                setDistanceTravelled(Integer.toString(100));
                setResourceBonusCrossed(Integer.toString(100));
                setResourceLevel(Integer.toString(100));
                setResourceRemaining(Integer.toString(100));
                setHazardsCrossed(Integer.toString(100));
                setHazardsShared(Integer.toString(100));
                setMistakeOrBribe("Mistake");
                setBehavioralResponse("behavioralResponse");
                setParticipationResponse("participationResponse");
                setRedRepType("setRedRepType");
                setRepString1("tsp.reparationString1");
                setRepString2("tsp.reparationString2");
                setStageId("Blue");
                setTimeForFinalPath(Long.toString(100));
                setTotalCurrency(Integer.toString(100));
                setConfrontResponse("confrontResponse");
            }
        };

        session.addEntry(e, true);
    }
}
