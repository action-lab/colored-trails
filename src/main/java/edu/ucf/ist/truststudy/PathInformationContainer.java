/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucf.ist.truststudy;

import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author culturelab
 */
public class PathInformationContainer extends InformationContainer {

    ArrayList<Point> positions;
    int offsetXPixel = 0;
    int offsetYPixel = 0;

    public PathInformationContainer() {
        setImg(BoardWindow.createImageIcon("path"));
        positions = new ArrayList<Point>();
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public PathInformationContainer(String owner) {
        super(BoardWindow.createImageIcon("path"),"Path of " + owner,owner);
        positions = new ArrayList<Point>();
    }

    public ArrayList<Point> getPositions() {
        return positions;
    }

    public void setPositions(ArrayList<Point> positions) {
        this.positions = positions;
    }

    public int getOffsetXPixel() {
        return offsetXPixel;
    }

    public int getOffsetYPixel() {
        return offsetYPixel;
    }

    public void setOffsetXPixel(int offsetXPixel) {
        this.offsetXPixel = offsetXPixel;
    }

    public void setOffsetYPixel(int offsetYPixel) {
        this.offsetYPixel = offsetYPixel;
    }

    @Override
    public boolean isShareEnabled() {
        return shareEnabled;
    }

}
