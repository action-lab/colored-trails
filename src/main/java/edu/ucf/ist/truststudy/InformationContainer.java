/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucf.ist.truststudy;

import java.awt.Color;
import javax.swing.ImageIcon;

/**
 *
 * @author culturelab
 */
public class InformationContainer {

    public ImageIcon img;
    public String text;
    public int row;
    public int col;
    public String owner;
    public Color color;
    public boolean shareEnabled = false;
    public String subType;
    public String description = null;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getSubType() {
        return subType;
    }

    public boolean isShareEnabled() {
        return /*subType.equals("payoff") ||*/ subType.equals("damage") || subType.equals("bonus");
    }

    public boolean isObstacle() {
        return subType.equals("obstacle");
    }

    public boolean isPublic() {
        if (subType == null) return false;
        return subType.equals("goal") || subType.equals("obstacle") || subType.equals("start") || subType.equals("dest");
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setShareEnabled(boolean shareEnabled) {
        this.shareEnabled = shareEnabled;
    }

    public InformationContainer() {
    }

    public InformationContainer(ImageIcon img, String text, String owner) {
        this.img = img;
        this.text = text;
        this.owner = owner;
    }

    public InformationContainer(ImageIcon img, String text, String owner, int row, int col) {
        this.img = img;
        this.text = text;
        this.row = row;
        this.col = col;
        this.owner = owner;
    }


    public ImageIcon getImg() {
        return img;
    }

    public String getText() {
        return text;
    }

    public void setImg(ImageIcon img) {
        this.img = img;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwner() {
        return owner;
    }


}
