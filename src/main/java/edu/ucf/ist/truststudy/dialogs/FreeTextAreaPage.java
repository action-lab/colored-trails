package edu.ucf.ist.truststudy.dialogs;

import java.awt.Dimension;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import edu.ucf.ist.dialogs.Nav;
import edu.ucf.ist.dialogs.Page;
import net.miginfocom.swing.MigLayout;

public abstract class FreeTextAreaPage extends Page {

  private static final long serialVersionUID = 1L;
  String mainText = "";
  protected JTextArea textArea;

  public FreeTextAreaPage(String mainText) {
    this.mainText = mainText;
    init();
  }

  private void init() {
    setLayout(new MigLayout("flowy, inset 5", "[grow,fill]", "20%[fill,10%]20%[fill,50%]"));

    JTextPane textPane = new JTextPane();
    textPane.setText(mainText);
    StyledDocument doc = textPane.getStyledDocument();
    SimpleAttributeSet attrs = new SimpleAttributeSet();
    StyleConstants.setAlignment(attrs, StyleConstants.ALIGN_CENTER);
    doc.setParagraphAttributes(0, doc.getLength(), attrs, false);
    textPane.setPreferredSize(new Dimension());
    textPane.setOpaque(false);
    textPane.setEditable(false);
    textPane.setFocusable(false);
    add(textPane);
    textArea = new JTextArea();
    add(textArea);
    textArea.getDocument().addDocumentListener(new DocumentListener() {

      @Override
      public void removeUpdate(DocumentEvent e) {
        validateNav();
      }

      @Override
      public void insertUpdate(DocumentEvent e) {
        validateNav();
      }

      @Override
      public void changedUpdate(DocumentEvent arg0) {
        validateNav();
      }
    });
  }

  private void validateNav(){
    if (textArea.getText().isEmpty()) {
      invalidListeners.forEach((listener)->{
        listener.call(this);
      });
    } else {
      validListeners.forEach((listener)->{
        listener.call(this);
      });
    }
  }

  @Override
  public boolean isReadyToAdvance() {
    return !textArea.getText().isEmpty();
  }
}
