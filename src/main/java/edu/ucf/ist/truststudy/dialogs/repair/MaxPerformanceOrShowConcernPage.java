package edu.ucf.ist.truststudy.dialogs.repair;

import java.awt.Dimension;
import javax.swing.JDialog;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import edu.ucf.ist.dialogs.DialogWizard.Data;
import edu.ucf.ist.dialogs.Page;
import net.miginfocom.swing.MigLayout;

public class MaxPerformanceOrShowConcernPage extends Page {

  private static final long serialVersionUID = -7607402596416909601L;

  private static final String MAX_PERFORMANCE_TEXT = "Great news! Your partner achieved maximum efficiency in this stage. Because of this you will both be awarded with an additional 50 fuel to aid your team in delivering resources.";

  private static final String SHOW_CONCERN_TEXT = "Your partner's experimenter has sent you the following message, \"Your teammate asked me to pass this message to you, \"Hey thanks for helping to meet our goals. You've been a great teammate!\"\"";

  boolean showConcern = false;

  public MaxPerformanceOrShowConcernPage(boolean showConcern) {
    this.showConcern = showConcern;
    init();
  }

  public void init() {
    setLayout(new MigLayout("flowy, inset 5", "[grow,fill]", "20%[fill,10%]20%[fill,50%]"));

    JTextPane textPane = new JTextPane();
    textPane.setText(showConcern ? SHOW_CONCERN_TEXT : MAX_PERFORMANCE_TEXT);
    StyledDocument doc = textPane.getStyledDocument();
    SimpleAttributeSet attrs = new SimpleAttributeSet();
    StyleConstants.setAlignment(attrs, StyleConstants.ALIGN_CENTER);
    doc.setParagraphAttributes(0, doc.getLength(), attrs, false);
    textPane.setPreferredSize(new Dimension());
    textPane.setOpaque(false);
    textPane.setEditable(false);
    textPane.setFocusable(false);
    add(textPane);
  }

  @Override
  public boolean isReadyToAdvance() {
    return true;
  }

  @Override
  public Data getData() {
    return null;
  }

  public static void main(String[] args) {

    JDialog dialog = new JDialog();
    dialog.getContentPane().add(new MaxPerformanceOrShowConcernPage(true));
    dialog.pack();
    dialog.setVisible(true);
   
  }
}