package edu.ucf.ist.truststudy.dialogs.repair;

import java.awt.Dimension;
import javax.swing.JFrame;
import edu.ucf.ist.dialogs.DialogWizard.Data;
import edu.ucf.ist.truststudy.dialogs.FreeTextAreaPage;

public class RepairResponseOncePage extends FreeTextAreaPage {
  private static final long serialVersionUID = -5695499542842908873L;

  static final String MAIN_TEXT =
      "Please briefly describe anything unordinary that happened in this round.";

  public RepairResponseOncePage() {
    super(RepairResponseOncePage.MAIN_TEXT);
    init();
  }

  public void init() {
  }

  @Override
  public Data getData() {
    Data data = new Data();
    data.put("two", textArea.getText());
    return data;
  }

  public static void main(String[] args) {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setLocationRelativeTo(null);
    frame.setPreferredSize(new Dimension(640, 480));
    frame.add(new RepairResponseOncePage());
    frame.pack();
    frame.setVisible(true);
  }
}
