package edu.ucf.ist.truststudy.dialogs.repair;

import java.awt.Dimension;
import javax.swing.JFrame;
import edu.ucf.ist.dialogs.DialogWizard.Data;
import edu.ucf.ist.truststudy.dialogs.FreeTextAreaPage;

public class RepairResponseTwoPage extends FreeTextAreaPage {

  private static final long serialVersionUID = 1L;

  static final String MAIN_TEXT =
      "Why or how do you think something unordinary happened in this round?";

  public RepairResponseTwoPage() {
    super(RepairResponseTwoPage.MAIN_TEXT);
    init();
  }

  public void init() {
  }

  @Override
  public Data getData() {
    Data data = new Data();
    data.put("three", textArea.getText());
    return data;
  }

  public static void main(String[] args) {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setLocationRelativeTo(null);
    frame.setPreferredSize(new Dimension(640, 480));
    frame.add(new RepairResponseTwoPage());
    frame.pack();
    frame.setVisible(true);

  }

}
