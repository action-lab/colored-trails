package edu.ucf.ist.truststudy.dialogs.endofgame;

import javax.swing.JOptionPane;
import edu.ucf.ist.dialogs.DialogWizard;
import edu.ucf.ist.dialogs.PageFactory;
import edu.ucf.ist.dialogs.DialogWizard.InvalidSizeException;

public class EndGameDialogFactory extends PageFactory {

  private static final String name = "endofgamedialog";

  public EndGameDialogFactory() {
      addPage(new ParticipationPage());
  }

  public static void main(String[] args) throws InvalidSizeException {
    DialogWizard dialog = new DialogWizard(null,new EndGameDialogFactory());

    dialog.setOnSubmit(()->{
      JOptionPane.showMessageDialog(dialog, "Thank you!");
      dialog.getFormData().forEach((key,value)->{
        System.out.printf("key: %s, value: %s\n",key,value);
      });
      System.exit(0);
    });

    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
      public void windowClosing(java.awt.event.WindowEvent e) {
        System.exit(0);
      }
    });

    dialog.setVisible(true);
  }

  public static String getFactoryName() {
    return name;
  }

  @Override
  public String getName() {
    return name;
  }
}
