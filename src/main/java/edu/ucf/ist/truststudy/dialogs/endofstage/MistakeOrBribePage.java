package edu.ucf.ist.truststudy.dialogs.endofstage;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import edu.ucf.ist.dialogs.DialogWizard.Data;
import edu.ucf.ist.truststudy.dialogs.MultipleChoicePage;

public class MistakeOrBribePage extends MultipleChoicePage {
  private static final long serialVersionUID = -5695499542842908873L;

  Data data = new Data();

  static final String MISTAKE_TEXT =
      "In the last round, your teammate forgot to pick up their regolith. How would you like to respond to your partner's actions?";

  static final String BRIBE_TEXT = "In the last round, your teammate accepted a bribe. How would you like to respond to your partner's actions?";

  private static final String retaliate = "retaliate";
  public static String getRetaliateText(){return retaliate;}
  private static final int retaliateValue = 100;
  public static int getRetaliateValue(){return retaliateValue;}
  private static final String ignore = "ignore";
  public static String getIgnoreText(){return ignore;}
  private static final String pick_up_slack = "pick_up_slack";
  public static String getPickUpSlackText(){return pick_up_slack;}
  private static final int slackValue = 20;
  public static int getSlackValue(){return slackValue;}
  private static final String confront = "confront";
  public static String getConfrontText(){return confront;}
  private static final String notify = "notify";
  public static String getNotifyText(){return notify;}

  static final Data CHOICES;
  static
  {
    CHOICES = new Data();
    CHOICES.put(retaliate,"Retaliate by taking "+retaliateValue+" fuel from your teammate. ");
    CHOICES.put(ignore,"Ignore this incident and move on.");
    CHOICES.put(pick_up_slack,"Forfeit "+slackValue+" of your gold coins to make up for the missing resource.");
    CHOICES.put(confront,"Send your partner a message.");
    CHOICES.put(notify,"Request the experimenter to remind your teammate of their objectives.");
  };


  public MistakeOrBribePage(boolean mistake) {
    super(mistake ? MistakeOrBribePage.MISTAKE_TEXT : MistakeOrBribePage.BRIBE_TEXT, MistakeOrBribePage.CHOICES);
    init();
  }

  private static final String confront_response = "confront_response";
  public static String getConfrontResponseText(){return confront_response;}

  private String response = "";
  
  public void init() {
    addValidListener((btn)->{
      if(JRadioButton.class.isInstance(btn)){
        JRadioButton b = JRadioButton.class.cast(btn);
        if(b != null && b.getActionCommand().equals(confront) && b.isSelected()){
          String tmp = JOptionPane.showInputDialog(this, "Enter the message?",response);

          if(tmp != null)
            response = tmp;

        }
      }
    });
  }

  @Override
  public boolean isReadyToAdvance() {
    return group.getSelection() != null;
  }

  public static void main(String[] args) {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setLocationRelativeTo(null);
    frame.setPreferredSize(new Dimension(640, 480));
    frame.add(new MistakeOrBribePage(false));
    frame.pack();
    frame.setVisible(true);

  }

  private static final String choice = "choice";
  public static String getChoiceText(){return choice;}

  @Override
  public Data getData() {
    data.put(choice,group.getSelection().getActionCommand());
    if(group.getSelection().getActionCommand().equals(confront))
      data.put(confront_response,response);
    return data;
  }
}
