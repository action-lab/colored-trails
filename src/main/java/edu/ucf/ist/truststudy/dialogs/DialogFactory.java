package edu.ucf.ist.truststudy.dialogs;

import edu.ucf.ist.truststudy.dialogs.endofgame.EndGameDialogFactory;
import edu.ucf.ist.truststudy.dialogs.endofstage.EndStageDialogFactory;
import edu.ucf.ist.truststudy.dialogs.repair.RepairDialogFactory;

public class DialogFactory {
  public enum DialogType {
    ENDGAME, ENDSTAGE, REPAIR
  }

  public static String getName(DialogType type) {
    String name = "";
    switch (type) {
      case ENDGAME:
        name = EndGameDialogFactory.getFactoryName();
        break;
      case ENDSTAGE:
        name = EndStageDialogFactory.getFactoryName();
        break;
      case REPAIR: 
        name = RepairDialogFactory.getFactoryName();
        break;
    }

    return name;
  }

}