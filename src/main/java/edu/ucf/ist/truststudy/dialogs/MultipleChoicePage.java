package edu.ucf.ist.truststudy.dialogs;

import java.awt.Dimension;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import edu.ucf.ist.dialogs.Page;
import edu.ucf.ist.dialogs.DialogWizard.Data;
import net.miginfocom.swing.MigLayout;

public abstract class MultipleChoicePage extends Page {

  private static final long serialVersionUID = 1L;
  protected ButtonGroup group = new ButtonGroup();
  String mainText = "";
  Data choices = new Data();

  public MultipleChoicePage(String mainText, Data choices) {
    this.mainText = mainText;
    this.choices = choices;
    init();
  }

  private void init() {
    setLayout(new MigLayout("flowy, inset 5", "[grow,fill]", "20%[fill,10%]20%[fill,50%]"));

    JTextPane textPane = new JTextPane();
    textPane.setText(mainText);
    StyledDocument doc = textPane.getStyledDocument();
    SimpleAttributeSet attrs = new SimpleAttributeSet();
    StyleConstants.setAlignment(attrs, StyleConstants.ALIGN_CENTER);
    doc.setParagraphAttributes(0, doc.getLength(), attrs, false);
    textPane.setPreferredSize(new Dimension());
    textPane.setOpaque(false);
    textPane.setEditable(false);
    textPane.setFocusable(false);
    add(textPane);
    JPanel tempPanel = new JPanel();
    tempPanel.setLayout(new MigLayout("flowy"));
    choices.forEach((action, choice) -> {
      JRadioButton btn = new JRadioButton(choice);
      btn.setActionCommand(action);
      btn.getModel().setGroup(group);
      btn.addItemListener((e) -> {
        validListeners.forEach((listener) -> {
          listener.call(btn);
        });
      });
      tempPanel.add(btn);
    });
    add(tempPanel);
  }
}
