package edu.ucf.ist.truststudy.dialogs.endofstage;

import javax.swing.JOptionPane;
import edu.ucf.ist.dialogs.DialogWizard;
import edu.ucf.ist.dialogs.DialogWizard.InvalidSizeException;
import edu.ucf.ist.dialogs.PageFactory;

public class EndStageDialogFactory extends PageFactory{

  private static final String name = "endofstagedialog";

  public EndStageDialogFactory(){}

  public EndStageDialogFactory(boolean repair, boolean behavior, boolean mistake) {
    if(repair)
      addPage(new NoMaxPerformancePage());
    
    if(behavior)
      addPage(new MistakeOrBribePage(mistake));

  }

  public static void main(String[] args) throws InvalidSizeException {
    DialogWizard dialog = new DialogWizard(null,new EndStageDialogFactory(true,true,true));

    dialog.setOnSubmit(()->{
      JOptionPane.showMessageDialog(dialog, "Thank you!");
      dialog.getFormData().forEach((key,value)->{
        System.out.printf("key: %s, value: %s\n",key,value);
      });
      System.exit(0);
    });

    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
      public void windowClosing(java.awt.event.WindowEvent e) {
        System.exit(0);
      }
    });

    dialog.setVisible(true);
  }

  @Override
  public String getName() {
    return name;
  }

  public static String getFactoryName() {
    return name;
  }
}
