package edu.ucf.ist.truststudy.dialogs.endofstage;

import javax.swing.JDialog;
import javax.swing.JTextArea;
import edu.ucf.ist.dialogs.DialogWizard.Data;
import edu.ucf.ist.dialogs.Page;

public class NoMaxPerformancePage extends Page {

  private static final long serialVersionUID = -7607402596416909601L;

  private String mainText = "You did great in delivering resources, but you didn’t achieve maximum performance.";

  public NoMaxPerformancePage() {
    init();
  }

  public void init() {
    JTextArea textArea = new JTextArea();
    add(textArea);
    textArea.setText(mainText);
    textArea.setWrapStyleWord(true);
    // textArea.setLineWrap(true);
    textArea.setOpaque(false);
    textArea.setEditable(false);
    textArea.setFocusable(false);
  }

  @Override
  public boolean isReadyToAdvance() {
    return true;
  }

  public static void main(String[] args) {

    JDialog dialog = new JDialog();
    dialog.getContentPane().add(new NoMaxPerformancePage());
    dialog.pack();
    dialog.setVisible(true);
   
  }
  
  @Override
  public Data getData() {
    return null;
  }
}