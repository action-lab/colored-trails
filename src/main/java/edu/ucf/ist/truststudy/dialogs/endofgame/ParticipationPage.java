package edu.ucf.ist.truststudy.dialogs.endofgame;

import java.awt.Dimension;
import javax.swing.JFrame;
import edu.ucf.ist.dialogs.DialogWizard.Data;
import edu.ucf.ist.truststudy.dialogs.MultipleChoicePage;

public class ParticipationPage extends MultipleChoicePage {

  private static final long serialVersionUID = 1L;

  static final String MAIN_TEXT =
      "Congratulations on completing the final stage! We have an important question for you: If you had the opportunity to play this game again, would you choose your current partner or a different partner?";

  static final Data CHOICES;
  static {
    CHOICES = new Data();
    CHOICES.put("current", "I would choose to work with my current partner.");
    CHOICES.put("different", "I would choose to work with a different partner.");
  };

  public ParticipationPage() {
    super(ParticipationPage.MAIN_TEXT, ParticipationPage.CHOICES);
    init();
  }

  public void init() {
  }

  @Override
  public boolean isReadyToAdvance() {
    return group.getSelection() != null;
  }

  public static void main(String[] args) {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setLocationRelativeTo(null);
    frame.setPreferredSize(new Dimension(640, 480));
    frame.add(new ParticipationPage());
    frame.pack();
    frame.setVisible(true);

  }

  private static final String participation = "participation";
  public static String getParticipationText(){return participation;}

  @Override
  public Data getData() {
    Data data = new Data();
    data.put(participation, group.getSelection().getActionCommand());
    return data;
  }

}
