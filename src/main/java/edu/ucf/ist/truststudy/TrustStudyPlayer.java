/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucf.ist.truststudy;

import edu.ucf.ist.def.AbstractServer;
import edu.ucf.ist.def.DefaultPlayer;
import java.awt.Color;

/**
 *
 * @author culturelab
 */
public class TrustStudyPlayer extends DefaultPlayer {
    Color color;
    int pathID;
    int offsetX;
    int offsetY;
    public enum ProposalStates { Proposed, Declined, Retracted, Undecided, Accepted, Confirmed };
    ProposalStates proposalState = ProposalStates.Undecided;


    long finalPathTime;
    float score = 0; //TODO: field not used
    boolean tookBribe;
    String reparation = new String();
    String reparationString1 = new String();
    String reparationString2 = new String();
    String apologyAccept = null;
    String participationAccept = null;
    public int distanceTravelled = 0; //score
    int shared = 0; //incremented and reported in summary
    int unShared = 0; //decremented and reported in summary
    public int currencyCollected = 0; //score
    public int currencyPoints = 0; //score
    public int resourceRemaining = 0; //score
    long executeTime = 0; //set but not used
    int proposalCount = 0; //incremented but not used
    int damageShare = 0; //score
    int bonusShare = 0; //score
    StringBuffer roundSummary = new StringBuffer(500);

    public enum ReparationType {
        MaxPerformance, ShowConcern
    }
    
    ReparationType reparationType = ReparationType.MaxPerformance;

    public ReparationType getReparationType() {
        return this.reparationType;
    }

    public void setReparationType(ReparationType reparationType) {
        this.reparationType = reparationType;
    }

    public TrustStudyPlayer(String id, AbstractServer as, String name, String cc, Color color, int offsetX, int offsetY) {
        super(id,as,name,cc);
        this.color = color;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getPathID() {
        return pathID;
    }

    public void setPathID(int pathID) {
        this.pathID = pathID;
    }

    public int getOffsetY() {
        return offsetY;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public void setOffsetY(int offsetY) {
        this.offsetY = offsetY;
    }

    public void setOffsetX(int offsetX) {
        this.offsetX = offsetX;
    }

}
