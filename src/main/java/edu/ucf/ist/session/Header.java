/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucf.ist.session;

import java.util.ArrayList;

/**
 *
 * @author dougcooper
 */
public class Header {
    
    public class Names extends ArrayList<String>{

        private static final long serialVersionUID = 7084196156497126429L;
    }
    
    public static final String ExperimentNumber = "Experiment Number";
    public static final String StageNumber = "Stage Number";
    
    protected Names _names;
    
    protected Header(){
        _names = new Names(){{
        add(ExperimentNumber);
        add(StageNumber);
    }};
    }
    
    public Names getNames() {
        return _names;
    }
    
}
