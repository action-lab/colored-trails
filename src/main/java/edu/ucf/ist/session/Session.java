package edu.ucf.ist.session;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.reflect.FieldUtils;
import edu.ucf.ist.session.StageResults.Entries;

/**
 *
 * @author dougcooper
 */
public class Session {

    public class StageData extends ArrayList<StageResults> {

        private static final long serialVersionUID = 6995407409144004557L;
    }

    private int _id;
    protected StageData _data;
    protected Entries _entries;
    private File _logFile;
    private Header _header;
    BufferedWriter log;

    /**
     * The main class that provides file logging facilities for a research session.
     * Each Session gets an Entry which makes up a row in the log.
     * Each Entry is comprised of fields which contains the research data.
     * 
     * @param id
     * @param logFile
     * @param header
     */
    public Session(int id, File logFile, Header header) {
        _id = id;
        _logFile = logFile;
        _header = header;
        _data = new StageData();
        _entries = new Entries();
        try {
            log = new BufferedWriter(new FileWriter(_logFile));
        } catch (IOException ex) {
            Logger.getLogger(Session.class.getName()).log(Level.SEVERE, null, ex);
        }

        StringBuilder s = new StringBuilder();

        _header.getNames().forEach((name) -> {
            s.append(name).append(",");
        });

        write(s.toString());
    }
    
    /**
     * Adds an entry with the option to log immediately.
     * @param entry
     * @param log
     */
    public void addEntry(Entry entry, boolean log){
        entry.setExperimentNumber(String.valueOf(_id));

        if (log) {
            logEntry(entry);
        }
        
    }

    public void addResults(StageResults results, boolean log) {
        addResults(results);

        if (log) {
            results.getEntries().forEach((entry) -> {
                logEntry(entry);
            });

        }
    }

    public void addResults(StageResults e) {
        e.getEntries().forEach((entry) -> {
            entry.setExperimentNumber(String.valueOf(_id));
        });
        _data.add(e);
    }

    /**
     * Logs an entry without storing the entry.
     * @param entry
     */
    public void logEntry(Entry entry) {
        StringBuilder s = new StringBuilder();

        //get all the public fields
        List<Field> fields = entry.getEntryFields();
        
        //try to match public field annotations against the headers
        _header.getNames().forEach((headerName) -> {
            for (Field field : fields) {
                try {
                    if (headerName.equals(field.getAnnotation(EntryField.class).name())) {
                        s.append(((String)FieldUtils.readField(field, entry,true))).append(",");
                        break;
                    }
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(Session.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        write(s.toString());
    }

    /**
     * Logs entries that have been previously stored.
     */
    public void logEntries(){
        _entries.forEach((entry)->{
            logEntry(entry);
        });
    }

    /**
     * Helper method that writes message to the log.
     * @param msg
     */
    private void write(String msg){
        try {
            log.write(msg.toString());
            log.newLine();
            log.flush();
        } catch (IOException ex) {
            Logger.getLogger(Session.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public int getId() {
        return _id;
    }

    public StageData getStageData() {
        return _data;
    }

    public void close() throws IOException {
        log.close();
    }

    /**
     * A utility class for log related actions.
     */
    public static class LogUtils {
        public static File BuildLogFile(int experimentNumber, String path, String baseName){
            new File(path).mkdir();
            String fileName = baseName.replace(' ', '_') + "-"
                    + (new DecimalFormat("0000")).format(experimentNumber) + "-game-"
                    + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd-HmsA"))
                    + ".csv";

            return new File(path + fileName);
        }
    }

    public static void main(String[] args) {
        int experimentNumber = 0;
        String path = "logs/";
        String baseName = "testlog";

        File f = Session.LogUtils.BuildLogFile(experimentNumber, path, baseName);

        Session session = new Session(experimentNumber,f,new Header());

        StageResults results = new StageResults();

        Entry e = new Entry(0);
        results.addEntry(e);

        session.addResults(results, true);
    }

}
