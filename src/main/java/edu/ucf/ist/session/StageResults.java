/*
 * To change this license _header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucf.ist.session;

import java.util.ArrayList;

/**
 *
 * @author dougcooper
 */
public class StageResults {
    public static class Entries extends ArrayList<Entry>{

        private static final long serialVersionUID = 5021829130875429020L;
    };
    
    
    private Entries entries = new Entries();
    
    public StageResults(){
        
    }

    public Entries getEntries() {
        return entries;
    }

    public void setEntries(Entries entries) {
        this.entries = entries;
    }

    public void addEntry(Entry entry) {
        entries.add(entry);
    }
}