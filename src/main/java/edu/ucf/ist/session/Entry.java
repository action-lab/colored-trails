/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucf.ist.session;

import java.lang.reflect.Field;
import java.util.List;
import org.apache.commons.lang3.reflect.FieldUtils;

/**
 *
 * @author dougcooper
 */
public class Entry{
                
    protected static String defaultValue = "not set";
    
    public Entry(int stageNumber){
        this.StageNumber = String.valueOf(stageNumber);
    }
    
    @EntryField(name=Header.ExperimentNumber)
    private String ExperimentNumber = defaultValue;

    public String getExperimentNumber() {
        return ExperimentNumber;
    }

    protected void setExperimentNumber(String ExperimentNumber) {
        this.ExperimentNumber = ExperimentNumber;
    }
    
    @EntryField(name=Header.StageNumber)
    private String StageNumber = defaultValue;

    public String getStageNumber() {
        return StageNumber;
    }

    public List<Field> getEntryFields() {
      return FieldUtils.getFieldsListWithAnnotation(Entry.class, EntryField.class);
    }
    
}
