/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucf.ist.def;

import edu.ucf.ist.session.Session;
import java.io.File;
import javax.jms.JMSException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import org.w3c.dom.Element;

/**
 *
 * @author culturelab
 */
public interface ExperimentServer extends Runnable{

    /**
     * Returns a String with HTML code that will be displayed in GUI.
     * @return 
     */
    public String getHTMLFormattedStatus();

    public enum ExperimentServerStatus { Idle, Waiting, Connected, Running, Paused }

    //ExperimentServerStatus getServerStatus();

    // public void initObject(Element serverRoot) throws InstantiationException;
    public void initObject(Element serverRoot, int playerNum, EndOfGameListener listener) throws InstantiationException;

    public void setManager(ServerManagerFrame serverManager) throws JMSException, ParserConfigurationException, TransformerConfigurationException;

    /**
     * Called by framework to inform the server that player with id name has connected and is listening.
     * @param name
     */
    public void playerAdded(String name);

    /**
     * Asks the server to change status to "Waiting" and be ready to receive clients.
     * @param startWhenReady
     */
    public void waitForClients(boolean startWhenReady);

    /**
     * Executes the business logic of the experiment. Normally would involve processing
     * messages from the clients.
     */
    public void startExperiment();

    public ExperimentServerStatus getServerStatus();

    public Player[] getPlayers();

    public String getExperimentName();

    public String getName();
    
    /**
     *
     * @param id
     * @param logFile
     * @return Session
     */
    public Session getSession(int id, File logFile);

    public void endGameRequest();

    public void setLinks(ExperimentServer nextStage);

}
