/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucf.ist.def;

/**
 *
 * @author culturelab
 */
public class DefaultPlayer implements Player
{
    String id;
    boolean connected = false;
    AbstractServer as;
    String clientClass;
    String name;

    public DefaultPlayer(String id, AbstractServer as, String name, String cc) {
        this.id = id;
        this.as = as;
        this.clientClass = cc;
        this.name = name;
    }

    public DefaultPlayer(String id,AbstractServer as, String name) {
        this(id,as,name,"edu.ucf.ist.def.ex.chatclient.ChatClient");
    }


    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public String getID() {
        return id;
    }

    public String getClientClassName() {
        return clientClass;
    }

    public AbstractServer getParentServer() {
        return as;
    }

    public String getName() {
        return name;
    }

}
