/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucf.ist.def;

/**
 *
 * @author culturelab
 */
public interface DEFMessageListener {
    void processMessage(DEFMessage dm);
    boolean isMarkedForRemoval();
}
