/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucf.ist.def;
import javax.jms.JMSException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;

/**
 *
 * @author culturelab
 */
public interface ExperimentClient {

    public void initObject(String serverIp, int listernerPort, String serverCommId, String clientId) throws JMSException, ParserConfigurationException, TransformerConfigurationException ;

    public String getClientId();

    public String getServerId();

    public void addListener(DEFMessageListener dml);
    
    public void removeListener(DEFMessageListener dml);

    public void sendMessage(DEFMessage dm);
}
