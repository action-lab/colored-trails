/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucf.ist.def;

import org.w3c.dom.Element;

/**
 *
 * @author culturelab
 */
public class DEFMessage {
    public long time;
    public long messageId;
    public String sender;
    public String receiver;
    public Element mRoot;
    public String mStr;
    public enum DEFMessageDataType {DOM, String}
    public DEFMessageDataType type;
    public String name;

    public DEFMessage(long time, long messageId, String sender, String receiver, Element mRoot, String name) {
        this.time = time;
        this.messageId = messageId;
        this.sender = sender;
        this.receiver = receiver;
        this.mRoot = mRoot;
        this.name = name;
        this.type = DEFMessageDataType.DOM;
    }

    public DEFMessage(String sender, String receiver, String mStr) {
        this.sender = sender;
        this.receiver = receiver;
        this.mStr = mStr;
        this.type = DEFMessageDataType.String;
    }


}
