/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucf.ist.def;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * A class designed to help logging
 *
 * @author culturelab
 */
public class L {

    public static Logger mcl(Class c, Level level) {
        Logger l = Logger.getLogger(c.getName());
        l.setLevel(level);
        ConsoleHandler ch = new ConsoleHandler();
        ch.setLevel(level);
        l.addHandler(ch);
        return l;
    }



//
//    HashMap<Class,RegistrationData> map = new HashMap<Class,RegistrationData>();
//
//
//    void register(Class c, boolean debug) {
//        map.put(c, new RegistrationData(debug,5,new PrintWriter(System.out)));
//    }
//
//    void register(Class c, boolean debug, int level ) {
//        map.put(c, new RegistrationData(debug,level,new PrintWriter(System.out)));
//    }
//
//    void register(Class c, boolean debug, int level, PrintStream ps ) {
//        map.put(c, new RegistrationData(debug,5,new PrintWriter(ps)));
//    }
//
//    void register(Class c, boolean debug, int level, String logFileName ) {
//        File f = new File(logFileName);
//        PrintWriter lpw;
//        try {
//            lpw = new PrintWriter(f);
//            map.put(c, new RegistrationData(debug,5,lpw));
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(L.class.getName()).log(Level.SEVERE, null, ex);
//            map.put(c, new RegistrationData(debug,5,new PrintWriter(System.out)));
//
//        }
//    }
//
//    void l(Class c, int level, String msg) {
//        if(c == null) {
//            System.out.println(msg);
//        }
//        RegistrationData rd = (RegistrationData) map.get(c);
//        if(rd == null) {
//            System.out.println(msg);
//        } else {
//            if(rd.debug && rd.level <= level) {
//                rd.ps.println("[" + c.getSimpleName() + "] " + msg);
//            }
//        }
//
//    }
//
//    @Override
//    protected void finalize() throws Throwable {
//        super.finalize();
//
//    }


}
//
//class RegistrationData {
//    public boolean debug;
//    public PrintWriter ps;
//    public int level;
//
//    public RegistrationData(boolean debug, int level, PrintWriter ps) {
//        this.debug = debug;
//        this.level = level;
//        this.ps = ps;
//    }
//
//    public void setDebug(boolean debug) {
//        this.debug = debug;
//    }
//
//
//
//
//}
