/** Distributed Experimentation Framework
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucf.ist.def;

import edu.ist.ucf.def.localization.Resource;
import edu.ist.ucf.def.localization.ResourceManager;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.w3c.dom.Element;

/**
 * An abstract class that is the parent class to all the experiment class you may write.
 *
 * Guidelines: When inheriting from this class, you must have a no parameter constructor (TODO: r u sure?).
 *
 * @author culturelab
 */
abstract public class AbstractServer implements ExperimentServer {

    private static Logger l = L.mcl(AbstractServer.class, Level.FINER);
    protected String experimentName;
    protected String serverCommId;
//    public enum ExperimentServerStatus {
//
//        Idle, Waiting, Running, Paused
//    }
    protected ServerManagerFrame manager;
    protected ExperimentServerStatus status = ExperimentServerStatus.Idle;
    protected Player[] players;
    protected String name;
    protected String venue;
// ganil: for talking with the chat
    protected JMSHelper jh = null;
    protected BlockingQueue<DEFMessage> messageQueue = new ArrayBlockingQueue<>(200);
    protected Resource r;
    protected ResourceManager rm;
    protected boolean startWhenReady;
    protected ExperimentServer previousStage = null;
    protected ExperimentServer nextStage = null;

    public ExperimentServer getPreviousStage() {
        return this.previousStage;
    }

    public void setPreviousStage(ExperimentServer previousStage) {
        this.previousStage = previousStage;
    }

    public AbstractServer previousStage(ExperimentServer previousStage) {
        this.previousStage = previousStage;
        return this;
    }

    public ExperimentServer getNextStage() {
        return this.nextStage;
    }

    public void setNextStage(ExperimentServer nextStage) {
        this.nextStage = nextStage;
    }

    public AbstractServer nextStage(ExperimentServer nextStage) {
        this.nextStage = nextStage;
        return this;
    }

    @Override
    public void setLinks(ExperimentServer nextStage){
        this.nextStage = nextStage;
        ((AbstractServer)nextStage).setPreviousStage(this);
    }

    @Override
    public String getName() {
        return name;
    }

    public String getServerCommId() {
        return serverCommId;
    }

    @Override
    public String getExperimentName() {
        return experimentName;
    }

    @Override
    public String getHTMLFormattedStatus() {
        String statusString = "<html><font size=\"4\">" + name + "</font><br>"
                + "<font size=\"2\">" + experimentName + "</font><br>"
                + "<i>" + status.toString() + "</i><br>";
        for (Player dp : players) {
            statusString += "<font color=\"" + (dp.isConnected() ? "green" : "red") + "\">" + dp.getID() + "</font> ";
        }
        return statusString;
    }

    @Override
    public ExperimentServerStatus getServerStatus() {
        return status;
    }

    //------------------------------------
    // Manage players
    /**
     * The Manager uses the returned array to share player information with the server.
     * @return 
     */
    @Override
    public Player[] getPlayers() {
        return players;
    }

    /**
     * Sets the players field; to be used by an inheriting class when prior information about players is available.
     * @param players
     */
    protected void setPlayers(Player[] players) {
        this.players = players;
    }

    /**
     * Marks the player as connected; override and call super(...) if additional bookkeeping is
     * required when a player joins.
     * @param name
     */
    @Override
    public void playerAdded(String name) {
        for (Player p : players) {
            if (p.getID().equals(name)) {
                p.setConnected(true);
            }
        }
        checkAndStart();
    }

    protected void checkAndStart() {
        boolean allPresent = true;
        for (Player p : players) {
            if (!p.isConnected()) {
                allPresent = false;
            }
        }

        if (allPresent) {
            status = ExperimentServerStatus.Connected;
            if(startWhenReady)
                manager.startServer(this);
        }
    }

    /**
     * Remembers the manager and initializes a connection to the JMS server run by it.
     * @param serverManager
     * @throws JMSException
     * @throws ParserConfigurationException
     * @throws TransformerConfigurationException
     */
    @Override
    public void setManager(ServerManagerFrame serverManager) throws JMSException, ParserConfigurationException, TransformerConfigurationException {
        manager = serverManager;

        jh = new JMSHelper() {

            @Override
            public void onMessage(DEFMessage msg) {
                boolean success = messageQueue.offer(msg);
                if (success == false) {
                    l.severe("messageQueue full! Exiting...");
                    System.exit(1);

                }
            }
        };
        jh.initConnection(ServerManagerFrame.getServerHostnameIP(), manager.getListeningPort(), getServerCommId(), new String[]{getServerCommId()});

    }

    @Override
    abstract public void startExperiment();

    @Override
    public void waitForClients(boolean startWhenReady) {
        if(status == ExperimentServerStatus.Idle)
                status = ExperimentServerStatus.Waiting;
        this.startWhenReady = startWhenReady;
        checkAndStart();
    }

    @Override
    public void run() {
        status = ExperimentServerStatus.Running;
        startExperiment();
        status = ExperimentServerStatus.Idle;

    }

    protected void disconnectClients(String message) throws JMSException, TransformerException {
        jh.send(new DEFMessage(serverCommId, experimentName + ".all", "<message name=\"disconnect\">" + (message == null ? r.s("registration.terminated") : message) + "</message>"));
        for (Player p : players) {
            p.setConnected(false);
        }
    }

    /**
     * This function will create an object of class cls which should be a child class
     * of AbstractServer and load parameters from the XML node serverRoot.
     * When you inherit from this class for putting business logic, and need to handle more parameters
     * from serverRoot, call super(..,..) and set the parameters on the returned
     * value and return it.
     * 
     * @param serverRoot
     * @throws InstantiationException
     */
    public void initObject(Element serverRoot) throws InstantiationException {
        // cls has to be a child class of AbstractServer
        name = serverRoot.getAttribute("name");
        venue = serverRoot.getAttribute("venue");
        experimentName = serverRoot.getAttribute("experimentId");
        serverCommId = experimentName + ".server";
        r = ResourceManager.createResourceManager( serverRoot.getElementsByTagName("resources")).getDefaultResource();
        players = new Player[0];
    }

    /**
     * Returns a message from the queue; to be actually used by the business logic.
     * Converts a regular XML text message to a DEFMessage.
     * @return
     * @throws java.lang.InterruptedException
     */
    public DEFMessage getMessage() throws InterruptedException {
        DEFMessage dm = null;
        do {
            l.finer("Waiting for a message.");
            dm = messageQueue.take();
            l.finer("Got a message.");
        } while (dm == null);
        return dm;
    }

    /**
     * Sends a message using the publisher; to be actually used by the business logic.
     * Converts a DEFMessage to a regular XML text message.
     * @param message
     */
    public void sendMessage(DEFMessage message) {
        try {
            jh.send(message);
        } catch (TransformerException | JMSException ex) {
            Logger.getLogger(AbstractServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void processMessage(DEFMessage dm) {
        
    }

    
}
