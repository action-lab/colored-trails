/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucf.ist.def;

/**
 *
 * @author culturelab
 */
public interface Player {
    boolean isConnected();
    String getID();
    public void setConnected(boolean b);
    String getClientClassName();
    AbstractServer getParentServer();
    String getName();
}
