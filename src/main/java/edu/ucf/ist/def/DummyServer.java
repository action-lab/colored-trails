/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucf.ist.def;

import edu.ucf.ist.session.Session;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.xml.transform.TransformerException;
import org.w3c.dom.Element;

/**
 *
 * @author culturelab
 */
public class DummyServer extends AbstractServer {

    private static Logger l = L.mcl(DummyServer.class, Level.FINER);

    @Override
    public void initObject(Element serverRoot) throws InstantiationException {
        super.initObject(serverRoot);
        players = new Player[1];
        players[0] = new DefaultPlayer(experimentName + ".A", this,"A", "edu.ucf.ist.truststudy.TrustStudyClient");
//        players[1] = new DummyPlayer(experimentName + ".B", this);
//        players[2] = new DummyPlayer(experimentName + ".C", this);
//        players[3] = new DummyPlayer(experimentName + ".D", this);
    }

    @Override
    public void initObject(Element serverRoot, int playerNum, EndOfGameListener listener) throws InstantiationException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    public void startExperiment() {
        try {
            Thread.sleep(1000);
            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"trustStudy.init\"></message>"));
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"chat.addMessage\"><chatMessage lifeTime=\"10000\" >Howdi! Welcome to the most awesome server</chatMessage></message>"));
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"chat.addMessage\"><chatMessage lifeTime=\"10000\" >1.</chatMessage></message>"));
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"chat.addMessage\"><chatMessage lifeTime=\"9000\" >2.</chatMessage></message>"));
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"chat.addMessage\"><chatMessage lifeTime=\"8000\" >3.</chatMessage></message>"));
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"chat.addMessage\"><chatMessage lifeTime=\"7000\" >4.</chatMessage></message>"));
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"chat.addMessage\"><chatMessage lifeTime=\"6000\" >5.</chatMessage></message>"));
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"chat.addMessage\"><chatMessage lifeTime=\"5000\" >6.</chatMessage></message>"));
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"chat.addMessage\"><chatMessage lifeTime=\"4000\" >7.</chatMessage></message>"));
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"chat.addMessage\"><chatMessage lifeTime=\"3000\" >8.</chatMessage></message>"));
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"chat.addMessage\"><chatMessage lifeTime=\"2000\" >9.</chatMessage></message>"));
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"chat.addMessage\"><chatMessage lifeTime=\"11000\" >Hope you are doing well.</chatMessage></message>"));
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"proposal.new\"><proposalID>0</proposalID></message>"));
//            Thread.sleep(2000);
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"proposal.setStatus\"><proposalID>0</proposalID><status text=\"MAIN TEXT\" buttonEnabled=\"true\" status=\"STATUS\"/></message>"));
//            Thread.sleep(5000);
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"proposal.setStatus\"><proposalID>0</proposalID><status text=\"MAIN TEXT\" buttonEnabled=\"false\" status=\"NO MORE!\"/></message>"));
//            Thread.sleep(5000);
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"proposal.close\"><proposalID>0</proposalID></message>"));


//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"dialog.new\"><dialogID>0</dialogID></message>"));
//            Thread.sleep(5000);
//            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"dialog.setStatus\"><dialogID>0</dialogID><status text=\"MAIN TEXT\" buttonEnabled=\"true\" buttonText=\"OK\"/></message>"));

            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"proposalList.new\"><dialogID>0</dialogID></message>"));
            sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"proposalList.setStatus\"><dialogID>0</dialogID><status cancelEnabled=\"true\" ><data>Path 0</data><data>Path 1</data></status></message>"));

            //Thread.sleep(5000);
            //sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"dialog.close\"><dialogID>0</dialogID></message>"));
            NEXTMESSAGE:
            while(true) {
                DEFMessage msg = getMessage();
                if(msg.name.equals("proposalList.propose")) {
                    Player sender = null;
                    for(Player p : players) {
                        if(p.getID().equals(msg.sender)) sender = p;
                    }
                    String proposedValue = msg.mRoot.getElementsByTagName("data").item(0).getFirstChild().getNodeValue();
                    sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"proposal.new\"><proposalID>0</proposalID></message>"));
                    sendMessage(new DEFMessage(getServerCommId(), experimentName + ".all", "<message name=\"proposal.setStatus\"><proposalID>0</proposalID>"
                            + "<status text=\"Player " + sender.getName() + " proposed option " + proposedValue + "\" buttonEnabled=\"true\" status=\"\"/></message>"));

                }
            }

        } catch (InterruptedException ex) {
            Logger.getLogger(AbstractServer.class.getName()).log(Level.WARNING, "Dummy Server was killed", ex);
        }
        try {
            disconnectClients(null);
        } catch (TransformerException ex) {
            Logger.getLogger(DummyServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JMSException ex) {
            Logger.getLogger(DummyServer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    protected void processMessage(DEFMessage dm) {
        
    }

    public void endGameRequest() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void initObject(Element serverRoot, boolean hs, int playerNum) throws InstantiationException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void initObject(Element serverRoot, boolean hs, int playerNum, EndOfGameListener listener) throws InstantiationException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Session getSession(int id, File logFile) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
