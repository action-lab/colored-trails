/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
* ServerMainFrame.java
*
* Created on Jul 8, 2010, 3:58:25 PM
*/
package edu.ucf.ist.def;

import edu.ist.ucf.def.localization.Resource;
import edu.ist.ucf.def.localization.ResourceManager;
import edu.ucf.ist.def.ExperimentServer.ExperimentServerStatus;
import edu.ucf.ist.session.Session;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import org.apache.activemq.broker.BrokerService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 *
 * @author culturelab
 */
public class ServerManagerFrame extends javax.swing.JFrame implements Runnable {

    private static Logger l = L.mcl(ServerManagerFrame.class, Level.FINE);
    DefaultListModel serverListData = new DefaultListModel();
    Thread updateThread = null;
    HashMap<ExperimentServer, Thread> threadMap = new HashMap<ExperimentServer, Thread>();
    JPopupMenu popup;
    // Communication related stuff
    private int listeningPort = 8100;
    private static int player = 1;
    private BrokerService broker = new BrokerService();
    private String url;
    public DocumentBuilderFactory dbf;
    public DocumentBuilder db;
    public Transformer tf;
    JMSHelper jh;
    Resource r;
    Random rand = new Random();
    ExperimentServer server = null;
    String title;
    private Session session = null;

    /**
     * Creates new form ServerMainFrame
     */
    public ServerManagerFrame() {
        // temporary server loading
        // ExperimentServer es = DummyServer.;
        // serverListData.addElement(es);
        // threadMap.put(es, new Thread(es));
        // es = new DummyServer();
        // serverListData.addElement(es);
        // threadMap.put(es, new Thread(es));

        initComponents();

        try // -------------------------------------
        // read the xml configuration
        {
            dbf = DocumentBuilderFactory.newInstance();
            // Using factory get an instance of document builder
            db = dbf.newDocumentBuilder();
            tf = TransformerFactory.newInstance().newTransformer();
            // parse using builder to get DOM representation of the XML file
            Document dom = db.parse("managerConfig.xml");
            // get the root elememt
            Element rootNode = dom.getDocumentElement();
            listeningPort = Integer.parseInt(rootNode.getAttribute("listeningPort")) + player - 1;
            server = experimentServerFactory(rootNode);
            title = rootNode.getAttribute("title");
            setTitle(title + "--Player " + player + " - Port: " + listeningPort);
            r = ResourceManager.createResourceManager(rootNode.getElementsByTagName("resources")).getDefaultResource();

        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(ServerManagerFrame.class.getName()).log(Level.SEVERE, null, ex);
            l.severe("The framework communication subsystem could not be initialized. Exiting...");
            System.exit(1);
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(ServerManagerFrame.class.getName()).log(Level.SEVERE, null, ex);
            l.severe("The server manager could not be initialized from the XML file. Exiting...");
            System.exit(1);
        }

        // -------------------------------------
        // create the popup menu
        // action listeners are here too
        JMenuItem menuItem;
        popup = new JPopupMenu();

        // TODO: manual connection. is this necessary?
        // menuItem = new JMenuItem("Connect");
        // menuItem.addActionListener((ActionEvent e) -> {
        // ExperimentServer es = (ExperimentServer)
        // serverListData.getElementAt(jList1.getSelectedIndex());
        // if (es.getServerStatus() == ExperimentServerStatus.Idle) {
        // es.waitForClients(false);
        //
        // } else {
        // l.info("The server has already started!");
        // }
        // });
        // popup.add(menuItem);
        menuItem = new JMenuItem("Start");
        menuItem.addActionListener((ActionEvent e) -> {
            ExperimentServer es = (ExperimentServer) serverListData.getElementAt(jList1.getSelectedIndex());
            connectServer(es);
        });
        popup.add(menuItem);
        // TODO: pause game
        // menuItem = new JMenuItem("Pause");
        // menuItem.addActionListener((ActionEvent e) -> {
        // l.warning("The PAUSE function is not implemented yet.");
        // });
        // popup.add(menuItem);
        menuItem = new JMenuItem("End Game");
        menuItem.addActionListener((ActionEvent e) -> {
            ExperimentServer es = (ExperimentServer) serverListData.getElementAt(jList1.getSelectedIndex());
            stopServer(es);
            connectServer(es);
        });
        popup.add(menuItem);

        MouseListener popupListener = new java.awt.event.MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                maybeShowPopup(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                maybeShowPopup(e);
            }

            private void maybeShowPopup(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    JList jl = (JList) e.getComponent();
                    int index = jl.locationToIndex(e.getPoint());
                    jl.setSelectedIndex(index);
                    popup.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        };
        jList1.addMouseListener(popupListener);

        // -------------------------------
        // Initialize JMS
        try {
            broker.setUseJmx(true);
            url = "tcp://" + getServerHostnameIP() + ":" + listeningPort;
            l.config("Broker URL [Pay ATTENTION HERE! Use this IP in the clients])");
            l.config(url);
            broker.addConnector(url);
            broker.start();
        } catch (Exception e) {
            l.log(Level.SEVERE, "Error starting the JMS Broker: {0}", e);
        }

        // --------------------------------
        // Start listener for registration requests
        jh = new JMSHelper() {

            @Override
            public void onMessage(DEFMessage msg) {
                processMessage(msg);
            }
        };
        try {
            jh.initConnection(getServerHostnameIP(), listeningPort, "manager", new String[] { "manager" });
        } catch (JMSException | ParserConfigurationException | TransformerConfigurationException ex) {
            l.log(Level.SEVERE, null, ex);
            l.severe("Could not register for listening to client messages.");
            System.exit(1);
        }

        // --------------------------------
        // start the update thread
        updateThread = new Thread(this);
        updateThread.start();

    }

    private void connectServer(ExperimentServer es) {
        if (es.getServerStatus() == ExperimentServerStatus.Connected
                || es.getServerStatus() == ExperimentServerStatus.Idle) {
            es.waitForClients(true);

        } else {
            l.info("The server has already started!");
        }

    }

    public void startServer(ExperimentServer es) {
        threadMap.put(es, new Thread(es));
        threadMap.get(es).start();
    }

    private void pauseServer(ExperimentServer es) {
        // TODO: pause game
    }

    private void stopServer(ExperimentServer es) {
        if (es.getServerStatus() != ExperimentServerStatus.Idle) {
            es.endGameRequest();
            Thread t = threadMap.get(es);
            if (t != null && t.isAlive())
                t.interrupt();
        } else {
            l.info("The server is already idle.");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated
    // Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jPanel2 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Distributed Experimentation Framework - Server Interface");
        setPreferredSize(new java.awt.Dimension(700, 600));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setMinimumSize(new java.awt.Dimension(500, 56));
        jPanel1.setLayout(new java.awt.BorderLayout());

        jScrollPane1.setPreferredSize(new java.awt.Dimension(700, 300));

        jList1.setModel(serverListData);
        jList1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList1.setPreferredSize(new java.awt.Dimension(200, 200));
        jList1.setCellRenderer(new ServerListRenderer());
        jScrollPane1.setViewportView(jList1);

        jPanel1.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jButton1.setText("Load Stages");
        jButton1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1);

        jPanel1.add(jPanel2, java.awt.BorderLayout.NORTH);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        getAccessibleContext().setAccessibleName("Distributed Experimentation Framework - Server Manager");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    public int getListeningPort() {
        return listeningPort;
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton1ActionPerformed

        if (session != null) {
            try {
                // create a messge box
                if (JOptionPane.showConfirmDialog(null, "Do you want to reset the selection?", "Reset Selection",
                        JOptionPane.OK_CANCEL_OPTION) == JOptionPane.CANCEL_OPTION)
                    return;

                session.close();
                session = null;
                for (Object server : serverListData.toArray()) {
                    stopServer((ExperimentServer) server);
                }
                serverListData.clear();

            } catch (IOException ex) {
                Logger.getLogger(ServerManagerFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        loadServerFromXML();
        // jButton1.setEnabled(false);
    }// GEN-LAST:event_jButton1ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {// GEN-FIRST:event_formWindowClosing

        l.info("Shutting down all servers and exiting...");
        for (int i = 0; i < serverListData.size(); i++) {
            ExperimentServer es = (ExperimentServer) serverListData.getElementAt(i);
            if (es.getServerStatus() != ExperimentServerStatus.Idle) {
                es.endGameRequest();
                if (threadMap.get(es) != null) {
                    threadMap.get(es).interrupt();
                }
            }
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            Logger.getLogger(ServerManagerFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.exit(0);
    }// GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new ServerManagerFrame().setVisible(true);
        });

        player = Integer.parseInt(args[0]);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JList jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ServerManagerFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            repaint();
        }
    }

    private Session createSession() throws IOException {

        int experimentIndex = 0;
        String message = "Enter the experiment number:";
        // boolean haveInput = false;

        // File logFileGame;
        // do {
        try {
            experimentIndex = Integer.parseInt((String) JOptionPane.showInputDialog(null, message,
                    "Input experiment number", JOptionPane.PLAIN_MESSAGE, null, null, Integer.toString(0)));

            // if (logFileGame.exists()) {
            // String[] options = new String[]{"Change number", "Rename existing files"};
            // int i = JOptionPane.showOptionDialog(super.manager, "There already exists
            // logfiles for that experiment name. \nWhat do you want to do?", "Filename
            // Conflict", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
            // options, options[0]);
            // if (i == 0) {
            // haveInput = false;
            // message = "Enter the CORRECT experiment number:";
            // } else {
            // haveInput = true;
            // long time = System.currentTimeMillis() / 100;
            // int j = nameLogFileGame.lastIndexOf("/");
            // logFileGame.renameTo(new File(nameLogFileGame.substring(0, j+1) + "b" + time
            // + "_" + nameLogFileGame.substring(j+1)));
            // logFileGame = new File(nameLogFileGame);
            // logFileGame.createNewFile();
            // }
            // } else {
            // haveInput = true;
            // logFileGame.createNewFile();
            // }
            // haveInput = true;
        } catch (NumberFormatException ex) {
            l.severe(ex.toString());
        }
        // } while (!haveInput);
        String path = "logs/";

        new File(path).mkdir();

        String fileName = title.replace(' ', '_') + "-" + (new DecimalFormat("0000")).format(experimentIndex) + "-game-"
                + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd-HmsA")) + ".csv";

        File f = new File(path + fileName);

        return f.createNewFile() ? server.getSession(experimentIndex, f) : null;

    }

    private ExperimentServer experimentServerFactory(Element rootNode) {
        ExperimentServer as = null;
        String classType = rootNode.getAttribute("serverClassName");

        try {
            as = (ExperimentServer) (Class.forName(classType).newInstance());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ServerManagerFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        return as;
    }

    void loadServerFromXML() {
        JFileChooser fc = new JFileChooser(".");
        fc.setDialogTitle("Select an XML file containing the description of the server...");
        fc.setDialogType(JFileChooser.OPEN_DIALOG);
        fc.setMultiSelectionEnabled(true);
        int returnVal = fc.showOpenDialog(this);
        if (returnVal != JFileChooser.CANCEL_OPTION && returnVal != JFileChooser.ERROR_OPTION) {

            try {
                // report is stored as a session
                session = createSession();

                File[] files = fc.getSelectedFiles();
                ExperimentServer prevServer = null;
                for (File file : files) {
                    ExperimentServer es = null;
                    boolean success = false;
                    try {
                        // parse using builder to get DOM representation of the XML file
                        Document dom = db.parse(file.getPath());
                        // get the root elememt
                        Element rootNode = dom.getDocumentElement();

                        es = experimentServerFactory(rootNode);

                        // store data from each stage and write to log
                        EndOfGameListener listener = (results) -> {
                            session.addResults(results, true);
                        };

                        es.initObject(rootNode, player, listener);

                        if (es != null) {
                            es.setManager(this);
                            serverListData.addElement(es);
                            es.waitForClients(false);
                            // threadMap.put(as, new Thread(as));
                            success = true;
                        }
                    } catch (InstantiationException | JMSException | ParserConfigurationException
                            | TransformerConfigurationException | IllegalArgumentException | SecurityException
                            | SAXException | IOException ex) {
                        Logger.getLogger(ServerManagerFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (!success) {
                        JOptionPane.showMessageDialog(this, "Error loading the server.", "Error",
                                JOptionPane.ERROR_MESSAGE);
                        if (es != null) {
                            serverListData.removeElement(es);
                            threadMap.remove(es);
                        }
                    }

                    //setup double-linking
                    if (prevServer != null) {
                        prevServer.setLinks(es);
                    }
                    prevServer = es;

                }
            } catch (IOException ex) {
                Logger.getLogger(ServerManagerFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    void processMessage(DEFMessage m) {
        // System.out.println("++++++++++++++++ " + m.name);
        if ("registrationRequest".equals(m.name)) {
            assert (m.mRoot.getFirstChild().getNodeName().equalsIgnoreCase("clientid"));
            String clientId = m.mRoot.getFirstChild().getTextContent();
            // System.out.println("----"+clientid+"---------");
            // check if any server needs this client
            ExperimentServer as = null;
            Player player = null;
            for (int i = 0; i < serverListData.getSize() && as == null; i++) {
                as = (ExperimentServer) serverListData.elementAt(i);

                for (Player p : as.getPlayers()) {
                    if (p.getID().equals(clientId)) {
                        player = p;
                    }
                }
                if (player == null) {
                    as = null;
                }
            }

            try {
                if (as != null) {
                    if (as.getServerStatus() == ExperimentServerStatus.Waiting) {
                        jh.send(new DEFMessage("manager", m.sender,
                                "" + "<message name=\"registrationApproved\">" + "<className>"
                                        + player.getClientClassName() + "</className>" + "<experimentName>"
                                        + as.getExperimentName() + "</experimentName>" + "</message>"));

                    } else {
                        jh.send(new DEFMessage("manager", m.sender, "" + "<message name=\"registrationDenied\">"
                                + r.s("registration.rejected.idleserver") + "</message>"));
                    }
                } else {
                    jh.send(new DEFMessage("manager", m.sender, "" + "<message name=\"registrationDenied\">"
                            + r.s("registration.rejected.noserver") + "</message>"));
                }
            } catch (JMSException ex) {
                l.log(Level.SEVERE, null, ex);
            } catch (TransformerException ex) {
                l.log(Level.SEVERE, null, ex);
            }
            return;
        }
        if ("clientLoaded".equals(m.name)) {
            String clientId = m.mRoot.getFirstChild().getTextContent();
            ExperimentServer as = null;
            Player player = null;
            for (int i = 0; i < serverListData.getSize() && as == null; i++) {
                as = (ExperimentServer) serverListData.elementAt(i);

                for (Player p : as.getPlayers()) {
                    if (p.getID().equals(clientId)) {
                        player = p;
                    }
                }
                if (player == null) {
                    as = null;
                }
            }
            as.playerAdded(clientId);
        }
    }

    public static String getServerHostnameIP() {
        String hostname_ip = "localhost";
        try {
            hostname_ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return hostname_ip;

    }
}

class ServerListRenderer extends JLabel implements ListCellRenderer {

    private static Logger l = Logger.getLogger(ServerManagerFrame.class.getName());

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
            boolean cellHasFocus) {
        this.setText(((ExperimentServer) value).getHTMLFormattedStatus());
        l.log(Level.FINER, "{0}, {1}, {2}", new Object[] { index, isSelected, cellHasFocus });
        if (isSelected) {
            this.setBorder(BorderFactory.createMatteBorder(2, 4, 2, 4, Color.LIGHT_GRAY));
        } else {
            this.setBorder(new EmptyBorder(2, 4, 2, 4));
        }
        return this;
    }
}
