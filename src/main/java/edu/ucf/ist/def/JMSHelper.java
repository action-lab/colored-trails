/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucf.ist.def;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author culturelab
 */
public abstract class JMSHelper implements MessageListener {

    private static Logger l = L.mcl(JMSHelper.class, Level.FINER);

    private TopicSession pubSession;
    private TopicSession subSession;
    private Session session;
    private TopicPublisher publisher;
    private TopicConnection connection;
    public DocumentBuilderFactory dbf;
    public DocumentBuilder db;
    public Transformer tf;
    protected String[] owner;
    protected long messageId = 0;

    /**
     * Connects to the JMS server at the specified IP and port and listens to messages with tag <code>tag</code>.
     * 
     * @param serverIP The IP of the JMS server to connect to.
     * @param port The port for the JMS server.
     * @param tag The tag of messages to send and listen to.
     * @param owner The source and target of the messages sent and received.
     * @throws JMSException
     * @throws ParserConfigurationException
     * @throws TransformerConfigurationException
     */
    public void initConnection(String serverIP, int port, String tag, String[] owner) throws JMSException, ParserConfigurationException, TransformerConfigurationException {
        String connectionString = "tcp://" + serverIP + ":" + port;
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(connectionString);
        TopicConnection connection = (TopicConnection) factory.createConnection();
        // Create two JMS session objects
        pubSession = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
        subSession = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
        // Look up a JMS topic
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Topic chatTopic = session.createTopic(tag);
        l.log(Level.INFO, "Subscribing to topic ''{0}''", chatTopic.getTopicName());
        // Create a JMS publisher and subscriber
        publisher = pubSession.createPublisher(chatTopic);

        TopicSubscriber subscriber = subSession.createSubscriber(chatTopic);
        // Set a JMS message listener
        subscriber.setMessageListener(this);
        connection.start();

        //------------------------------
        // XML stuff
        dbf = DocumentBuilderFactory.newInstance();
        //Using factory get an instance of document builder
        db = dbf.newDocumentBuilder();
        tf = TransformerFactory.newInstance().newTransformer();

        this.owner = owner;
        Arrays.sort(this.owner);

    }

    public void close() throws JMSException {
        session.close();
        pubSession.close();
        subSession.close();
    }

    /**
     * Processes framework level information in the message.
     * @param msg
     */
    @Override
    public final void onMessage(Message msg) {
        String message = "[Undecipherable]";
        try {
            message = ((TextMessage) msg).getText();
            DEFMessage dm = convertXMLMessageToDEFMessage(msg);
            
            if(Arrays.binarySearch(owner, dm.receiver) >= 0) {
                l.log(Level.FINER, "# {0}", message);
                onMessage(dm);
            }
        } catch (SAXException | IOException | JMSException ex) {
            Logger.getLogger(JMSHelper.class.getName()).log(Level.SEVERE, null, ex);
            l.log(Level.SEVERE, "Coult not understand message: {0}", message);
        }
    }



    public void send(DEFMessage dm) throws JMSException, TransformerException {
        TextMessage tm = (TextMessage) convertDEFMessageToXMLMessage(dm);
        l.finer(tm.getText());
        publisher.send(tm);
    }

    //-----------------------------------------------------------
    // Functions for sharing code for framework message conventions
    public DEFMessage convertXMLMessageToDEFMessage(Message m) throws SAXException, IOException, JMSException {
        // parse the XML. Remove the outermost node
        //System.out.println(((TextMessage) m).getText());
        Document dom = db.parse(new InputSource(new StringReader(((TextMessage) m).getText())));
        Element root = dom.getDocumentElement();
        DEFMessage dm = new DEFMessage(Long.parseLong(root.getAttribute("time"))
                ,Long.parseLong(root.getAttribute("messageId"))
                ,root.getAttribute("sender"),root.getAttribute("receiver")
                ,(Element) root.getFirstChild()
                ,((Element) root.getFirstChild()).getAttribute("name"));
        return dm;
    }

    protected Message convertDEFMessageToXMLMessage(DEFMessage dm) throws TransformerException, JMSException {
        String xmlString = "";
        if (dm.type == DEFMessage.DEFMessageDataType.DOM) {
            StreamResult result = new StreamResult(new StringWriter());
            DOMSource source = new DOMSource(dm.mRoot);
            tf.transform(source, result);
            xmlString = result.getWriter().toString();
        } else if(dm.type == DEFMessage.DEFMessageDataType.String) {
            xmlString = dm.mStr;
        }
        xmlString = "<messagewrapper time=\"" + System.currentTimeMillis() + "\" "
                + "messageId=\"" + messageId++ + "\" "
                + "sender=\"" + dm.sender + "\" "
                + "receiver=\"" + dm.receiver + "\">"
                + xmlString
                + "</messagewrapper>";
        
        return pubSession.createTextMessage(xmlString);
    }

    /**
     * Does business logic for handling the message.
     * @param convertXMLMessageToDEFMessage
     */
    protected abstract void onMessage(DEFMessage convertXMLMessageToDEFMessage);
}
