/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucf.ist.def;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

/**
 * The parent class for all client implementations. This class handles all framework
 * level stuff.
 *
 * <p> To make a new client, override this class and implement the following functions.</p>
 *
 * <p>postInit is called ONCE during initialization. Do all the initialization for the implementation here.</p>
 * <p>processMessage is called every time messages are received. </p>
 *
 * <p>Functionalities provided by this class are,</p>
 * <ul>
 * <li>Message communication using processMessage and sendMessage: processMessage is called by JMSHelper when messages come in.
 * It is highly recommended that you call super.processMessage(..) with the same message object inside this function.
 * Preferably, this should be the first call in the function. Messages can be send by any client side class using sendMessages which is routed through
 * the JMSHelper object.</li>
 *
 * <li>DEFMessage Listeners: Other classes part of the client can register as message listeners.
 * This allows the overriding class to not be aware of all communications between client side classes and the server.
 * E.g.: The OC only handles "dialog.new" message to create the SimpleClientDialog. All the other communication between
 * SimpleClientDialog and the server is done directly using sendMessage and listeners. This increases encapsulation.</li>
 * </ul>
 * @author culturelab
 */
public abstract class AbstractClient implements ExperimentClient {

    private static Logger l = L.mcl(AbstractClient.class, Level.FINER);

    protected JMSHelper jh;
    protected String experimentName;
    protected String serverId;
    protected String clientId;
    protected List<DEFMessageListener> messageListeners = new ArrayList<>();

    @Override
    public void initObject(String serverIp, int listernerPort, String experimentName, String clientId) throws JMSException, ParserConfigurationException, TransformerConfigurationException {
        jh = new JMSHelper() {

            @Override
            protected void onMessage(DEFMessage defMessage) {
                processMessage(defMessage);
            }
        };
        this.serverId = experimentName+".server";
        this.experimentName = experimentName;
        this.clientId = clientId;
        jh.initConnection(serverIp, listernerPort, serverId, new String[] {clientId,experimentName+".all"});

        postInit();
    }

    /**
     * Handles framework level messages; overriding function must call super(...)
     * and pass on the message if it is of no interest.
     * @param defMessage
     */
    public void processMessage(DEFMessage defMessage) {
        if(defMessage.name.equals("disconnect")) {
            l.log(Level.INFO, "Server requested disconnect with reason:{0}", defMessage.mRoot.getTextContent());
            System.exit(0);
        }

        for(int i = 0; i < messageListeners.size(); i++) {
            if(messageListeners.get(i).isMarkedForRemoval()) {
                messageListeners.remove(i);
                i--;
            } else {
                messageListeners.get(i).processMessage(defMessage);
            }
        }
    }

    protected abstract void postInit();

    @Override
    public String getClientId() {
        return clientId;
    }

    @Override
    public String getServerId() {
        return serverId;
    }

    @Override
    public void addListener(DEFMessageListener dml) {
        messageListeners.add(dml);
    }

    @Override
    public void removeListener(DEFMessageListener dml) {
        messageListeners.remove(dml);
    }

    @Override
    public void sendMessage(DEFMessage dm) {
        try {
            jh.send(dm);
        } catch (JMSException | TransformerException ex) {
            Logger.getLogger(AbstractClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendMessageToServer(String name, String body) {
        sendMessage(new DEFMessage(getClientId(), getServerId(), "<message name=\"" + name + "\">" + body + "</message>"));
    }


}
