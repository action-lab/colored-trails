/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucf.ist.def.ex.chatclient;

import edu.ucf.ist.truststudy.BoardWindow;
import edu.ucf.ist.def.AbstractClient;
import edu.ucf.ist.def.DEFMessage;
import edu.ucf.ist.def.L;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Handles message chat.addMessage
 * <pre><message name="chat.addMessage"><chatMessage lifeTime=\"5000\" >Text</chatMessage></message></pre>
 * @author culturelab
 */
public class ChatClient extends AbstractClient{

    private static final Logger l = L.mcl(ChatClient.class, Level.FINER);
    protected ChatWindow cw;
    //BoardWindow bw;

    @Override
    protected void postInit() {
        cw = new ChatWindow(jh, this);
        cw.setLocation(0, 0);
        cw.setVisible(true);
//        bw = new BoardWindow(jh,this);
//        bw.initBoard();
//        bw.setVisible(true);
    }

    @Override
    public void processMessage(DEFMessage defMessage) {
        if(defMessage.name.equals("chat.addMessage")) {
            l.finer("Processing chat.addMessage");
            Element cmN = (Element) defMessage.mRoot.getFirstChild();
            cw.addMessage(defMessage.time, Integer.parseInt(cmN.getAttribute("lifeTime")), cmN.getFirstChild().getNodeValue());
            return;
        }
        l.finer("sending unknown message upstream.");
        super.processMessage(defMessage);
    }


}
