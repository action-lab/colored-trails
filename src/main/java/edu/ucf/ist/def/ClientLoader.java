/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucf.ist.def;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.swing.JOptionPane;
import javax.xml.transform.TransformerException;
import javax.xml.parsers.ParserConfigurationException;
import picocli.CommandLine;
import picocli.CommandLine.Option;

/**
 *
 * @author culturelab
 */
public class ClientLoader implements Callable {

    private static Logger l = L.mcl(ClientLoader.class, Level.FINER);

    JMSHelper jh;
    
    
    
    String serverIp = "127.0.0.1";
    @Option(names={"-i","--interface"},required=true,description = "server ip address")
    public void setServerIp(String ipAddress){
        serverIp = getIpAddress(ipAddress);
    }
    @Option(names = {"-p","--port"},required=true,description = "listening port")
    int listeningPort = -1;
    @Option(names = {"-c","client-id"},required=true,description = "client id")
    String clientId = "";
    @Option(names = { "-h", "--help" }, usageHelp = true, description = "display a help message")
    boolean helpRequested = false;
    
    ClientLoader()
    {
        //handles the case where IP is not specified on the command line
//        serverIp = getIpAddress("auto");
    }
    
    @Override
    public Void call() throws Exception{
        
        ClientLoader cl = this;
        cl.jh = new JMSHelper() {

            @Override
            public void onMessage(DEFMessage msg) {
                cl.processMessage(msg);
            }
        };
        
        l.log(Level.INFO, "Connecting to server at tcp://{0}:{1}", new Object[]{serverIp, listeningPort});
        
        try {
            cl.jh.initConnection(serverIp, listeningPort, "manager", new String[] {clientId});
        } catch (JMSException ex) {
            l.log(Level.SEVERE, "Connection error", ex);
            JOptionPane.showMessageDialog(null, "Could not connect to server. Contact the Research Assistant.");
            System.exit(1);
        }

        try {
            cl.jh.send(new DEFMessage(clientId, "manager", "<message name=\"registrationRequest\"><clientid>" + clientId + "</clientid></message>"));
        } catch (JMSException ex) {
            l.log(Level.SEVERE, "Send error", ex);
            JOptionPane.showMessageDialog(null, "Could not send message to server. Contact the Research Assistant.");
            System.exit(1);
        }
        
        return null;
    }
    
    public static void main(String[] args) {
        
        CommandLine.call(new ClientLoader(), args);

    }
    
    private String getIpAddress(String ipAddress){
        String ip = "";
        try {
            if(ipAddress.equals("auto") || ipAddress.isEmpty())
            {
                InetAddress[] addresses = InetAddress.getAllByName(InetAddress.getLocalHost().getHostName());
                for(InetAddress address:addresses)
                {
                    if(!address.isLoopbackAddress())
                    {
                        ip = address.getHostAddress();
                        break;
                    }
                }
            }
            else
            {
                ip = InetAddress.getByName(ipAddress).getHostAddress();
            }
        } catch (UnknownHostException ex) {
            Logger.getLogger(ClientLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ip;
    }
     
    private void processMessage(DEFMessage dm) {
        String messageName = dm.mRoot.getAttribute("name");
        if (messageName == null ? false : messageName.equals("registrationApproved")) {

            //String reply = dm.mRoot.getTextContent();
            //JOptionPane.showMessageDialog(null, "Server accepted registration");// with reply: " + reply + ".");
            String className = dm.mRoot.getElementsByTagName("className").item(0).getFirstChild().getNodeValue();
            l.log(Level.INFO, "Loading client class {0}", className);
            String experimentName = dm.mRoot.getElementsByTagName("experimentName").item(0).getFirstChild().getNodeValue();

            try {

                ExperimentClient ec = (ExperimentClient) Class.forName(className).newInstance();
                ec.initObject(serverIp,  listeningPort, experimentName, clientId);
                //TODO: Send a reply saying client has been started. The server should wait for this before calling startExperiment.
                jh.send(new DEFMessage(clientId, "manager", "<message name=\"clientLoaded\"><clientid>" + clientId + "</clientid></message>"));
                jh.close();

            } catch (JMSException | ParserConfigurationException | TransformerException | InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
                l.log(Level.SEVERE, null, ex);
                l.severe("Could not load and set up Client Class. Exiting...");
                System.exit(1);
            }
        } else if (messageName.isEmpty() ? false : messageName.equals("registrationDenied")) {
            String reply = dm.mRoot.getFirstChild().getTextContent();
            JOptionPane.showMessageDialog(null, "Server rejected registration with reply: " + reply);
            System.exit(1);
        } else {
            l.log(Level.SEVERE, null, "Could not understand message from server ("+messageName+"). Contact the Research Assistant.");
            JOptionPane.showMessageDialog(null, "Could not understand message from server ("+messageName+"). Contact the Research Assistant.");
            System.exit(1);
        }
    }
}
