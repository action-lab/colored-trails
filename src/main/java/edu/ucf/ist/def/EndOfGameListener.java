/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucf.ist.def;

import edu.ucf.ist.session.StageResults;

/**
 *
 * @author dougcooper
 */
public interface EndOfGameListener{
    void Report(StageResults results);
}
