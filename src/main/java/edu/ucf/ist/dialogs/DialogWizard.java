package edu.ucf.ist.dialogs;

import java.awt.Container;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import net.miginfocom.swing.MigLayout;
import unquietcode.tools.esm.EnumStateMachine;
import unquietcode.tools.esm.StateHandler;
import unquietcode.tools.esm.TransitionHandler;

public class DialogWizard extends JDialog {

  private static final long serialVersionUID = 6176947089664585065L;

  public static class Data extends LinkedHashMap<String, String> {

    private static final long serialVersionUID = 2162800290547098111L;
  }

  private PageFactory factory;
  private OnSubmitListener listener;
  private Data data = new Data();

  enum NavState {
    INIT, ONLY, FIRST, MIDDLE, LAST
  }

  EnumStateMachine<NavState> esm = new EnumStateMachine<>(NavState.INIT);

  public interface OnSubmitListener {
    public void call();
  }

  public static class Pages extends ArrayList<Page> {
    private static final long serialVersionUID = 4233673081551588375L;
  }

  private Container contentPane;
  private Nav nav;

  public class InvalidSizeException extends Exception {

    private static final long serialVersionUID = 2763386503155698051L;

    public InvalidSizeException(String string) {
      super(string);
    }
  }

  public DialogWizard(JFrame parent, PageFactory factory) throws InvalidSizeException{
    super(parent, true);
    
    if(factory.size()==0)
      throw new InvalidSizeException("The dialog wizard requires at least one page!");

    this.factory = factory;
    initStateMachine();
    initComponents();
  }

  private void initStateMachine() {

    TransitionHandler<NavState> goToFirstPage = new TransitionHandler<NavState>() {
      public void onTransition(NavState from, NavState to) {
        updateView(factory.goToFirstPage());
      }
    };

    esm.addTransitions(goToFirstPage, NavState.INIT, NavState.FIRST, NavState.ONLY, null);

    esm.addAllTransitions(Arrays.asList(NavState.FIRST, NavState.MIDDLE, NavState.LAST), true);

    esm.addTransitions(NavState.ONLY, NavState.ONLY);

    esm.addTransition(NavState.ONLY, null);
    esm.addTransition(NavState.LAST, null);

    StateHandler<NavState> setFirstState = new StateHandler<NavState>() {
      @Override
      public void onState(NavState state) {
        nav.disableAll();
        if (factory.isValid(factory.getCurrentPage())) {
          nav.getNextBtn().setEnabled(true);
        } else {
          // do nothing
        }

        nav.setNextListener(() -> {
          goToNextPage();
        });

      }
    };

    StateHandler<NavState> setMiddleState = new StateHandler<NavState>() {
      @Override
      public void onState(NavState state) {
        nav.enableAll();
        if (factory.isValid(factory.getCurrentPage())) {
          nav.getSubmitBtn().setEnabled(false);
        } else {
          nav.disableAll();
          nav.getPreviousBtn().setEnabled(true);
        }

        nav.setNextListener(() -> {
          goToNextPage();
        });

        nav.setPreviousListener(() -> {
          gotToPreviousPage();
        });
      }
    };

    StateHandler<NavState> setLastState = new StateHandler<NavState>() {
      @Override
      public void onState(NavState state) {
        nav.enableAll();
        if (factory.isValid(factory.getCurrentPage())) {
          nav.getNextBtn().setEnabled(false);
          nav.getSubmitBtn().setVisible(true);
        } else {
          nav.disableAll();
          nav.getPreviousBtn().setEnabled(true);
        }

        nav.setPreviousListener(() -> {
          gotToPreviousPage();
        });
      }
    };

    StateHandler<NavState> setOnlyState = new StateHandler<NavState>() {
      @Override
      public void onState(NavState state) {
        nav.disableAll();
        nav.getPreviousBtn().setVisible(false);
        nav.getNextBtn().setVisible(false);
        if (factory.isValid(factory.getCurrentPage())) {
          nav.getSubmitBtn().setEnabled(true);
          nav.getSubmitBtn().setVisible(true);
        } else {
          // do nothing
        }
      }
    };

    esm.onEntering(NavState.FIRST, setFirstState);
    esm.onEntering(NavState.MIDDLE, setMiddleState);
    esm.onEntering(NavState.LAST, setLastState);
    esm.onEntering(NavState.ONLY, setOnlyState);

  }

  private void gotToPreviousPage() {
    updateView(factory.goToPreviousPage());
    if (factory.isFirst(factory.getCurrentPage())) {
      esm.transition(NavState.FIRST);
    } else {
      esm.transition(NavState.MIDDLE);
    }
    
  }

  private void goToNextPage() {
    updateView(factory.goToNextPage());
    if (factory.isLast(factory.getCurrentPage())) {
      esm.transition(NavState.LAST);
    } else {
      esm.transition(NavState.MIDDLE);
    }
    
  }

  private void initComponents() {
    contentPane = getContentPane();
    this.getParent().setPreferredSize(new Dimension(320, 240));
    contentPane.setLayout(new MigLayout("flowy"));
    nav = new Nav();

    //set the initial state
    if (factory.size() > 1) {
      esm.transition(NavState.FIRST);
    } else if (factory.size() == 1) {
      esm.transition(NavState.ONLY);
    } else {
      esm.transition(null); // TODO (doug): handle transition
    }

    factory.getPages().forEach((page) -> {
      page.addValidListener((val) -> {
        esm.transition(esm.currentState());
      });

      page.addInvalidListener((val)->{
        esm.transition(esm.currentState());
      });
    });

    nav.setSubmitListener(() -> {
      factory.getPages().forEach((page) -> {
        Data d = page.getData();
        if (d != null)
          data.putAll(d);
      });
      listener.call();
    });
    contentPane.add(nav);

    pack();
    setLocationRelativeTo(getParent());
  }

  void updateView(Page currentPage) {
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        try {
          contentPane.remove(0);
        } catch (IndexOutOfBoundsException ex) {
          // exception thrown on initialization
        } finally {
          contentPane.add(currentPage, 0);
          contentPane.revalidate();
          contentPane.repaint();
        }
      }
    });
  }

  public void setOnSubmit(OnSubmitListener listener) {
    this.listener = listener;
  }

  public Data getFormData() {
    return data;
  }
}
