package edu.ucf.ist.dialogs;

import java.awt.event.*;
import javax.swing.*;
import net.miginfocom.swing.MigLayout;

public class Nav extends JPanel {

  private static final long serialVersionUID = 2715281741263200774L;

  public interface NavListener{
    void call();
  }

  private JButton previousBtn;
  private JButton nextBtn;
  private JButton submitBtn;

  public JButton getNextBtn() {
    return this.nextBtn;
  }

  public JButton getSubmitBtn() {
    return this.submitBtn;
  }

  public JButton getPreviousBtn() {
    return this.previousBtn;
  }

  NavListener nextListener,previousListener,submitListener;

  Nav() {
    init();
  }

  private void init() {
    setLayout(new MigLayout(""));
    previousBtn = new JButton("Previous");
    nextBtn = new JButton("Next");
    submitBtn = new JButton("OK");

    previousBtn.addActionListener(new ActionListener(){
    
      @Override
      public void actionPerformed(ActionEvent e) {
        previousListener.call();
      }
    });

    nextBtn.addActionListener(new ActionListener(){
    
      @Override
      public void actionPerformed(ActionEvent e) {
        nextListener.call();
      }
    });

    submitBtn.addActionListener(new ActionListener(){
    
      @Override
      public void actionPerformed(ActionEvent e) {
        submitListener.call();
      }
    });
    previousBtn.setEnabled(false);
    nextBtn.setEnabled(false);
    submitBtn.setVisible(false);

    add(previousBtn, "tag back");
    add(nextBtn, "tag next");
    add(submitBtn,"tag ok");
  }

  public void setNextListener(NavListener listener){
    nextListener = listener;
  }

  public void setPreviousListener(NavListener listener){
    previousListener = listener;
  }

  public void setSubmitListener(NavListener listener){
    submitListener = listener;
  }

  public void disableAll() {
    previousBtn.setEnabled(false);
    nextBtn.setEnabled(false);
    submitBtn.setEnabled(false);
  }

  public void enableAll() {
    previousBtn.setEnabled(true);
    nextBtn.setEnabled(true);
    submitBtn.setEnabled(true);
  }
}