package edu.ucf.ist.dialogs;

import edu.ucf.ist.dialogs.DialogWizard.Pages;

public abstract class PageFactory {

  protected Pages pages;

  public PageFactory() {
    pages = new Pages();
  }

  public abstract String getName();

  protected void addPage(Page page){
    pages.add(page);
  }

  public final Pages getPages() {
    return pages;
  }

  public final int size(){
    return pages.size();
  }

  private int currentIndex = 0;

  public int getCurrentIndex() {
    return this.currentIndex;
  }

  public void setCurrentIndex(int currentIndex) {
    if(currentIndex < 0){
      this.currentIndex = 0;
    } else if (currentIndex > this.size()-1) {
      this.currentIndex = this.size()-1;
    } else {
      this.currentIndex = currentIndex;
    }
  }

  public Page goToNextPage() {
    setCurrentIndex(getCurrentIndex()+1);
    return getCurrentPage();
  }

  public Page goToPreviousPage(){
    setCurrentIndex(getCurrentIndex()-1);
    return getCurrentPage();
  }

  public Page goToFirstPage(){
    setCurrentIndex(0);
    return getCurrentPage();
  }

  public Page goToLastPage(){
    setCurrentIndex(size()-1);
    return getCurrentPage();
  }

  public Page getCurrentPage(){
    return pages.get(getCurrentIndex());
  }

  public Page getFirstPage(){
    return pages.get(0);
  }

  public Page getLastPage(){
    return pages.get(size()-1);
  }

  public Page getNextPage(){
    return pages.get(getCurrentIndex()+1);
  }

  public Page getPreviousPage(){
    return pages.get(getCurrentIndex()-1);
  }

  public boolean isLast(Page page){
    return page.equals(getLastPage());
  }

  public boolean isFirst(Page page){
    return page.equals(getFirstPage());
  }

  public boolean isValid(Page page){
    return page.isReadyToAdvance();
  }
}