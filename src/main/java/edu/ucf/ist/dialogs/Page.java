package edu.ucf.ist.dialogs;

import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import edu.ucf.ist.dialogs.DialogWizard.Data;

public abstract class Page extends JPanel {

  private static final long serialVersionUID = 2181240438946808195L;

  private Data data = new Data();

  protected void setData(String key, String value){
    this.data.put(key, value);
  }

  public interface ValidListener{
    void call(Object obj);
  }

  public interface InvalidListener{
    void call(Object obj);
  }

  public class ValidListeners extends ArrayList<ValidListener>{

    private static final long serialVersionUID = -3866631214744799408L;
  }

  public class InvalidListeners extends ArrayList<InvalidListener>{

    private static final long serialVersionUID = 7055059315661332681L;

  }

  public abstract boolean isReadyToAdvance();

  protected ValidListeners validListeners = new ValidListeners();
  protected InvalidListeners invalidListeners = new InvalidListeners();

  protected Page() {
    setBorder(BorderFactory.createTitledBorder(""));
    setPreferredSize(new Dimension(640, 480));
    setMaximumSize(new Dimension(640, 480));
    setMinimumSize(new Dimension(640, 480));
  }

  public void addValidListener(ValidListener listener) {
    validListeners.add(listener);
  }

  public abstract Data getData();

  public void addInvalidListener(InvalidListener listener) {
    invalidListeners.add(listener);
  }

}