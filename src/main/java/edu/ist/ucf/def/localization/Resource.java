/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ist.ucf.def.localization;

import java.util.HashMap;

/**
 * This class provides access to locale specific resource. Mostly a sugaring class
 * that avoids having to pass around the location string and avoids one extra hashTable lookup.
 *
 * @author culturelab
 */
public class Resource {

    protected final ResourceManager rm;
    protected final String locale;
    protected final HashMap<String,String> map;

    public Resource(ResourceManager rm,String locale, HashMap<String,String> map) {
        this.rm = rm;
        this.locale = locale;
        this.map = map;
    }

    public String s(String key) {
        String value;
        if((value = map.get(key)) != null) return value;
        else
            return rm.getFallbackString(locale, key);
    }

    public String getString(String key) {
        return s(key);
    }

    public ResourceManager getResourceManager() {
        return rm;
    }

}
