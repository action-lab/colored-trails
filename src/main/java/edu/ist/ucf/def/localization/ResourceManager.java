/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ist.ucf.def.localization;

import edu.ucf.ist.def.L;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This class contains all the local specific resources associated with the server.
 * Using the Resource object which can be produced using getResource, developers
 * can easily provide flexible localization.
 *
 * <p>Users are expected to first initialize the ResourceManager using the direct
 * insertion functions (add, addLocale, etc.) or just make one from DOM objects
 * using createResourceManager.
 *
 * @author culturelab
 */
public class ResourceManager {

    private static final Logger l = L.mcl(ResourceManager.class, Level.INFO);
    HashMap<String, HashMap<String, String>> resourceMap = new HashMap<>();
    HashMap<String, String[]> fallbackMap = new HashMap<>();
    String defaultLocale = null;

    public void add(String locale, String key, String value) {
        resourceMap.get(locale).put(key, value);
    }

    public void addLocale(String locale, String[] localeFallbackOrder) {
        resourceMap.put(locale, new HashMap<>());
        fallbackMap.put(locale, localeFallbackOrder);
    }

    public void setDefaultLocale(String locale) {
        this.defaultLocale = locale;
    }

    /**
     * Returns a resource for the specified locale.
     * @param locale
     * @return
     */
    public Resource getResource(String locale) {
        return new Resource(this, locale, resourceMap.get(locale));
    }

    public String getFallbackString(String locale, String key) {
        l.log(Level.FINER, "Looking for fallback string for {0} for locale {1}", new Object[]{key, locale});
        for (String fallbackLocale : fallbackMap.get(locale)) {
            String value;
            if ((value = resourceMap.get(fallbackLocale).get(key)) != null) {
                return value;
            }
        }
        l.log(Level.WARNING, "Search for value of {0} for locale {1} failed through locales {2}", new Object[]{key, locale, Arrays.toString(fallbackMap.get(locale))});
        return "[#"+key+"]";
    }

    public Resource getDefaultResource() {
        return getResource(defaultLocale);
    }

    /**
     * Creates a ResourceManager from XML DOM objects created from the XML configuration file. 
     * See the server configuration file for format.
     * 
     * @param resourceList
     * @return
     */
    public static ResourceManager createResourceManager(NodeList resourceList) {
        ResourceManager rm = new ResourceManager();
        if(resourceList.getLength() == 0) {
            l.warning("No resources could be loaded.");
            return rm;
        }
        Element resourceRoot = (Element) resourceList.item(0);
        rm.setDefaultLocale(resourceRoot.getAttribute("defaultLocale"));

        // Find the resources
        NodeList nl = resourceRoot.getElementsByTagName("resource");
        for (int i = 0; i < nl.getLength(); i++) {
            Element resource = (Element) nl.item(i);
            String locale = resource.getAttribute("locale");

            // Make the fallback
            NodeList fb = resource.getElementsByTagName("fallback");
            String[] fallbackList;
            if (fb.getLength() != 0) {
                Element fallback = (Element) fb.item(0);
                List<String> fbLocales = new ArrayList<>();
                NodeList fbList = fallback.getElementsByTagName("locale");
                for (int li = 0; li < fbList.getLength(); li++) {
                    fbLocales.add(fbList.item(li).getFirstChild().getNodeValue());
                }
                fallbackList = fbLocales.toArray(new String[0]);
            } else {
                fallbackList = new String[0];
            }
            // Add the locale with the fallback
            rm.addLocale(locale, fallbackList);

            // Add the strings
            NodeList sList = resource.getElementsByTagName("string");
            for (int si = 0; si < sList.getLength(); si++) {
                Element s = (Element) sList.item(si);
                String value = null;
                if(s.getFirstChild() == null)
                    value = "";
                else if(s.getFirstChild().getNodeType() == Element.TEXT_NODE) {
                    value = s.getFirstChild().getNodeValue();
                } else if(s.getFirstChild().getNodeType() == Element.CDATA_SECTION_NODE) {
                    CDATASection cdata = (CDATASection) s.getFirstChild();
                    value = cdata.getTextContent();
                }

                rm.add(locale, s.getAttribute("key"), value);
            }

        }
        return rm;
    }
}
