#!/bin/bash

if [[ $# -eq 0 ]]; then
    echo "No arguments supplied"
fi

stage_a="run_stage$1_a.bat"
stage_b="run_stage$1_b.bat"

if [[ $(sh $stage_a > /dev/null 2>&1 &)  ]]; then
  echo "$stage_a launched!"
fi

if [[ $(sh $stage_b > /dev/null 2>&1 &)  ]]; then
  echo "$stage_b launched!"
fi
