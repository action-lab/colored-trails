# README

[![pipeline status](https://gitlab.com/action-lab/colored-trails/badges/Dev/pipeline.svg)](https://gitlab.com/action-lab/colored-trails/commits/Dev)

## Prepare the build environment

### Install Java

#### *Windows*

Install [chocolatey](https://chocolatey.org/docs/installation)

Install java

`choco install javaruntime`

#### *Debian/Ubuntu*

`sudo apt install default-jre`

> Verify that java has been installed.
> `java -version`

`sudo apt-get install openjdk-8-jdk-headless`

### Install Maven

#### *Windows*

Install maven

`choco install maven`

#### *Debian/Ubuntu*

`sudo apt install maven`

> Verify that maven has been installed.
> `mvn -version`

### Build the app

Run `mvn clean install`

> This will download maven dependencies on first run
> Need to set cmd/powershell directory to folder that Colored Trails is saved to:  
> e.g., `Set-location "C:\Users\trevor\Desktop\colored-trails-Dev"`

## Before you run

### Check the network connections

Before running any software you should make sure the computers can "talk" to each other. On each computer do the following:

1. [Get](https://kb.wisc.edu/page.php?id=27309) the ip address
2. On the researcher pc, [ping](https://www.lifewire.com/how-to-ping-computer-or-website-818405) the lab laptop

If you have no lost packets then you should be good to go.

If they cant see each other then you're dead in the water and you need to call the IT department. You may run into an issue where each has internet access but is prevented from talking to each other. If for some reason they wont allow it you'll have to look into setting up a private network.

### Windows Firewall Settings

In order to get the lab computers to talk to each other and send information through Java, there are a few settings that will need to be changed. You will need administrator access to the computers.

#### Change Firewall Settings

1. Open Windows Defender Firewall Settings
2. Select Inbound Rules on the left pane
3. In the right pane, find File and Printer Sharing (Echo Request - ICMPv4- In)
4. Right click on each of these instances and Enable Rule
5. Select Outbound Rules on the left pane
6. In the right pane, find File and Printer Sharing (Echo Request - ICMPv4- In)
7. Right click on each of these instances and Enable Rule
8. Select Apps to Allow through Firewall and be sure that all instances of Java are allowed on both private and public networks

#### As Administrator, allow Everyone to use Java

1. Go to: C:\Program Files (x86)\Common Files\Oracle\Java\javapath_target_#########
2. Where ######### is a unique 9 digit number
3. Right click on the java.exe, Select Properties >> Security >> click the Edit button >> give Full Control to Everyone
4. Repeat for javaw.exe and javaws.exe

## Setting up the Game

For each participant, the application directory should be copied on the researcher computer. Both the `run_stage<n>_a.bat` and the `run_stage<n>_b.bat` files should have the ip address of the researcher computer and matching port numbers. However, the port number should be different for each copy of the directory. Once this has been done, designate one of the directories for a specific participant computer and copy the directory to that computer.

### On the researcher computer

1. Double-click `run_server.bat`
2. Click `Load Server`
3. Select all `truststudy_stage<n>_en.xml` files and click `Open`
4. Enter the experiment number
5. Double-click `run_stage1_a.bat`
6. Repeat step 5 for each `a` stage
7. Repeat steps 1 thru 6 for each participant computer

### On each participant computer

1. Double-click `run_stage1_b.bat`
2. Repeat step 1 for each `b` stage

## Starting the game

From the researcher computer, select and right-click on Stage 1, then select `Start`