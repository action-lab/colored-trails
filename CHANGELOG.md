[1.1.0]
* Add date-timestamp to log file name
* Load multiple stages at once for a given experiment
* Use output from previous stage as input for current stage when `foodLevel="previous"` specified in `<experiment>` tag in stage .xml file
* Combine logs for all stages into a single log file
* Automatically connect each stage after selected
* Clear stages when Load is clicked to allow add new set of stages
* Command line parameter support added for ClientLoader class when running stages from .bat files

[1.0.0]
* Initial release